<?php
error_reporting(1);
require_once ('dbinclude.php');
//
define ('FLASH','flash');
define ('HTML5','html5');
define('VIDEO_DIR','http://rushnewmedia.de/clients/kloepfer/videos/final/');
define('THUMBS_DIR','http://rushnewmedia.de/clients/kloepfer/videos/final/thumbs/');
define('THUMB',"{THUMB}");
define('RTMP','rtmp://212.227.89.184/kloepfer/final/');
//
define('VIDEO_FILE','{VIDEO}');
define('TITLE','{TITLE}');
define('DESC','{DESC}');
define ('START_RSS_FLASH',"<rss version=\"2.0\" xmlns:jwplayer=\"http://developer.longtailvideo.com/\" xmlns:media=\"http://search.yahoo.com/mrss/\">\n");
define ('START_RSS_HTML5',"<rss version=\"2.0\" xmlns:media=\"http://search.yahoo.com/mrss/\">\n");

define ('THUMBNAIL'," \n<media:thumbnail url=\"".THUMBS_DIR.THUMB."\"/>\n");
define ('FLASH_ITEM',"<item>\n<title>".TITLE."</title>\n<description>".DESC."</description>\n<jwplayer:file>".VIDEO_FILE."</jwplayer:file>\n<jwplayer:streamer>".RTMP."</jwplayer:streamer>".THUMBNAIL."\n</item>\n");
define ('HTML5_ITEM',"<item>\n<title>".TITLE."</title>\n<description>".DESC."</description>\n<media:content url=\"".VIDEO_DIR.VIDEO_FILE."\">\n</media:content>".THUMBNAIL."</item>\n");
define ('CHANNEL_TITLE_START',"<channel>\n<title>Playlist</title>\n");
define ('CHANNEL_END',"</channel>\n");
define ('RSS_END',"</rss>\n");
//
$target=$_REQUEST['target'];
$catid=$_REQUEST['catid'];
$userid=$_REQUEST['userid'];
$productid=$_REQUEST['productid'];
$azubiid=$_REQUEST['azubiid'];
$tutorialid=$_REQUEST['tutorialid'];
$standortid=$_REQUEST['standortid'];
$regionid=$_REQUEST['regionid'];
$serviceid=$_REQUEST['serviceid'];
/*
 if(!isset($catid))
{
makePlaylist();
exit();
}
*/
if(isset($serviceid))
{
	$records=getServiceVideos($serviceid,$standortid,$regionid);
	makePlaylist($records);
	exit();
}
if(isset($userid))
{
	$records=getUserVideos($userid);
	makePlaylist($records);
	exit();
}
if(isset($productid))
{
	$records=getGeneralIDVideos($productid);
	makePlaylist($records);
	exit();
}
if(isset($azubiid))
{
	$records=getGeneralIDVideos($azubiid);
	makePlaylist($records);
	exit();
}
if(isset($tutorialid))
{
	$records=getGeneralIDVideos($tutorialid);
	makePlaylist($records);
	exit();
}
if(isset($standortid))
{
	$records=getStandortVideos($standortid);
	makePlaylist($records);
	exit();
}
makePlaylist();
function makePlaylist($records)
{
	global $target;
	if( $target==FLASH)
	{
		$playlist=START_RSS_FLASH;
	}
	else
	{
		$playlist=START_RSS_HTML5;
	}

	$playlist.=CHANNEL_TITLE_START;
	foreach($records as $record)
	{
		$filename=utf8_encode($record['FileName']);
		$thumbname=str_replace('.mp4','.jpg',$filename);
		$title=utf8_encode($record['ThemaText']);
		$desc=utf8_encode($record['DetailText']);
		if( $target==FLASH)
		{
			$item=FLASH_ITEM;
		}
		else
		{
			$item=HTML5_ITEM;
		}
		$item=str_replace(TITLE,$title,$item);
		$item=str_replace(DESC,$desc,$item);
		$item=str_replace(VIDEO_FILE,$filename,$item);
		$item=str_replace(THUMB,$thumbname,$item);
		$playlist.=$item;
	}
	$playlist.=CHANNEL_END.RSS_END;
	echo $playlist;
}
function getServiceVideos($serviceid,$standortid,$regionid)
{
	global $db;
	if(!is_numeric($serviceid))
	{
		$serviceid=enquote($serviceid);
	}
	if(!isset($standortid))
	{
		$standortid=0;
	}
	else
	{
		$sql = "SELECT ID  FROM standort WHERE StandOrtID_extern=$standortid";
		$sqlresult = mysql_query($sql, $db);
		if (!$sqlresult) {
			$error = mysql_error($db);
			return $records;
		}
		$num=mysql_num_rows($sqlresult);
		if($num>0)

		{
			$record=mysql_fetch_assoc($sqlresult);
			$standortid=$record['ID'];
		}
		else
		{
			$standortid=0;
		}

	}
	if(!isset($regionid))
	{
		$regionid=0;
	}

	$sql = "SELECT FileName ,ThemaText,DetailText FROM video WHERE  GeneralID=$serviceid AND active=1 AND StandOrtID=$standortid AND RegionID=$regionid";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		return $records;
	}
	$num=mysql_num_rows($sqlresult);
	if($num>0)

	{
		while($record=mysql_fetch_assoc($sqlresult))
		{
			$records[]=$record;
		};
		return $records;
	}
	$sql = "SELECT FileName ,ThemaText,DetailText FROM video WHERE  GeneralID=$serviceid AND active=1 AND StandOrtID=$standortid AND RegionID=0";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		return $records;
	}
	$num=mysql_num_rows($sqlresult);
	if($num>0)

	{
		while($record=mysql_fetch_assoc($sqlresult))
		{
			$records[]=$record;
		};
		return $records;
	}
	$sql = "SELECT FileName ,ThemaText,DetailText FROM video WHERE  GeneralID=$serviceid AND active=1 AND StandOrtID=0 AND RegionID=0";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		return $records;
	}
	$num=mysql_num_rows($sqlresult);
	if($num>0)

	{
		while($record=mysql_fetch_assoc($sqlresult))
		{
			$records[]=$record;
		};
		return $records;
	}
}
function getUserVideos($userid)
{
	global $db;
	$sql = "SELECT ID  FROM mitarbeiter WHERE InternalID=\"$userid\" ";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		return $records;
	}

	$num=mysql_num_rows($sqlresult);
	if($num>0)
	{
		$record=mysql_fetch_assoc($sqlresult);
		$userid=$record['ID'];
	}
	else
	{
		return  $records;
	}
	$sql = "SELECT FileName ,ThemaText,DetailText FROM video WHERE  MitarbeiterID=$userid AND active=1";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		return $records;
	}
	else
	{
		while($record=mysql_fetch_assoc($sqlresult))
		{
			$records[]=$record;
		};
	}
	return $records;
}
function getGeneralIDVideos($generalid)
{
	global $db;

	$sql = "SELECT FileName ,ThemaText,DetailText FROM video WHERE  GeneralID=\"$generalid\" AND active=1";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		return $records;
	}
	else
	{
		while($record=mysql_fetch_assoc($sqlresult))
		{
			$records[]=$record;
		};
	}
	return $records;
}
function getStandortVideos($standortid)
{
	global $db;
	$sql = "SELECT ID  FROM standort WHERE StandOrtID_extern=$standortid";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		return $records;
	}
	$num=mysql_num_rows($sqlresult);
	if($num>0)
	{
		$record=mysql_fetch_assoc($sqlresult);
		$standortid=$record['ID'];
	}
	else
	{
		$standortid=0;
	}
	$sql = "SELECT FileName ,ThemaText,DetailText FROM video WHERE  StandOrtID=$standortid AND active=1 AND CategoryID=5 ";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		return $records;
	}
	else
	{
		while($record=mysql_fetch_assoc($sqlresult))
		{
			$records[]=$record;
		};
	}
	return $records;
}
?>