<?php
/*
 * Created on Mar 8, 2011
*
* To change the template for this generated file go to
* Window - Preferences - PHPeclipse - PHP - Code Templates

*/
define("VIDEOS", "http://kloepfer.rushnewmedia.de/video-n66Q2/distribution-12998/stream/");
define("THUMBS", "/video-n66Q2/distribution-12998/thumbs/");
define('BASE','http://kloepfer.rushnewmedia.de');


//require_once ('dbinclude.php');
$videos=array();
//$videos=getVideos();

/*
 *
*/
function getVideos() {
	global $db;
	$json['video'] = array ();
	$sql = "SELECT video.ID,video.FileName as videoname,video.Length as duration,video.UpdateTimestamp as timestamp,theme.ThemaText as theme,theme.ID as ThID,theme.DetailText as detail FROM video,theme WHERE  theme.ID=video.ThemeID";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'select all videos' . $error);
	};
	$videos;
	$amount=0;
	while ($record = mysql_fetch_assoc($sqlresult) ) {
		$filename=$record['videoname'];
		$thumb=array_shift(explode(".",$filename));
		$thumb=THUMBS.$thumb.'.jpg';

		if(file_exists('..'.$thumb))
		{
			$amount++;
			$record['theme']=htmlentities($record['theme']);
			$record['theme']=substr($record['theme'],0,20).'...';
			$record['thumb']=BASE.$thumb;
			$record['videofile']=VIDEOS.$record['videoname'];
			$record['videoname']=substr($record['videoname'],0,20).'...';
			$videos[] =$record;
		}
		if($amount>=10)
			break;
	}
	return $videos;
}

$thumbRow =<<<TROW
<tr>
<td> </td>
<td >{THUMB1}</td><td >{THUMB2}</td>
<tr height="40"></tr>
</tr>
TROW;

$thumbBlock =<<<TBLK
<table width="100%" border="0" cellpadding="0"  cellspacing="2" >		
            <tr>
			<td width="131" height="70" rowspan="8"><div style="position:relative;"><img src="{THUMBIMAGE}" width="128" height="96" />
            <div style="position:absolute;top:0px;left:0px;">
			<a class="movieSmallPlay" onclick="showMovie({MOVIE});" href="javascript:;"> </a>
			</div></div></td>
		  	</tr>
            <tr class="theme">
              <td width="59"  >Thema<br /></td>
              <td width="308"  >{THEME}</td>
            </tr>
            <tr  class="detail">
              <td width="59" >Detail<br /></td>
              <td width="308"  >{DETAIL}</td>
            </tr>
            <tr class="detail">
              <td width="59"  >Bereich<br /></td>
              <td width="308"  >{BEREICH}</td>
            </tr>
            <tr class="detail">
              <td width="59"  >Name<br /></td>
              <td width="308"  >{NAME}</td>
            </tr>
            <tr class="detail">
              <td width="59"  >Länge<br /></td>
              <td width="308"  >{DURATION}</td>
            </tr>
            <tr class="detail">
              <td width="59"  >Datum<br /></td>
              <td width="308"  >{DATE}</td>
            </tr>
              <tr class="detail">
              <td width="59"  >Status<br /></td>
              <td width="308"  >&nbsp;</td>
            </tr>
            <tr>
            </table>
TBLK;

$thumbsInRow=0;
foreach($videos as $video)
{
	$thumb=$thumbBlock;
	$thumb=str_replace("{THUMBIMAGE}",'thumbsmall.jpg',$thumb);
	$thumb=str_replace("{THEME}",'name',$thumb);
	$thumb=str_replace("{DETAIL}",'name',$thumb);
	$thumb=str_replace("{BEREICH}",'name',$thumb);
		$thumb=str_replace("{NAME}",'name',$thumb);
	$thumb=str_replace("{DURATION}",'name',$thumb);
	$thumb=str_replace("{DATE}",'name',$thumb);

	if($thumbsInRow==0)
	{
		$rowofthumbs=str_replace("{THUMB1}",$thumb,$thumbRow);
	}
	if($thumbsInRow==1)
	{
		$rowofthumbs=str_replace("{THUMB2}",$thumb,$rowofthumbs);
	}
	$thumbsInRow++;
	if($thumbsInRow>1)
	{
		$rows.=$rowofthumbs;
		$thumbsInRow=0;
	}	
}
if($thumbsInRow==1)
	{
		$rowofthumbs=str_replace("{THUMB2}","",$rowofthumbs);
		$rows.=$rowofthumbs;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<meta name="generator" content="Adobe GoLive" />
<title>Abnahme</title>
<script type='text/javascript' src='js/jwplayer.js'></script>
<script type='text/javascript' src='js/jjquery-1.7.2.min.js'></script>
<script type="text/javascript">
		function playMovie(sFolder,sFilename,bAutostart) {
			jwplayer("mediaplayer").setup({
				autostart : bAutostart,
				shuffle : true,
				repeat : "none",
				bufferlength : 2,
				height : 424,
				width : 566,
				modes : [ {
					type : "flash",
					src : "player.swf",
					config : {
						file : "/KLVideos/"+ sFolder +"/" + sFilename + "_flash.xml"				}
				}, {
					type : "html5",
					config : {
						file : "/KLVideos/"+ sFolder +"/" + sFilename + "_html5.xml",
						autostart : true
					}
				}, ],
				"plugins" : {
					"gapro-2" : {
						"trackstarts" : true,
						"trackpercentage" : true,
						"tracktime" : true,
						"idstring": "||mediaid||--||file||"
					}
				},
				events : {
					onPlaylistItem : function(event) {
						//showItem(event);
					}
				}
			});
		}
		
		
		function showMovie(sFolder,sFilename,bTitle,bAutostart){
		 	var nWindowHeight = '445';
		 	if (bTitle) {
		 		nWindowHeight = '475'; //+30
		 	}
		 	
		 	
		 	$.openDOMWindow({ 
		        loader:1, 
		        loaderImagePath:'animationProcessing.gif', 
		        loaderHeight:16, 
		        loaderWidth:17, 
		        windowSourceID:'#FilmDiv' ,
		        height:nWindowHeight,  
		        width:590 ,
		        windowPadding:5,
		        windowBGColor:'#222222',
		        modal:1,
		        overlay:1
		 	});
		 	playMovie(sFolder,sFilename,bAutostart);	
		 	
	 	}
	 
	 	
	 	function closeit() {
	 		$.closeDOMWindow({
	 			windowSourceID:'#FilmDiv'
	 			});
	 		document.getElementById("mediaplayerContainer").innerHTML = '<div id="mediaplayer"></div>';
	 	}
	 	
	</script>
 <style type="text/css" media="all">
body {
	color: #fff;
	font-size: 9pt;
	font-family: Arial, Verdana, Helvetica, sans-serif;
	background-color: #262626;
}
.genTitle {
	font-size: 14pt;
}
.theme {
	font-size: 10pt;
}
.detail {
	font-size: 8pt;
}
a.movieSmallPlay {
    background-image: url("/img/smallplay.png");
    display: block;
    height: 35px;
    text-decoration: none;
    width: 35px;
}
a.movieSmallPlay:hover {
		background-image:url("/img/smallplay_hover.png");
	
	}
</style>
</head>

<body>
	<table  cellspacing="2" cellpadding="0" border="0" width="1200" align="center">
	<?php   echo "$rows";?>	</table>	
</body>

</html>