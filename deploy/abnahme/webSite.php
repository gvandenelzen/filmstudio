<?php
/*
 * Created on Mar 8, 2011
*
* To change the template for this generated file go to
* Window - Preferences - PHPeclipse - PHP - Code Templates

*/
define("PRIME", 16769023);
define("VIDEOS", "http://kloepfer.rushnewmedia.de/video-n66Q2/distribution-12998/stream/");
define("THUMBS", "/video-n66Q2/distribution-12998/thumbs/");
define("THUMB_POSTFIX", "_thumbsmall.jpg");
define('BASE','http://kloepfer.rushnewmedia.de');

require_once ('dbinclude.php');

$id = $_REQUEST['id'];
$script=$_SERVER['SCRIPT_NAME'];
$formaction="$script?id=$id";
$id=hexdec( $id );$websiteID=$id/PRIME;

if($_SERVER['REQUEST_METHOD']=='POST'){	foreach($_POST as $key=>$value)	{
		if( is_numeric($key))
		{			$ids[]=$key;			$ratings[]=$value;
		}			}	updateVideos($ids,$ratings);	updateWebSite($websiteID );}
$videoids=getVideosForWebsite($websiteID);$idsAsString=implode(',',$videoids);$website=getWebsite($websiteID);
if($website['RatingActive'] ==1)$ratingActive=true;if(!$website){	echo makeNoWebSite($title);
	exit;}
$title=$website['Title'];
if(!isset($title))
{
	$title="Klöpfer Filmstudio - Ihre Videos im Überblick ";

}$amount=count($videoids);if($amount==0){	$title=$website['Title'];	echo makeNoVideo($title);	exit;}$videos=getVideos($videoids);
$amount=count($videos);if($amount==0){		echo makeNoVideo($title);	exit;}


/*
 *
*/
function updateVideos($ids,$ratings){	for ($i=0;$i<count($ids);$i++)	{		$id=$ids[$i];		$rating=$ratings[$i];		updateVideo($id,$rating);	}}function updateWebSite($id)
{
	global $db;
	$ts = date("YmdHms");
	$sql = "UPDATE website SET RatingTimestamp=$ts WHERE ID=$id";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'update website rating' . $error);
	};

}function updateVideo($id,$rating){	global $db;	$ts = date("YmdHms");
	$sql = "UPDATE video SET UserRating=$rating,RatingTimestamp=$ts WHERE ID=$id";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'update user rating' . $error);
	};
}
function formatTS($ts)
{
	return substr($ts,6,2).'.'.substr($ts,4,2).'.'.substr($ts,0,4);
}

function formatDuration($ts)
{
	if($ts==0)
		return "";
	return $ts. " Sek.";
}
function getWebsite($websiteID) {
	global $db;	$ts = date("YmdHms");
	//$sql = "SELECT * FROM website WHERE  ID=$websiteID AND ExpireTimestamp>=$ts";
	$sql = "SELECT * FROM website WHERE  ID=$websiteID ";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'select all videos' . $error);
	};
	$record = mysql_fetch_assoc($sqlresult);
	return $record;
}function getVideosForWebsite($websiteID) {
	global $db;
	$sql = "SELECT VideoID FROM videoforwebsite WHERE  WebSiteID=$websiteID";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'select all videos' . $error);
	};
	while ($record = mysql_fetch_assoc($sqlresult) ) {

		$videoids[] =$record['VideoID'];

	}
	return $videoids;
}
function getVideos($ids){	foreach($ids as $id)	{
		$record=getVideo($id);		if($record)
		{
			$records[]=$record;
		}			}	return $records;}
function getVideo($id) {
	global $db;
	$sql = "SELECT video.ID as id,video.FileName as videoname,video.Length as duration,video.UserRating as rating,video.UpdateTimestamp as timestamp,theme.ThemaText as theme,theme.ID as ThID,theme.DetailText as detail ,people.FirstName as firstname, people.LastName as lastname , category.Name as category FROM video,theme,people, category WHERE  theme.ID=video.ThemeID AND people.ID=video.peopleID AND category.ID=theme.KategorieID AND video.ID=$id" ;
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'select all videos' . $error);
	};
	$record = mysql_fetch_assoc($sqlresult) ;
	$filename=$record['videoname'];
	
	$thumb=array_shift(explode(".",$filename));
	$thumb=THUMBS.$thumb.THUMB_POSTFIX;
	if(!file_exists('..'.$thumb))
	{
		return false;
	}
	$record['lastname']=htmlentities($record['lastname']);
	$record['firstname']=htmlentities($record['firstname']);
	$record['category']=htmlentities($record['category']);
	$record['theme']=htmlentities($record['theme']);	
	$record['detail']=htmlentities($record['detail']);
	if($record['detail']==0)
	{
		$record['detail']="";
	}
	$record['detail']=abreviate($record['detail'],80);
	$record['thumb']=BASE.$thumb;
	$record['videofile']=VIDEOS.$record['videoname'];
	$record['timestamp']=formatTS($record['timestamp']);
	$record['duration']=formatDuration($record['duration']);
	return $record;
}
function abreviate($test,$len)
{
	if(strlen($test)<=80)
		return $test;
	$newText="";
	$words=explode( " ",$test);
	foreach($words as $word)
	{
		$tmpText.=$word." ";
		if(strlen($tmpText)<$len)
		{
			$newText=$tmpText;
		}
		else
		{
			break;
		}
		
	}
	return $newText.'…';
}
$thumbRow =<<<TROW
<tr>
<td width="100"></td>
<td align='left'>{THUMB1}</td><td align='left' >{THUMB2}</td>
<tr height="40"></tr>
</tr>
TROW;

$thumbBlock =<<<TBLK
<table border="0" cellpadding="0"  cellspacing="2" align='left'>		
            <tr>
			<td width="135" height="96" rowspan="8"  align='left'  valign="top"><div style="position:relative;"><img src="{THUMBIMAGE}" width="128" height="96" />
            <div style="position:absolute;top:0px;left:0px;">
			<a class="movieSmallPlay"  href='javascript:showMovie({MOVIE});'> </a>
			</div></div></td>
		  	</tr>
            <tr class="theme">
              <td width="59" valign="top" align='left'>Thema<br /></td>
              <td width="308" valign="top" >{THEME}</td>
            </tr>
            <tr  class="detail">
              <td width="59" valign="top" >Detail<br /></td>
              <td width="308" valign="top" >{DETAIL}</td>
            </tr>
            <tr class="detail">
              <td width="59" valign="top" >Bereich<br /></td>
              <td width="308" valign="top" >{BEREICH}</td>
            </tr>
            <tr class="detail">
              <td width="59" valign="top" >Name<br /></td>
              <td width="308" valign="top" >{NAME}</td>
            </tr>
            <tr class="detail">
              <td width="59" valign="top" >Länge<br /></td>
              <td width="308" valign="top" >{DURATION}</td>
            </tr>
            <tr class="detail">
              <td width="59" valign="top" >Datum<br /></td>
              <td width="308" valign="top" >{DATE}</td>
            </tr>
              <tr class="detail">
              <td width="59"  >Status<br /></td>
              <td width="308"  >{SELECT}</td>
            </tr>
            <tr>
            </table>
TBLK;
$thumbsInRow=0;
foreach($videos as $record)
{

	$id=$record['id'];
	$theme=$record['theme'];
	$thumbimage	=$record['thumb'];
	$videofile=$record['videofile'];
	$detail=$record['detail'];
	$duration=$record['duration'];
	$name=$record['firstname'].' '.$record['lastname'];
	$bereich=$record['category'];
	$date=$record['timestamp'];
	$rating= $record['rating'];
	$thumb=$thumbBlock;
	if($ratingActive)
	{
	$select='<select name="{ID}" size="1">
								<option  {SELECTED0} value="0">OK</option>
								<option  {SELECTED1} value="1">nicht OK</option>
						</select>';
	}
	else
	{
		$select='';
	}
	$thumb=str_replace("{THUMBIMAGE}",$thumbimage,$thumb);
	$thumb=str_replace("{THEME}",$theme,$thumb);
	$thumb=str_replace("{DETAIL}",$detail,$thumb);
	$thumb=str_replace("{BEREICH}",$bereich,$thumb);
	$thumb=str_replace("{NAME}",$name,$thumb);
	$thumb=str_replace("{DURATION}",$duration,$thumb);
	$thumb=str_replace("{DATE}",$date,$thumb);
	$thumb=str_replace("{MOVIE}",enquote($videofile),$thumb);
	$thumb=str_replace("{SELECT}",$select,$thumb);
	
	$thumb=str_replace("{ID}",$id,$thumb);
	$selected0='';
	$selected1='';

	switch($rating)
	{
		case 0:
			$selected0='selected';
			break;
		case 1:
			$selected1='selected';
			break;
		default:
			$selected1='selected';
	}
	$thumb= str_replace ('{SELECTED0}' , $selected0 , $thumb);	$thumb= str_replace ('{SELECTED1}' , $selected1 , $thumb);
	if($thumbsInRow==0)
	{
		$rowofthumbs=str_replace("{THUMB1}",$thumb,$thumbRow);
	}
	if($thumbsInRow==1)
	{
		$rowofthumbs=str_replace("{THUMB2}",$thumb,$rowofthumbs);
	}
	$thumbsInRow++;
	if($thumbsInRow>1)
	{
		$rows.=$rowofthumbs;
		$thumbsInRow=0;
	}	
}
if($thumbsInRow==1)
	{
		$rowofthumbs=str_replace("{THUMB2}","",$rowofthumbs);
		$rows.=$rowofthumbs;
	}
$submit="";	
if($ratingActive)
	{	
		$submit='<input type="submit" name="submit" id="submit" value="Eingaben speichern" />';
	}
	
function makeNoVideo($title){	$novideos=<<<NOVIDEOS<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="content-type" content="text/html;charset=utf-8" /><meta name="generator" content="Adobe GoLive" /><title>$title</title><style type="text/css" media="all"><!--body {	color: #fff;	font-size: 9pt;	font-family: Verdana, Arial, Helvetica, sans-serif;	background-color: #262626;}.grau {	color: #CCC;	font-size: 9pt;	font-family: Verdana, Arial, Helvetica, sans-serif;}--></style></head><body>	<table cellspacing="2" cellpadding="0">		<tr>			<td colspan="4"><img src="logo.jpg" alt="" height="166" width="148"				border="0" /></td>		</tr>		<tr>			<td></td>			<td colspan="3"><span >Klöpfer Filmstudio </span>				<p>					<span>Unter dieser Adresse konnten leider keine Videos (mehr) gefunden werden. Bitte wenden Sie sich an das Team vom OnlineTerminal.</span>				</p></td>		</tr>	</table></body></html>NOVIDEOS;	return $novideos;}function makeNoWebSite($title)
{
	$novideos=<<<NOVIDEOS
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<meta name="generator" content="Adobe GoLive" />
<title>$title</title>
<style type="text/css" media="all">
<!--
body {
	color: #fff;
	font-size: 9pt;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	background-color: #262626;
}
-->
</style>
</head>
<body>
	<table cellspacing="2" cellpadding="0">
		<tr>
			<td colspan="4"><img src="logo.jpg" alt="" height="166" width="148"
				border="0" /></td>
		</tr>
		<tr>
			<td></td>
			<td colspan="3"><span >Klöpfer Filmstudio </span>
				<p>
					<span>Diese Website ist nicht mehr aktuell</span>
				</p></td>
		</tr>
	</table>
</body>
</html>
NOVIDEOS;
	return $novideos;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<meta name="generator" content="Adobe GoLive" />
<title><?php  echo "$title";?></title>
<script type='text/javascript' src='js/jwplayer.js'></script>
<script type='text/javascript' src='js/jquery-1.7.2.min.js'></script>
 <script type="text/javascript" src="js/jquery.DOMWindow.js"></script>
<script type="text/javascript">
		function showMovie(sFilename){		
		 	$.openDOMWindow({ 
		        windowSourceID:'#FilmDiv' ,
		        height:'445',  
		        width:590 ,
		        windowPadding:5,
		        windowBGColor:'#222222',
		        modal:1,
		        overlay:1
		 	});
		 		playMovie(sFilename);	
	 	}
	 
	 	
	 	function closeit() {
	 		$.closeDOMWindow({
	 			windowSourceID:'#FilmDiv'
	 			});
	 		document.getElementById("mediaplayerContainer").innerHTML = '<div id="mediaplayer"></div>';
	 	}
	 	
	 	function playMovie(sFilename) {
			 jwplayer('mediaplayer').setup({    			'flashplayer': 'player.swf',   			 'id': 'playerID',    			'width': '566',   			 'height': '424',
   			 autostart : true,			    		modes : [ {		type : "flash",		src : "player.swf",		config : {			file : sFilename		}	}, {		type : "html5",		config : {			file : sFilename		}	}, ]  });		}
	 	
	</script>
 <style type="text/css" media="all">
body {
	color: #fff;
	font-size: 9pt;
	font-family: Arial, Verdana, Helvetica, sans-serif;
	background-color: #262626;
}
.genTitle {
	font-size: 14pt;
}
.theme {
	font-size: 10pt;
}
.detail {
	font-size: 8pt;
}
a.movieSmallPlay {
    background-image: url("img/smallplay.png");
    display: block;
    height: 35px;
    text-decoration: none;
    width: 35px;
}
a.movieSmallPlay:hover {
		background-image:url("img/smallplay_hover.png");
	
	}
</style>
</head>

<body>

	<form id="form1" name="form1" method="post" action="<?php echo $formaction;?>">

	<table  cellspacing="2" cellpadding="0" border="0" width="100%" align="left">
	<tr>
			<td  colspan="1"><h2><strong><em><img src="logo.jpg" alt="" height="166"
				width="148" border="0" /></em></strong></h2></td>	<td class="dsR13" colspan="3" valign="top"></td>
		</tr>
				<tr>
			<td height="20"> </td>
		  <td colspan="2" valign="top"><p class="genTitle">Willkommen im Klöpfer Filmstudio. </p></td>
		</tr>
		<tr>
			<td height="20"> </td>
		  <td colspan="2" valign="top">Ihre Filme im Überblick. Zum Starten der Videos bitte auf den Play-Knopf klicken.</td>
		</tr>
				<tr><td></td>
    <td colspan="3" align="center" valign="middle"><hr /></td>   
  </tr>
	<?php   echo "$rows";?>	
	 <tr>
	 <td width="148"></td>
    		<td colspan="2"><?php   echo "$submit";?>	</td>
 	 </tr>
 	 </table>	

	</table>	
</form>
	<style type="text/css">
	#gc {
		margin: 0 auto;
		width: 566px;
		
	}
	</style>
	<div style="DISPLAY:none; visibility:hidden; height:1px;" id="FilmDiv">
	   <div style="text-align:right;"><a href="javascript:closeit();" ><img src="img/cross.gif" border="0" title="Fenster schließen"></a></div>

		<div class="gencontainer" id="gc">
			
			
			<div id="mediaplayerContainer">
			  <div id="mediaplayer">JW Player goes here</div>
			</div>
		</div>
	</div>
</body>

</html>