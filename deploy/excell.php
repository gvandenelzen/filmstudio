<?php


/*
 * 
ID
Filename
standort:standortname
mitarbeiter: fullname
kategorie:name
CatalogID
ProductID
ThemaID
Detailtext


 */
$excellfile = 'videos.csv';

require_once ('dbinclude.php');

$sql = "SELECT video.ID,video.FileName as videofile,video.CatalogID as catalogID,video.ProductID as productID, video.ThemaID as themaID,video.ThemaText as themaText ,video.DetailText as detail  FROM video WHERE   (video.MitarbeiterID=0 AND video.ThemaID=0  )";
$sqlresult = mysql_query($sql, $db);
if (!$sqlresult) {
	$error = mysql_error($db);
	echo "Error selecting video $error $sql";
	return;
}
while ($record = mysql_fetch_assoc($sqlresult)) {
	$records[]=$record;
}
$sql = "SELECT video.ID,video.FileName as videofile,standort.Name as location, mitarbeiter.FullName as employee ,kategorie.Name as category,video.CatalogID as catalogID,video.ProductID as productID, video.ThemaID as themaID,video.ThemaText as themaText  ,video.DetailText as detail  FROM video,mitarbeiter,standort,thema,kategorie WHERE   (mitarbeiter.ID=video.MitarbeiterID AND thema.ID=video.ThemaID  AND kategorie.ID=thema.KategorieID AND standort.ID=mitarbeiter.StandortID)";
$sqlresult = mysql_query($sql, $db);
if (!$sqlresult) {
	$error = mysql_error($db);
	echo "Error selecting video $error $sql";
	return;
}
while ($record = mysql_fetch_assoc($sqlresult)) {
	$records[]=$record;
}
$excell = "ID;Filename;Standort;Mitarbeiter;Kategorie;CatalogID;ProductID;ThemaID;Thematext;Detailtext\n";

foreach  ($records as $record) {
	$id = $record['ID'];
	$filename = enquote($record['videofile']);
	$location = enquote($record['location']);
	$employee = enquote($record['employee']);
	$category = enquote($record['category']);
	$catalogID=enquote($record['catalogID']);
	$productID=enquote($record['productID']);
	$themaID=enquote($record['themaID']);
	$thema=$record['themaText'];
	$thema = str_replace( chr(10),' ',$thema);
	$thema = str_replace( chr(13),' ',$thema);
	$thema=enquote($thema);
	$detail=$record['detail'];
	$detail = str_replace( chr(10),' ',$detail);
	$detail = str_replace( chr(13),' ',$detail);
	$detail=enquote($detail);
	$excell .= "$id;$filename;$location;$employee;$category;$catalogID;$productID;$themaID;$thema;$detail \n";
}
$excell=chop($excell);
$excell=utf8_encode($excell);
$handle = fopen($excellfile,'w');
fwrite($handle,$excell);
header("Location:$excellfile");
?>
