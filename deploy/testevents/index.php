<?php
error_reporting(1);
//Parameter handling and playlistname creation
// testflash.xml and testHTML5.xml are examples
// if file not there ask the external server to generate one

$playlistFlash='playlistflash.xml';
if(!file_exists($playlistFlash))
{
	$urlFlash='http://rushnewmedia.de/clients/kloepfer/integration/service/getPlaylist.php?target=flash';
	$contents=file_get_contents($urlFlash);
	$xml=fopen($playlistFlash,"w+");
	fwrite($xml , $contents);
}
$playlistHTML5='playlisthtml5.xml';
if(!file_exists($playlistHTML5))
{
	$urlHTML5='http://rushnewmedia.de/clients/kloepfer/integration/service/getPlaylist.php?target=html5';
	$contents=file_get_contents($urlHTML5);
	$xml=fopen($playlistHTML5,"w+");
	fwrite($xml , $contents);
}

$playlistFlash='playlistflash.xml';
$playlistFlash='playlisthtml5.xml';
$playlistHTML5='playlisthtml5.xml';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Video Player</title>
<style type="text/css">
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #FFF;
}

body {
	background-color: #3b0a00;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

#gc {
	margin: 0 auto;
	width: 640px;
}

#gen {
	height: 100%;
	width: 100% x;
	background-color: #3B0A00;
}
</style>
</head>

<body>
	<div id="gen">
		<p>&nbsp;</p>
		<div class="gencontainer" id="gc">
			<div id="listtitle">Title</div>
			<div id="listdesc">Description</div>
			<div id="mediaplayer">JW Player goes here</div>
		</div>
		<p>&nbsp;</p>
		<script type="text/javascript" src="jwplayer.js"></script>
		<script type="text/javascript">
		function showMovie(sFilename){		
		 	$.openDOMWindow({ 
		        windowSourceID:'#FilmDiv' ,
		        height:'445',  
		        width:590 ,
		        windowPadding:5,
		        windowBGColor:'#222222',
		        modal:1,
		        overlay:1
		 	});
		 		playMovie(sFilename);	
	 	}
	 
	 	
	 	function closeit() {
	 		$.closeDOMWindow({
	 			windowSourceID:'#FilmDiv'
	 			});
	 		document.getElementById("mediaplayerContainer").innerHTML = '<div id="mediaplayer"></div>';
	 	}
	 	
	 	function playMovie(sFilename) {
			 jwplayer('mediaplayer').setup({    			'flashplayer': 'player.swf',   			 'id': 'playerID',    			'width': '566',   			 'height': '424',
   			 autostart : true,			    		modes : [ {		type : "flash",		src : "player.swf",		config : {			file : sFilename		}	}, {		type : "html5",		config : {			file : sFilename		}	}, ]  });		}
	 	
	</script>
 
		<script type="text/javascript">
		jwplayer("mediaplayer").setup({
			autostart : true,
			shuffle : false,
			repeat : "none",
			bufferlength : 2,
			height : 480,
			width : 640,
			modes : [ {
				type : "flash",
				src : "player.swf",
				config : {
					file : <?php echo "\"$playlistFlash\"" ?>
				}
			}, {
				type : "html5",
				config : {
					file : <?php echo "\"$playlistHTML5\"" ?>,
					autostart : true
				}
			}, ],
			"plugins" : {
				"gapro-2" : {
					"trackstarts" : true,
					"trackpercentage" : true,
					"tracktime" : true
				}
			},
			events : {
				onPlaylistItem : function(event) {
					showÍtem(event);
				}
				,
		
			onComplete:function(event) {
					alert("END");	
				}

			}
		});
	</script>
		<!-- END OF THE PLAYER EMBEDDING  -->
		<p>&nbsp;</p>
		<p>&nbsp;</p>
	</div>
</body>
</html>
