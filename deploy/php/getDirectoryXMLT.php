<?php
error_reporting(1);
require_once ('dbinclude.php');
define ('PERSONS','2');
define ('SOLO','99');
//
$catid=$_REQUEST['catid'];
if(!isset($catid))
{
	return "<category>invalid</category>";
}
makeDirectoryXML($catid);
//
function makeDirectoryXML($catid)
{
	global $db;
	$xml='<category id="'.$catid."\">\n";;
	if($catid==SOLO)
	{
		$sql = "SELECT video.ID as ID  ,theme.ThemaCode as themecode, if(video.UpdateTimestamp>selectedvideo.TimeStamp, 		video.UpdateTimestamp, selectedvideo.TimeStamp)  as timestamp ,people.InternalID as peopleID 
		FROM video,theme,people,selectedvideo WHERE
		video.Active=1 AND
		theme.ID=video.ThemeID 
		AND people.ID=video.peopleID AND video.ID=selectedVideo.VideoID 
		ORDER BY themecode ,timestamp DESC,peopleID";
	}
	else
	{
	$sql = "SELECT video.ID as ID  ,theme.ThemaCode as themecode, video.UpdateTimestamp as timestamp ,people.InternalID as peopleID
		FROM video,theme,people WHERE
		video.Active=1 AND
		theme.ID=video.ThemeID AND theme.KategorieID=$catid
		AND theme.ThemaCode!='' AND theme.ThemaCode!='0'
		AND people.ID=video.peopleID
		ORDER BY themecode ,timestamp DESC,peopleID";
	}
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		$xml.=$error;
		$xml.="<category >\n";
		return $xml;
	}

	
	$num=mysql_num_rows($sqlresult);
	if($num>0)
	{
		while($record=mysql_fetch_assoc($sqlresult))
		{
			if($themecode!=$record['themecode'])
			{
				$timestamp=$record['timestamp'];
				$themecode=$record['themecode'];
			}
			$record['timestamp']=$timestamp;
			$records[]=$record;
		};
		asort($records);
		$themecode='';
		
		foreach ($records as $record)
		{
			if($themecode!=$record['themecode'])
			{
				if(strlen($themecode)>0)
				{
					$xml.="</theme>\n";
				}
				$xml.="<theme>\n";
				$timestamp=$record['timestamp'];
				$themecode=$record['themecode'];
				$xml.="<code>".wrap($themecode)."</code>\n";
				$xml.="<timestamp>".$timestamp."</timestamp>\n";
				
				if($catid==PERSONS || $catid==SOLO ){
					$peopleID=$record['peopleID'];
					if($peopleID!='' && $peopleID!=0)
					{
						$xml.="<person>$peopleID</person>\n";
						$xml.="<id>".$record['ID']."</id>\n";

					}
				}
			}
			else
			{
				if($catid==PERSONS || $catid==SOLO){
					if($peopleID!=$record['peopleID'])
					{
						$peopleID=$record['peopleID'];
						if($peopleID!='' && $peopleID!=0)
						{
							$xml.="<person>$peopleID</person>\n";
							$xml.="<id>".$record['ID']."</id>\n";
						}
					}
				}
			}
		};
		$xml.="</theme>\n";
	}
	$xml.="</category>\n";
	echo $xml;
}
function wrap($item)
{
	return "<![CDATA[".$item."]]>";
}
?>