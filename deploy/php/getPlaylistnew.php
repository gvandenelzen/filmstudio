<?php
error_reporting(1);
require_once ('dbinclude.php');
//
define ('FLASH','flash');
define ('HTML5','html5');
define('VIDEO_DIR','https://kloepfer.rushnewmedia.de/video-n66Q2/distribution-12998/stream/');
define('THUMBS_DIR','https://kloepfer.rushnewmedia.de/video-n66Q2/distribution-12998/thumbs/');
define('THUMB',"{THUMB}");
define('RTMP','rtmp://82.165.41.34/streamingstudio');
define ('RTMP_PATH',"distribution-12998/stream/");
define ('CATTHEME',"{CATTHEME}");
define('ANATAG',"<jwplayer:mediaid>".CATTHEME."</jwplayer:mediaid>\n");
//
define('VIDEO_FILE','{VIDEO}');
define('TITLE','{TITLE}');
define('DESC','{DESC}');
define ('START_RSS_FLASH',"<rss version=\"2.0\" xmlns:jwplayer=\"http://developer.longtailvideo.com/\" xmlns:media=\"http://search.yahoo.com/mrss/\">\n");
define ('START_RSS_HTML5',"<rss version=\"2.0\" xmlns:jwplayer=\"http://developer.longtailvideo.com/\" xmlns:media=\"http://search.yahoo.com/mrss/\">\n");

define ('THUMBNAIL'," \n<jwplayer:tags> ".THUMBS_DIR.THUMB."</jwplayer:tags>\n");
define ('FLASH_ITEM',"<item>\n<title>".TITLE."</title>\n<description>".DESC."</description>\n<jwplayer:file>".RTMP_PATH.VIDEO_FILE."</jwplayer:file>\n<jwplayer:streamer>".RTMP."</jwplayer:streamer>".THUMBNAIL.ANATAG."</item>\n");
define ('HTML5_ITEM',"<item>\n<title>".TITLE."</title>\n<description>".DESC."</description>\n<media:content url=\"".VIDEO_DIR.VIDEO_FILE."\">\n</media:content>".THUMBNAIL.ANATAG."</item>\n");
define ('CHANNEL_TITLE_START',"<channel>\n<title>Playlist</title>\n");
define ('CHANNEL_END',"</channel>\n");
define ('RSS_END',"</rss>\n");
//
$target=$_REQUEST['target'];
$catid=$_REQUEST['catid'];
$userid=$_REQUEST['userid'];
$standortid=$_REQUEST['standortid'];
$videoid=$_REQUEST['videoid'];
$themecode=$_REQUEST['themecode'];

if(isset($videoid))
{
	unset($catid);
	unset($userid);
	unset($standortid);
	unset($themecode);

}
$records=getAll($catid,$userid,$standortid,$themecode,$videoid);
$records=getPostRoll($records);
makePlaylist($records);
exit();


function makePlaylist($records)
{
	global $target;


	if( $target==HTML5)
	{
		$playlist=START_RSS_HTML5;
	}
	else
	{
		$playlist=START_RSS_FLASH;
	}

	$playlist.=CHANNEL_TITLE_START;
	foreach($records as $record)
	{
		$catid=$record['Category'];
		$themecode=utf8_encode($record['ThemaCode']);
		$cattheme="$catid--$themecode";
		$filename=utf8_encode($record['FileName']);
		$thumbname=str_replace('.mp4','_thumbbig.jpg',$filename);
		$title=utf8_encode($record['Webtext1']);
		if($title==0)
			$title="";
		$desc=utf8_encode($record['Webtext2']);
		if($desc==0)
			$desc="";
		if( $target==HTML5)
		{
			$item=HTML5_ITEM;
		}
		else
		{
			$item=FLASH_ITEM;
		}
		$item=str_replace(TITLE,$title,$item);
		$item=str_replace(DESC,$desc,$item);
		$videofilename=$filename;
		$item=str_replace(VIDEO_FILE,$videofilename,$item);
		$item=str_replace(THUMB,$thumbname,$item);
		$item=str_replace(CATTHEME,$cattheme,$item);
		$playlist.=$item;
		if($record['PostRoll'])
		{
			$postRollrecord=$record['PostRoll'];
			if( $target==HTML5)
			{
				$item=HTML5_ITEM;
			}
			else
			{
				$item=FLASH_ITEM;
			}
			$title=$postRollrecord['Title'];
			$desc="";
			$videofilename=$postRollrecord['FileName'];
			$item=str_replace(TITLE,$title,$item);
			$item=str_replace(DESC,$desc,$item);
			$item=str_replace(VIDEO_FILE,$videofilename,$item);
			$item=str_replace(THUMB,"",$item);
			$item=str_replace(CATTHEME,$cattheme,$item);
			$playlist.=$item;
		}
	}
	$playlist.=CHANNEL_END.RSS_END;
	echo $playlist;
}
function getPostRoll($records)
{
	global $db;
	foreach($records as $record)
	{
		$PostRollID=$record['PostRollID'];
		if($PostRollID!=0)
		{
			$sql = "SELECT *  FROM postroll WHERE ID=$PostRollID ";
			$sqlresult = mysql_query($sql, $db);

			if ($sqlresult) {			
				$num=mysql_num_rows($sqlresult);
				if($num>0)
				{
					$record['PostRoll']=mysql_fetch_assoc($sqlresult);
				}
			}
		}
		$newrecords[]=$record;
	}
	return $newrecords;
}


function getAll($catid,$userid,$standortid,$themecode,$videoid)
{
	global $db;
	if(isset($standortid))
	{
		$sql = "SELECT ID  FROM location WHERE InternalID=$standortid";
		$sqlresult = mysql_query($sql, $db);

		if (!$sqlresult) {
			$error = mysql_error($db);
			return $records;
		}
		$num=mysql_num_rows($sqlresult);
		if($num>0)
		{
			$record=mysql_fetch_assoc($sqlresult);
			$standortid=$record['ID'];
		}
		else
		{
			return $records;
		}
	}

	if(isset($userid))

	{

		$sql = "SELECT ID  FROM people WHERE InternalID=\"$userid\" ";
		$sqlresult = mysql_query($sql, $db);

		if (!$sqlresult) {

			$error = mysql_error($db);
			return $records;

		}

		$num=mysql_num_rows($sqlresult);
		if($num>0)
		{
			$record=mysql_fetch_assoc($sqlresult);
			$userid=$record['ID'];
		}
		else
		{
			return $records;

		}
	}

	$userclause="";
	if(isset($userid))
		$userclause="AND video.PeopleID=$userid";

	$categoryclause="";
	if(isset($catid))
		$categoryclause=" AND theme.KategorieID=$catid" ;

	$themeclause="";
	if(isset($themecode))
		$themeclause=" AND theme.ThemaCode=\"$themecode\" ";

	$locationclause="";
	if(isset($standortid))
		$locationclause=" AND (videoforlocation.LocationID=$standortid AND videoforlocation.videoID=video.ID)";
	
	$idclause="";	
	if(isset($videoid))
		$idclause=" AND video.ID=$videoid";

	$sql = "SELECT video.ID as ID

	FROM video,theme,people,videoforlocation

	WHERE

	video.peopleID=people.ID
	$locationclause

	$userclause AND

	theme.ID=video.ThemeID

	$categoryclause

	$themeclause

	$idclause

	AND video.active=1";

	$sqlresult = mysql_query($sql, $db);

	if (!$sqlresult) {
		$error = mysql_error($db);
		return $records;
	}
	else
	{
		while($vidrecord=mysql_fetch_assoc($sqlresult))
		{
			$vidrecords[]=$vidrecord;

		};
	}
	asort($vidrecords);
	foreach($vidrecords as $vidrecord )
	{
		if($videoID==$vidrecord['ID'])
			continue;
		//echo "$videoID <br>";
		$videoID=$vidrecord['ID'];
		$sql = "SELECT video.PostRollID as PostRollID,theme.ThemaCode as ThemaCode, theme.Webtext1 as Webtext1, theme.Webtext2 as Webtext2 ,category.Prefix as Category,video.FileName as FileName

		FROM video,theme,category,people,videoforlocation

		WHERE
		video.ID=$videoID AND (

		video.peopleID=people.ID

		$locationclause

		$userclause AND

		theme.ID=video.ThemeID
		AND theme.KategorieID=category.ID

		$categoryclause

		$themeclause

		AND video.active=1)";
		$sqlresult = mysql_query($sql, $db);
		if (!$sqlresult) {

			$error = mysql_error($db);

			return $records;

		}
		else

		{
			$record=mysql_fetch_assoc($sqlresult);

			$records[]=$record;

		}
	}
	return $records;
}
function wrap($item)

{
	return "<![CDATA[".$item."]]>";

}
?>