<?php
error_reporting(1);

function sendRequestResult($request, $data) {
global $compress;
	if($compress==1)
	{
		$data=gzdeflate($data,9);
		$data= base64_encode ( $data );		
	}
	$result = '<?xml version="1.0" encoding="UTF-8" ?>';
	$result .= "<result>\n<status>ok</status>\n<request>$request</request>\n<data>$data</data><zlib>$compress</zlib>\\n</result>\n";
	echo $result;
	exit ();
}
function sendRequestError( $request, $error) {
	$result = '<?xml version="1.0" encoding="UTF-8" ?>';
	$result .= "<result>\n<status>error</status>\n<request>$request</request>\n<data>$error</data>\n</result>\n";
	echo $result;
	exit ();
}

function createDir($dir) {
	if (!file_exists($dir)) {
		mkdir($dir, 0777, true);
		chmod($dir, 0777);
	}
	if (!file_exists($dir)) {
		sendRequestError( 'createdir', 'notexists'.$dir);
	}
}
?>
