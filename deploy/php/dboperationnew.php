<?php


/*
 * Created on Mar 8, 2011
*
* To change the template for this generated file go to
* Window - Preferences - PHPeclipse - PHP - Code Templates

*/
define('ALERT_HARBOTT','alertHarbott.php');
define("VIDEOS", "../video-n66Q2/");
require_once ('dbinclude.php');
$request = $_REQUEST['request'];
$ID = $_REQUEST['id'];
$fmsfile = $_REQUEST['idfmsfile'];
$data = ($_REQUEST['data']);
$compress = $_REQUEST['compress'];
if (get_magic_quotes_gpc()) {
	$data = stripslashes($data);
}
switch ($request) {
	case 'getbasetables' :
		getBasetables($request);
		break;
	case 'getinterviews':
		$table='interview';
		getTableRecords($request,$table);
		break;
	case 'getthemes' :
		getThemes($request);
		break;
	case 'getrecordings' :
		getRecordings($request);
		break;
	case 'getvideos' :
		getVideos($request);
		break;
	case 'getvideoedit' :
		getVideoEdit($request);
		break;
	case 'getvideoforlocation':
		$table='videoforlocation';
		getTableRecords($request,$table);
		break;
	case 'getvideoforwebsite':
		$table='videoforwebsite';
		getTableRecords($request,$table);
		break;
	case 'getwebsites':
		$table='website';
		getTableRecords($request,$table);
		break;
		//updatevideoforwebsite
	case 'getrecordingfiles':
		getRecordingfiles($request,$ID);
		break;
	case 'getrecording' :
		$table='recording';
		getRecord($request,$table,$ID);
		break;
	case 'getrecordingfile':
		$table='recordingfile';
		getRecord($request,$table,$ID);
		break;
	case 'gettheme' :
		$table='theme';
		getRecord($request,$table,$ID);
		break;
	case 'getinterview' :
		$table='interview';
		getRecord($request,$table,$ID);
		break;
	case 'getpeople' :
		$table='people';
		getRecord($request,$table,$ID);
		break;
	case 'getwebsites' :
		$table='website';
		getRecord($request,$table,$ID);
		break;
	case 'updatevideo' :
		$table="video";
		updateRecord($request, $data,$table);
		alertHarbott($data);
		break;
	case 'updatevideoedit' :
		$table="videoedit";
		updateRecord($request, $data,$table);
		break;
	case 'updatetheme' :
		$table="theme";
		updateRecord($request, $data,$table);
		break;
	case 'updatewebsite' :
		$table="website";
		updateRecord($request, $data,$table);
		break;
	case 'updaterecording' :
		$table="recording";
		updateRecord($request, $data,$table);
		break;
	case 'deleterecording' :
		deleteRecording($request, $ID);
		break;
	case 'updaterecordingfile':
		$table="recordingfile";
		updateRecord($request, $data,$table);
		break;
	case 'updateencodingjob':
		$table="encodingjobs";
		updateRecord($request, $data,$table);
		break;
	case 'updateinterview' :
		$table="interview";
		updateRecord($request, $data,$table);
		break;
	case 'updatepeople' :
		$table="people";
		updateRecord($request, $data,$table);
		break;
	case 'updatelocationsforvideo' :
		updateLocationsForVideo($request,$ID, $data);
		break;
	case 'updatevideoforwebsite' :
		updateVideoForWebsite($request,$ID, $data);
		break;
	case 'removeencodingjob':
		removeEncodinJob($request, $ID,$fmsfile);
		break;
	case 'deletefmsfile':
		removeFMSFile($request, $fmsfile);
		break;
	case 'uploadfile':
		uploadFile($request,  $_FILES);
		break;
	case  'updateselectedvideo':
		$table="selectedvideo";
		updateRecord($request, $data,$table);
		break;
	case 'getrecordingvideos' :
		getRecordingVideos($request);
		break;
	default :
		sendRequestError($request, 'invalidrequest');
}
/*
 *
*/
function alertHarbott($data)
{
	$record = json_decode($data, true);
	$php=ALERT_HARBOTT.('test');
	echo $php;
}
function getBasetables($request) {
	global $db;
	$basetables=array('postroll','people','category','region','location','session','quality','filelocation','statusinterview','statusrecording','statusencodingjob');
	foreach($basetables as $table)
	{
		$json[$table] = array ();
		$sql = "SELECT * FROM $table ";
		$sqlresult = mysql_query($sql, $db);
		if (!$sqlresult) {
			$error = mysql_error($db);
			sendRequestError($request, "select: $table $error");
		};
		while ($record = mysql_fetch_assoc($sqlresult)) {
			foreach ($record as $key => $value) {
				$record[$key] = urlencode( $value);;
			}
			$json[$table][] = $record;
		}
	}
	sendRequestResult($request, json_encode($json));
	exit ();
}
function getTableRecords($request,$table) {
	global $db;
	if($table=='interview')
		$jsonname='interviews';
	else
		$jsonname=$table;
	$json[$jsonname] = array ();

	$sql = "SELECT * FROM $table ";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, "select all $table" . $error);
	};

	while ($record = mysql_fetch_assoc($sqlresult)) {
		foreach ($record as $key => $value) {
			$record[$key] = urlencode( $value);;
		}
		$json[$jsonname][] = $record;
	}
	sendRequestResult($request, json_encode($json));
	exit ();
}
function deleteRecord($request,$table,$id) {
	global $db;
	$sql = "DELETE FROM $table WHERE ID=$id";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, "delete $table" . $error);
	};
	sendRequestResult($request,'');
	exit ();
}
function getVideoEdit($request) {
	global $db;
	$json['videoedit'] = array ();
	$sql = "SELECT * FROM videoedit ";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'select all videoedit' . $error);
	};

	while ($record = mysql_fetch_assoc($sqlresult)) {
		foreach ($record as $key => $value) {
			$record[$key] = urlencode( $value);;
		}
		$json['videoedit'][] = $record;
	}
	sendRequestResult($request, json_encode($json));
	exit ();
}
function getVideos($request) {
	global $db;
	$json['video'] = array ();
	$sql = "SELECT * FROM video ";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'select all videos' . $error);
	};

	while ($record = mysql_fetch_assoc($sqlresult)) {
		foreach ($record as $key => $value) {
			$record[$key] = urlencode( $value);;
		}
		$json['video'][] = $record;
	}
	sendRequestResult($request, json_encode($json));
	exit ();
}
function getRecordings($request) {
	global $db;
	$json['recordings'] = array ();
	$sql = "SELECT * FROM recording ";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'select all recordings' . $error);
	};

	while ($record = mysql_fetch_assoc($sqlresult)) {
		foreach ($record as $key => $value) {
			$record[$key] = urlencode( $value);;
		}
		$json['recordings'][] = $record;
	}
	sendRequestResult($request, json_encode($json));
	exit ();
}
function getThemes($request) {
	global $db;
	$json['themes'] = array ();
	$sql = "SELECT * FROM theme";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'select thema' . $error);
	};

	while ($theme = mysql_fetch_assoc($sqlresult)) {
		foreach ($theme as $key => $value) {
			$theme[$key] = urlencode( $value);;
		}
		$json['themes'][] = $theme;
	}
	sendRequestResult($request, json_encode($json));
	exit ();
}

function getRecordingfiles($request,$id) {
	global $db;
	$json['recordingfile'] = array ();
	$sql = "SELECT * FROM recordingfile WHERE RecordingID=$id ";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, "select recordingfiles sql:$sql Error: $error<br>");
	};

	while ($record = mysql_fetch_assoc($sqlresult)) {
		foreach ($record as $key => $value) {
			$record[$key] = urlencode( $value);;
		}
		$json['recordingfile'][] = $record;
	}
	sendRequestResult($request, json_encode($json));
	exit ();
}
function getRecord($request,$table,$id) {	
	global $db;
	$sql = "SELECT * FROM $table WHERE ID=$id ";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, "select $table sql:$sql Error: $error<br>");
	};
	
	$record = mysql_fetch_assoc($sqlresult);
	foreach ($record as $key => $value) {
		$log.="$key $value\n";
		$record[$key] = urlencode( $value);
	}
	$json=json_encode($record);
	$log.=$json;
	file_put_contents ( 'log.txt' , $log,FILE_APPEND );
	sendRequestResult($request, $json);
	exit ();
}

function deleteRecording($request,$id) {
	global $db;
	$sql = "DELETE FROM recording WHERE ID=$id";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'delete recording' . $error);
	};
	$sql = "DELETE FROM recordingfiles WHERE RecordingID=$id";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'delete recording' . $error);
	};
	$json['recording'] = array ();
	sendRequestResult($request,json_encode($json));
	exit ();
}
function removeEncodinJob($request, $ID,$fmsfile)
{
	global $db;
	$sql = "DELETE FROM encodingjobs WHERE ID=$ID";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, "delete encodingjobs >SQL  $sql ERROR $error");
	};
	$fmsfile=VIDEOS.$fmsfile;
	if(file_exists($fmsfile))
	{
		unlink($fmsfile);
	}
	$json['encodingjobs'] = array ();
	sendRequestResult($request,json_encode($json));
	exit ();
}

function removeFMSFile($request,$fmsfile)
{
	$fmsfile=VIDEOS.$fmsfile;
	if(file_exists($fmsfile))
	{
		unlink($fmsfile);
	}
	$json['fmsfile'] = array ();
	sendRequestResult($request,json_encode($json));
	exit ();
}
function updateLocationsForVideo($request,$videoID, $data)
{
	global $db;
	mysql_query('SET NAMES utf8', $db);
	$sql = "DELETE  FROM  videoforlocation WHERE VideoID=$videoID";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, "delete: $table sql:  $sql error: $error");
	};
	$records = json_decode($data, true);
	foreach ($records as $locationID) {
		$sql = "INSERT INTO  videoforlocation (VideoID,LocationID) VALUES ($videoID,$locationID)";
		$sqlresult = mysql_query($sql, $db);

		if (!$sqlresult) {
			$error = mysql_error($db);
			sendRequestError($request, "update: $table sql:  $sql error: $error");
		};
	}
	$sql = "SELECT * FROM videoforlocation WHERE VideoID=$videoID;";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'select' . $error);
	};
	$json['videoforlocation'] = array ();
	while ($record = mysql_fetch_assoc($sqlresult)) {
		foreach ($record as $key => $value) {
			$record[$key] = urlencode( $value);;
		}
		$json['videoforlocation'][] = $record;
	}
	sendRequestResult($request, json_encode($json));
}
function updateVideoForWebsite($request,$websiteID, $data)
{
	global $db;
	mysql_query('SET NAMES utf8', $db);
	$sql = "DELETE  FROM  videoforwebsite WHERE WebSiteID=$websiteID";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, "delete: $table sql:  $sql error: $error");
	};
	$records = json_decode($data, true);
	foreach ($records as $videoID) {
		$sql = "INSERT INTO  videoforwebsite (VideoID,WebSiteID) VALUES ($videoID,$websiteID)";
		$sqlresult = mysql_query($sql, $db);

		if (!$sqlresult) {
			$error = mysql_error($db);
			sendRequestError($request, "update: $table sql:  $sql error: $error");
		};
	}
	$sql = "SELECT * FROM videoforwebsite WHERE WebSiteID=$websiteID;";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'select' . $error);
	};
	$json['videoforwebsite'] = array ();
	while ($record = mysql_fetch_assoc($sqlresult)) {
		foreach ($record as $key => $value) {
			$record[$key] = urlencode( $value);;
		}
		$json['videoforwebsite'][] = $record;
	}
	sendRequestResult($request, json_encode($json));
}
/*
 * generische uodate function
*/
function updateRecord($request, $data,$table) {
	global $db;
	mysql_query('SET NAMES utf8', $db);
	$record = json_decode($data, true);
	if (!$record) {
		sendRequestError($request, 'json' . $data);
	}
	if (!isset ($record['ID']) || $record['ID'] == "" || $record['ID'] == 0) {
		unset ($record['ID']);
	} else {
		$recordID = $record['ID'];
	}
	foreach ($record as $key => $value) {
		$columns[] = $key;
		$value = enquote($value);
		$values[] = $value;
		$colvals[] = "$key=$value";
	}

	if (isset ($recordID)) {
		$recordcolvals = implode($colvals, ',');
		$sql = "UPDATE $table SET $recordcolvals WHERE ID = $recordID;";
		$sqlresult = mysql_query($sql, $db);
	} else {
		$recordcols = implode($columns, ',');
		$recordvals = implode($values, ',');
		$sql = "INSERT INTO  $table ($recordcols) VALUES ($recordvals)";
		$sqlresult = mysql_query($sql, $db);
		$recordID = mysql_insert_id();
	}
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, "update: $table sql:  $sql error: $error");
	};
	
		$sql = "SELECT * FROM $table WHERE ID=$recordID ";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, "select $table sql:$sql Error: $error<br>");
	};
	/*
		Because of mysql_query('SET NAMES utf8', $db);
		we have to utf8_decode;
	*/
	$record = mysql_fetch_assoc($sqlresult);
	foreach ($record as $key => $value) {
		$log.="$key $value\n";
		$record[$key] = urlencode( utf8_decode($value));
	}
	$json=json_encode($record);
	$log.=$json;
	file_put_contents ( 'log.txt' , $log,FILE_APPEND );
	sendRequestResult($request, $json);
}
function uploadfile($request,$files)
{
	if (!is_uploaded_file($files['Filedata']['tmp_name'])) {
		sendRequestError('uploadfile', 'nofiletouploaded');
	}
	$filename = $files['Filedata']['name'];
	$filename=str_replace('#','/',$filename);
	$uploadfile = "../".$filename;
	$filedir=pathinfo($uploadfile,PATHINFO_DIRNAME );
	if(!file_exists($filedir))
	{
		mkdir($filedir, 0777, true);
	}
	if (move_uploaded_file($files['Filedata']['tmp_name'], $uploadfile)) {
		if(file_exists($uploadfile))
		{
			chmod($uploadfile, 0666);
			sendRequestResult($request, $uploadfile);
		}
		else {
			sendRequestError($request, $uploadfile.'filenotmoved');
		}
	} else {
		sendRequestError($request, $uploadfile.'filenotuploaded');
	}

}

function getRecordingVideos($request) {
	global $db;
	$sql = "SELECT recording.ID as recID FROM recording,videoedit,video WHERE recording.ID=videoedit.RecordingID AND video.VideoEditID=videoedit.ID AND video.StatusEncodingJob=9 ORDER BY recID";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'select all recordings' . $error);
	};
	$total=0;
	$recordingvideos=array();
	while ($record = mysql_fetch_assoc($sqlresult)) {
		$recID=$record['recID'];
		if(isset($lastrecID) && $lastrecID!=$recID)
		{
			$recordingvideos[$recID]=$total;
			$total=0;
		}
		$lastrecID=$recID;
		$total++;
	}
	$json['recordingvideos']=$recordingvideos;
	sendRequestResult($request,json_encode($json));
	exit ();
}
?>
