<?php


/*
 * Created on May 5, 2011
 *
CREATE TABLE `video` (
  `ID` int(11) NOT NULL auto_increment,
  `FileName` varchar(30) collate utf8_unicode_ci NOT NULL,
  `StandOrtID` int(11) NOT NULL,
  `MitarbeiterID` int(11) NOT NULL,
  `CatalogID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `ThemaText` varchar(80) collate utf8_unicode_ci NOT NULL,
  `DetailText` varchar(80) collate utf8_unicode_ci NOT NULL,
  `RecordingStatus` tinyint(1) NOT NULL,
  `Notes` varchar(80) collate utf8_unicode_ci NOT NULL,
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE `recording` (
  `ID` int(11) NOT NULL auto_increment,
  `TimestampStart` bigint(20) NOT NULL,
  `Duration` bigint(20) NOT NULL,
  `ThemaID` int(11) NOT NULL,
  `FileName` varchar(80) collate latin1_general_ci NOT NULL,
  `MitarbeiterID` int(11) NOT NULL,
  `StatusID` int(11) NOT NULL,
  `AufnahmeModus` varchar(20) collate latin1_general_ci NOT NULL,
  `Notizen` text collate latin1_general_ci NOT NULL,
  PRIMARY KEY  (`ID`),
  KEY `TimestampStart` (`TimestampStart`),
  KEY `FileName` (`FileName`)
  
Produktempfehlung-20110412_144625.mp4
 */
require_once ('dbinclude.php');
$recordingsdir = 'D:\Program Files\FlashMediaServer3\applications\kloepfer\streams\final';

$files = getFilesInDir($recordingsdir);
foreach ($files as $file) {
	/*
	 * Check if entry exists when yes skip
	 */
	$sql = "SELECT FileName FROM video WHERE FileName=\"$file\" ";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		echo "Error selecting video $error";
	};
	if (mysql_num_rows($sqlresult) != 0) {
		continue;
	}
	$parts = explode('-', $file);
	$fileName = array_pop($parts);
	$fileName = str_replace('.mp4', '', $fileName);
	$fileName = 'F' . $fileName;
	/*
	 * Get the baserecording
	 */
	$sql = "SELECT * FROM recording WHERE FileName=\"$fileName\" ";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		echo "Error selecting recording $error";
	};
	/*
	 * if no baserecording then empty fields in video
	 */
	if (mysql_num_rows($sqlresult) == 0) {
		//	continue;
	}
	$recording = mysql_fetch_assoc($sqlresult);
	updateVideoTable($file, $recording);

}
function updateVideoTable($file, $recording) {
	global $db;
	$file=enquote($file);
	if ($recording) {
		$recordingStatus = $recording['StatusID'];
		$notes = enquote($recording['Notizen']);
		$employee = $recording['MitarbeiterID'];
		$themeID = $recording['ThemaID'];
		$sql = "SELECT thema.CatalogID,thema.ProductID,thema.ThemaText,thema.DetailText,mitarbeiter.StandOrtID FROM mitarbeiter,thema,standort WHERE mitarbeiter.ID=$employee AND thema.MitarbeiterID=$employee AND thema.ID= $themeID";
		$sqlresult = mysql_query($sql, $db);
		if (!$sqlresult) {
			$error = mysql_error($db);
			echo "Error selecting theme $error";
		}
		$theme = mysql_fetch_assoc($sqlresult);
		$catalogID = $theme['CatalogID'];
		$productID = $theme['ProductID'];
		$themeText = enquote($theme['ThemaText']);
		$detailText =enquote($theme['DetailText']);
		$location = $theme['StandOrtID'];
	}
	else
	{
		$employee=0;
		$recordingStatus=0;
		$notes = enquote("");
		$themeText= enquote("");
		$detailText= enquote("");
		$themeID=0;
		$location=0;
		$catalogID=0;
		$productID=0;
		
	}
	$sql = "INSERT INTO `video` (`ID`, `FileName`, `StandOrtID`, `MitarbeiterID`, `ThemaID`,`CatalogID`, `ProductID`, `ThemaText`, `DetailText`, `RecordingStatus`, `Notes`) VALUES (NULL, $file, $location, $employee, $themeID,$catalogID, $productID, $themeText, $detailText, $recordingStatus, $notes);" ;
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		echo "Error inserting  video $error, $sql";
	}
	else
	{
			echo " new videorecord :$file $location $employee $catalogID $productID $themeText $detailText $recordingStatus $notes <br>";
		
	}
}

function getFilesInDir($dir) {
	if ($handle = opendir($dir)) {
		$files = array ();
		while (false !== ($file = readdir($handle))) {
			//.mp4
			if ($file != "." && $file != ".." && $file != "Thumbs.db" && $file != ".DS_Store") {
				$files[] = $file;
			}
		}
		closedir($handle);
	}
	return $files;
}
?>
