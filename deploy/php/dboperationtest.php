<?php


/*
 * Created on Mar 8, 2011
*
* To change the template for this generated file go to
* Window - Preferences - PHPeclipse - PHP - Code Templates

*/

require_once ('dbinclude.php');
$request = $_REQUEST['request'];
$ID = $_REQUEST['id'];
$fmsfile = $_REQUEST['idfmsfile'];
$data = ($_REQUEST['data']);
if (get_magic_quotes_gpc()) {
	$data = stripslashes($data);
}
switch ($request) {
	case 'getrecordingvideos' :
		getRecordingVideos($request);
		break;
	default :
		sendRequestError($request, 'invalidrequest');
}
/*
 *
*/

function getRecordingVideos($request) {
	global $db;
	$sql = "SELECT recording.ID as recID FROM recording,videoedit,video WHERE recording.ID=videoedit.RecordingID AND video.VideoEditID=videoedit.ID AND video.StatusEncodingJob=9 ORDER BY recID";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'select all recordings' . $error);
	};
	$total=0;
	$recordingvideos=array();
	while ($record = mysql_fetch_assoc($sqlresult)) {
		$recID=$record['recID'];
		if(isset($lastrecID) && $lastrecID!=$recID)
		{
			$recordingvideos[$recID]=$total;
			$total=0;
		}
		$lastrecID=$recID;
		$total++;
	}
	$json['recordingvideos']=$recordingvideos;
	sendRequestResult($request,json_encode($json));
	exit ();
}
?>
