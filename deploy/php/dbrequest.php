<?php


/*
 * Created on Mar 8, 2011
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates

 */
require_once ('dbinclude.php');
$request = $_REQUEST['request'];
$catalogID = $_REQUEST['catalogID'];
$productID = $_REQUEST['productID'];
$themeID = $_REQUEST['themeID'];
$data = ($_REQUEST['data']);
if (get_magic_quotes_gpc()) {
	$data = stripslashes($data);
}
switch ($request) {
	case 'getthemes' :
		getThemes($request);
		break;
	case 'gettheme' :
		getTheme($request, $data);
		break;
	case 'getselectedthemes' :
		getThemeSelection($request, $catalogID, $productID);
	case 'getrecordings' :
		getRecordings($request, $themeID);
		break;
	case 'updatetheme' :
		updateTheme($request, $data);
		break;
	case 'updaterecording' :
		updateRecording($request, $data);
		break;
	case 'getbasetables' :
		getBasetables($request);
		break;

	default :
		sendRequestError($request, 'invalidrequest');
}
/*
 * 
 */
function getBasetables($request) {
	global $db;
	$json['employees'] = array ();
	$json['categories'] = array ();
	$json['locations'] = array ();
	$sql = "SELECT * FROM mitarbeiter ";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'select employee' . $error);
	};
	while ($employee = mysql_fetch_assoc($sqlresult)) {
		foreach ($employee as $key => $value) {
			$employee[$key] = htmlentities(addslashes($value));
		}
		$json['employees'][] = $employee;
	}

	$sql = "SELECT * FROM standort ";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'select location' . $error);
	};
	while ($location = mysql_fetch_assoc($sqlresult)) {
		foreach ($location as $key => $value) {
			$location[$key] = htmlentities(addslashes($value));
		}

		$json['locations'][] = $location;
	}

	$sql = "SELECT * FROM kategorie ";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'select category' . $error);
	};
	while ($category = mysql_fetch_assoc($sqlresult)) {
		foreach ($category as $key => $value) {
			$category[$key] = htmlentities(addslashes($value));
		}
		$json['categories'][] = $category;
	}
	sendRequestResult($request, json_encode($json));
	exit ();
}
function getRecordings($request, $themeID) {
	global $db;
	if (!isset ($themeID)) {
		sendRequestError($request, 'get recordings' . 'themeID empty');
	}
	$json['recordings'] = array ();
	$sql = "SELECT * FROM recording WHERE `ThemaID` =$themeID ";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'select recordings' . $error);
	};

	while ($recording = mysql_fetch_assoc($sqlresult)) {
		foreach ($recording as $key => $value) {
			$recording[$key] = htmlentities(addslashes($value));
		}
		$json['recordings'][] = $recording;
	}
	sendRequestResult($request, json_encode($json));
	exit ();
}
function updateRecording($request, $data) {
	global $db;
	mysql_query('SET NAMES utf8', $db);
	$recording = json_decode($data, true);
	if (!$recording) {
		sendRequestError($request, 'json' . $data);
	}
	if (!isset ($recording['ID']) || $recording['ID'] == "" || $recording['ID'] == 0) {
		unset ($recording['ID']);
	} else {
		$recordingID = $recording['ID'];
	}
	foreach ($recording as $key => $value) {
		$columns[] = $key;
		$value = enquote($value);
		$values[] = $value;
		$colvals[] = "$key=$value";
	}

	if (isset ($recordingID)) {
		$reccolvals = implode($colvals, ',');
		$sql = "UPDATE `recording` SET $reccolvals WHERE `id` = $recordingID;";
		$sqlresult = mysql_query($sql, $db);
	} else {
		$reccols = implode($columns, ',');
		$recvals = implode($values, ',');
		$sql = "INSERT INTO `recording` ($reccols) VALUES ($recvals)";
		$sqlresult = mysql_query($sql, $db);
		$recordingID = mysql_insert_id();
	}
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'updateRecording' . $sql . $error);
	};
	$themeID = $recording['ThemaID'];
	$json['recordings'] = array ();
	$sql = "SELECT * FROM recording WHERE `ThemaID` =$themeID ";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'select recording' . $error . $sql);
	};
	while ($recording = mysql_fetch_assoc($sqlresult)) {
		foreach ($recording as $key => $value) {
			$recording[$key] = htmlentities(addslashes(utf8_decode($value)));
		}
		$json['recordings'][] = $recording;
	}
	sendRequestResult($request, json_encode($json));
}
function getThemeSelection($request, $catalogID, $productID) {
	global $db;

	if (!isset ($catalogID) || !isset ($productID)) {
		sendRequestError($request, 'get selected thema' . 'ProductID or CatalogID empty');
	}

	$sql = "SELECT * FROM thema WHERE `ProductID` = $productID AND  `CatalogID`=$catalogID";

	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'select themas' . $error);
	};

	$json['themes'] = array ();
	while ($theme = mysql_fetch_assoc($sqlresult)) {
		foreach ($theme as $key => $value) {
			$theme[$key] = htmlentities(addslashes($value));
		}
		$json['themes'][] = $theme;
	}
	sendRequestResult($request, json_encode($json));
	exit ();

}
function getTheme($request, $data) {
	global $db;
	if (!isset ($data['CatalogID']) || $data['CatalogID'] == "") {
		unset ($data['CatalogID']);
	} else {
		$catalogID = $data['CatalogID'];
	}
	if (!isset ($data['ProductID']) || $data['ProductID'] == "") {
		unset ($data['ProductID']);
	} else {
		$productID = $data['ProductID'];
	}
	if (!isset ($catalogID) || !isset ($productID)) {
		sendRequestError($request, 'get thema' . 'ProductID or CatalogID empty');
	}

	$sql = "SELECT * FROM thema WHERE `ProductID` = $productID AND  `CatalogID`=$catalogID";

	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'select thema' . $error);
	};

	$json['themes'] = array ();
	while ($theme = mysql_fetch_assoc($sqlresult)) {
		foreach ($theme as $key => $value) {
			$theme[$key] = htmlentities(addslashes($value));
		}
		$json['themes'][] = $theme;
	}
	sendRequestResult($request, json_encode($json));
	exit ();
}
function getThemes($request) {
	global $db;
	$json['themes'] = array ();
	$sql = "SELECT * FROM thema WHERE `MitarbeiterID` !=0 ";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'select thema' . $error);
	};

	while ($theme = mysql_fetch_assoc($sqlresult)) {
		foreach ($theme as $key => $value) {
			$theme[$key] = htmlentities(addslashes($value));
		}
		$json['themes'][] = $theme;
	}
	sendRequestResult($request, json_encode($json));
	exit ();
}

function updateTheme($request, $data) {
	global $db;
	mysql_query('SET NAMES utf8', $db);
	$theme = json_decode($data, true);
	if (!$theme) {
		sendRequestError($request, 'json' . $data);
	}
	if (!isset ($theme['ID']) || $theme['ID'] == "" || $theme['ID'] == 0) {
		unset ($theme['ID']);
	} else {
		$themeID = $theme['ID'];
	}
	foreach ($theme as $key => $value) {
		$columns[] = $key;
		$value = enquote($value);
		$values[] = $value;
		$colvals[] = "$key=$value";
	}

	if (isset ($themeID)) {
		$themecolvals = implode($colvals, ',');
		$sql = "UPDATE `thema` SET $themecolvals WHERE `id` = $themeID;";
		$sqlresult = mysql_query($sql, $db);
	} else {
		$themecols = implode($columns, ',');
		$themevals = implode($values, ',');
		$sql = "INSERT INTO `thema` ($themecols) VALUES ($themevals)";
		$sqlresult = mysql_query($sql, $db);
		$themeID = mysql_insert_id();
	}
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'updatetheme' . $sql . $error);
	};
	$sql = "SELECT * FROM `thema` WHERE `id` = $themeID;";
	$sqlresult = mysql_query($sql, $db);
	if (!$sqlresult) {
		$error = mysql_error($db);
		sendRequestError($request, 'select thema' . $error);
	};
	$theme = mysql_fetch_assoc($sqlresult);
	foreach ($theme as $key => $value) {
		$theme[$key] = htmlentities(addslashes(utf8_decode($value)));
	}
	sendRequestResult($request, json_encode($theme));
}
?>
