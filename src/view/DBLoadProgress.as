package view 
{
	import flash.text.TextField;

	/**
	 * @author gerard
	 */
	public class DBLoadProgress extends View 
	{
		public var txtMessage:TextField;
		function set message(message:String):void
		{
			if(txtMessage)
				txtMessage.text=message;
		}
	}
}
