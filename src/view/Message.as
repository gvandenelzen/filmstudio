package view 
{
	import utils.Logger;

	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.utils.setTimeout;

	/**
	 * @author gerard
	 */
	public class Message extends MovieClip 
	{
		public var txtMessage : TextField;
		private var _message : String;
		private var _disable : Boolean;

		public function Message(message : String = '',disable : Boolean = false)
		{
			_disable = disable;
			_message = message;
			addEventListener(Event.ADDED, initUI);
		}

		private function initUI(event : Event) : void 
		{
			removeEventListener(Event.ADDED, initUI);
			
			if(parent)
			{
				if(_disable && parent != stage)
				{
					parent.mouseEnabled = false;
					parent.mouseChildren = false;
				}
				init();
			}
		}

		private function onMouseIsDown(event : MouseEvent) : void 
		{
			dispose();
		}

		public function set message(message : String) : void
		{
			_message = message;
			if(txtMessage)
				txtMessage.text = _message;
		}

		public function dispose() : void 
		{
			if(!parent)
				return;
			if(parent != stage)
			{
				parent.mouseEnabled = true;
				parent.mouseChildren = true;
			}
			removeEventListener(MouseEvent.MOUSE_DOWN, onMouseIsDown);
			
			parent.removeChild(this);
			dispatchEvent(new Event(Event.CLOSE));
		}

		private function init() : void 
		{
			//parent.mouseEnabled = false;
			//parent.mouseChildren = false;
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseIsDown);
			x = parent.stage.stageWidth / 2 - width / 2;
			y = parent.stage.stageHeight / 2 - height / 2;
			if(txtMessage)
			{
				if(_message)
					txtMessage.text = _message;
			else
				txtMessage.text = "";
			}
			setTimeout(dispose, 4000);
		}
	}
}
	