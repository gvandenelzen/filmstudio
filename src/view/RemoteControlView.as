package view 
{
	import fl.controls.CheckBox;

	import flash.events.Event;

	/**
	 * @author gerard
	 */
	public class RemoteControlView extends View 
	{
		public var cbxRemoteLogging : CheckBox;

		public function RemoteControlView()
		{
			addEventListener(Event.ADDED_TO_STAGE, initUI);
		}

		private function initUI(event : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initUI);
			cbxRemoteLogging.addEventListener(Event.SELECT,onRemoteLoggingChanged);
		}
		public function get remoteLogging():Boolean
		{
			return cbxRemoteLogging.selected;
		}
		public function set remoteLogging(logging:Boolean):void
		{
			cbxRemoteLogging.selected=logging;
		}
		private function onRemoteLoggingChanged(event : Event) : void 
		{
			dispatchEvent(new Event(Event.CHANGE));
		}
	}
}
