package view 
{
	import controler.DataManager;

	import dbdata.Category;
	import dbdata.Theme;
	import dbdata.VideoRecord;

	import fl.controls.ComboBox;
	import fl.data.DataProvider;

	import model.VideoFilter;

	import utils.Logger;

	import flash.events.Event;
	import flash.utils.Dictionary;

	/**
	 * @author gerard
	 */
	public class PlaylistSelectionView extends View 
	{
		private static const ALLOWED_CATS : Array = [1,3,8];
		private var _filter : VideoFilter;
		public var cbThemeCategory : ComboBox;
		public var cbThemeCode : ComboBox;
		private var _dataManager : DataManager;
		private var _allowedCodes : Dictionary;

		override	protected function initView() : void 
		{
			show();
		}

		public function refresh() : void
		{
			initCBCode();
			setCBCode();	
		}

		private function initCBCode() : void 
		{
			_allowedCodes = new Dictionary(true);
			var theme : Theme;
			var codes : Array;
			for each (var catID:int in ALLOWED_CATS)
			{
				codes = [];
				for each (var nVideo:VideoRecord in _dataManager.videos)
				{
					theme = _dataManager.themes[nVideo.ThemeID];
					if(!theme || theme.ThemaCode == '')
						continue;
					if(theme.KategorieID != catID)
						continue;
					if(codes.indexOf(theme.ThemaCode)==-1)
						codes.push(theme.ThemaCode);
				}
				codes.sort();
				_allowedCodes[catID] = codes;
			}
		}

		public function set dataManager(dataManager : DataManager) : void
		{
			_dataManager = dataManager;
			cbThemeCategory.removeAll();
			var itemsArray : Array = [];
			for each(var category:Category in dataManager.categories)
			{
				if(ALLOWED_CATS.indexOf(category.ID) >= 0)
				{
					itemsArray.push({label:category.Name, data:category.ID});
				}
			}
			itemsArray.sortOn('label');
			cbThemeCategory.dataProvider = new DataProvider(itemsArray);
			cbThemeCategory.selectedIndex = 0;
			
			cbThemeCode.addEventListener(Event.CHANGE, onCodeSelectionChanged);
			cbThemeCategory.addEventListener(Event.CHANGE, onCategorySelectionChanged);
			initCBCode() ;
			setCBCode();
		}

		private function onCodeSelectionChanged(event : Event) : void 
		{
			dispatchEvent(new Event(Event.CHANGE));
		}

		private function onCategorySelectionChanged(event : Event) : void 
		{
			setCBCode();
		}

		private function setCBCode() : void 
		{
			if(!cbThemeCategory.selectedItem)
				return;
			var catID : int = cbThemeCategory.selectedItem.data;
			var codes : Array = _allowedCodes[catID];
			cbThemeCode.dataProvider = new DataProvider(codes);
			dispatchEvent(new Event(Event.CHANGE));
		}

		
		
		public function get selected() : Boolean
		{
			return cbThemeCategory.enabled;
		}

		public function set selected(selected : Boolean) : void
		{
			if(selected)
				alpha = 1;
			else
				alpha = 0.6;
			cbThemeCategory.enabled = selected;
			cbThemeCode.enabled = selected;
			cbThemeCategory.prompt = 'Kategorie wählen';
			cbThemeCategory.selectedIndex = -1;
			cbThemeCode.prompt = 'Themencode wählen';
			cbThemeCode.selectedIndex = -1;
		}

		public function get filter() : VideoFilter
		{
			_filter = new VideoFilter();
			
			if(!cbThemeCategory.selectedItem || !cbThemeCode.selectedItem)
				return null;
			Logger.writeLog("setCBCode ", cbThemeCategory.selectedItem.data, cbThemeCode.selectedItem.label);
			_filter.category = cbThemeCategory.selectedItem.data;
			_filter.themecode = cbThemeCode.selectedItem.label;
			return _filter;
		}
	}
}
