package view 
{
	import controler.DataManager;

	import dbdata.People;
	import dbdata.WebSite;

	import fl.controls.CheckBox;
	import fl.controls.ComboBox;

	import settings.Constants;

	import utils.DateTimeUtils;
	import utils.Logger;

	import flash.events.Event;
	import flash.text.TextField;

	/**
	 * @author gerard
	 */
	public class WebSiteDetailView extends View 
	{
		public var txtStartdate : TextField;
		public var txtTitle : TextField;		public var txtUrl : TextField;		public var txtText : TextField;		public var txtNotes : TextField;
		public var cbExpire : ComboBox;
		public var cbPerson : ComboBox;
		public var cbxRating : CheckBox;		public var cbxLink : CheckBox;		public var cbxOutlook : CheckBox;
		private var _dataManager : DataManager;
		private var _currentWebSite : WebSite;

		public function WebSiteDetailView()
		{
			addEventListener(Event.ADDED_TO_STAGE, initUI);
		}

		private function initUI(event : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initUI);
			txtTitle.addEventListener(Event.CHANGE, onChange);
			txtNotes.addEventListener(Event.CHANGE, onChange);
			txtText.addEventListener(Event.CHANGE, onChange);
			txtUrl.addEventListener(Event.CHANGE, onChange);
			cbExpire.addEventListener(Event.CHANGE, onChangeExpire);
			
			
			clearFields();
		}

		private function onChangeExpire(event : Event) : void 
		{
			if(cbExpire.selectedIndex==0)
				return ;
			if(_currentWebSite.ExpireTimestamp)
			{
				var ts : Number = Number(calculateExpDate(DateTimeUtils.dateToTimeStamp(new Date())));
			}
			txtStartdate.text = DateTimeUtils.fileStampToFormattedDate(ts, true, '.');
			dispatchEvent(new Event(Event.CHANGE));
		}

		private function calculateExpDate(ts : String) : String 
		{
	
			var date : Date = DateTimeUtils.timeStampToDate(ts);
			date = DateTimeUtils.datePlusDays(date, cbExpire.selectedItem.data);
			return DateTimeUtils.dateToTimeStamp(date);
		}

		private function clearFields() : void 
		{
			txtTitle.text = "";
			txtNotes.text = "";
			txtText.text = "";
			txtUrl.text = "";
			txtStartdate.text = DateTimeUtils.formattedDate(null, true);
		}

		private function onChange(event : Event) : void 
		{
			dispatchEvent(new Event(Event.CHANGE));
		}

		private function setExpireCB() : void 
		{
			cbExpire.removeAll();
			for each (var item:Object in Constants.EXPIRE_DATES)
				cbExpire.addItem(item);
			if(_currentWebSite)
			{
				if(_currentWebSite.ID)
					cbExpire.addItemAt({label:'keine Änderung', data:0}, 0);
			}
		}

		public function set webSite(webSite : WebSite) : void
		{
			setExpireCB();
			_currentWebSite = webSite;
			txtTitle.text = _currentWebSite.Title;
			txtNotes.text = _currentWebSite.Notes;
			txtText.text = _currentWebSite.Text;
			if(cbxRating)
				cbxRating.selected = _currentWebSite.RatingActive == 1 ? true : false;
			if(cbxLink)
				cbxLink.selected = _currentWebSite.DownloadActive == 1 ? true : false;
			
			if(!_currentWebSite.ID)
			{
				cbExpire.selectedIndex = -1;
				cbExpire.prompt = 'Bitte wählen';
				_currentWebSite.ExpireTimestamp = Number(DateTimeUtils.dateToTimeStamp());
				_currentWebSite.CreatedTimestamp = Number(DateTimeUtils.dateToTimeStamp());
			}
			else
			{
				cbExpire.selectedIndex = 0;
			}
			if(_currentWebSite.ID)
			{
				txtUrl.htmlText = '<a href="' + makeURL(_currentWebSite.ID) + '">' + makeURL(_currentWebSite.ID) + '<a>';
				Logger.writeLog(txtUrl.htmlText);
			}
			else
			{
				txtUrl.text = '';
			}
			txtStartdate.text = DateTimeUtils.fileStampToFormattedDate(_currentWebSite.ExpireTimestamp, true);
			setCBPerson();
		}

		private function makeURL(id : Number) : String 
		{
			return Constants.BASE_HTTP + Constants.WEBSITE_PHP + '?id=' + (Constants.PRIME * id).toString(16);
		}

		private function setCBPerson() : void 
		{
			var item : Object;
			cbPerson.selectedIndex = -1;
			for ( var i : uint = 0;i < cbPerson.dataProvider.length;i++)
			{
				item = cbPerson.getItemAt(i);
				if(item.data == _currentWebSite.PersonID)
				{
					cbPerson.selectedIndex = i;
					break;
				}
			}
		}

		
		public function get currentWebSite() : WebSite
		{
			_currentWebSite.Title = txtTitle.text ;
			_currentWebSite.Notes = txtNotes.text ;
			_currentWebSite.Text = txtText.text ;
			if(cbxRating)
				_currentWebSite.RatingActive = (cbxRating.selected) ? 1 : 0;
			if(cbxLink)	
				_currentWebSite.DownloadActive = (cbxLink.selected) ? 1 : 0;
			if(cbExpire.selectedIndex)
				_currentWebSite.ExpireTimestamp = Number(calculateExpDate(DateTimeUtils.dateToTimeStamp(new Date())));
			_currentWebSite.URL = makeURL(_currentWebSite.ID);
			if(cbPerson.selectedItem)
				_currentWebSite.PersonID = cbPerson.selectedItem.data;
			return _currentWebSite;
		}

		
		
		public function set dataManager(dataManager : DataManager) : void
		{
			_dataManager = dataManager;
			initCBPeople();
		}

		private function initCBPeople() : void 
		{
			var employees : Array = [];
			for each(var employee:People in _dataManager.people)
			{

				employees.push(employee);
			}
			employees.sortOn('LastName');
			cbPerson.removeAll();
			for each(employee in employees)
			{
				if(employee.Active == 1)
					cbPerson.addItem({label:employee.FullName, data:employee.ID});
			}
			cbPerson.prompt = 'Bitte wählen';
		}
	}
}
