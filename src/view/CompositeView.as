package view 
{
	import flash.events.Event;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.NetStream;

	/**
	 * @author gerard
	 */
	public class CompositeView extends View 
	{
		public var mcEditorPlayerView : EditorPlayerView;
		public var mcMask : VideoMask;

		public function CompositeView()
		{
			addEventListener(Event.ADDED_TO_STAGE, initUI);
		}

		private function initUI(event : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initUI);
			scrollRect=new Rectangle(0,0,mcEditorPlayerView.width,mcEditorPlayerView.height);		
			mcEditorPlayerView.show();	
			showMask();
		}

		public function set netStream(netStream:NetStream):void
		{
			mcEditorPlayerView.netStream = netStream;
		}
		public function get maskVisible():Boolean
		{
			return mcMask.visible;
		}
		public function setLogo(logo:String):void
		{
			mcMask.setLogo(logo);
		}
		public function set subTitle1(caption:String):void
		{
			mcMask.subTitle1=caption;
		}
		public function set subTitle2(caption:String):void
		{
			mcMask.subTitle2=caption;
		}
		public function set playerVisible(visible:Boolean):void
		{
			mcEditorPlayerView.visible=visible;
		}
		public function set logoVisible(visible:Boolean):void
		{
			mcMask.logoVisible=visible;
		}
		public function get logoVisible():Boolean
		{
			return mcMask.logoVisible;
		}
		public function set subTitlesVisible(visible:Boolean):void
		{
			mcMask.subTitlesVisible=visible;
		}
		public function get subTitlesVisible():Boolean
		{
			return mcMask.subTitlesVisible;
		}
		public function showMask() : void 
		{
			mcMask.visible=true;
		}
		public function hideMask() : void 
		{
			mcMask.visible=false;
		}

		public function set size(size : Point) : void
		{
			mcEditorPlayerView.size=size;
			mcMask.size=size;
		}
	}
}
