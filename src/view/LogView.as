package view 
{
	import controls.Indicator;

	import fl.controls.TextArea;

	import utils.Logger;
	import flash.system.Capabilities;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;

	/**
	 * @author gerard
	 */
	public class LogView extends View 
	{
		public var btnClear : SimpleButton;
		public var mcLogging : Indicator;
		public static const CLEAR : String = 'Einträge Löschen';
		public var mcDragger : MovieClip;
		public var txtHeader : TextField;
		public var txtLog : TextArea;

		private var _red : uint = 0xff0000;
		private var _green : uint = 0x00ff00;
		private var _isActive : Boolean;

		public function LogView()
		{
			addEventListener(Event.ADDED_TO_STAGE, initUI);
		}

		private function initUI(event : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initUI);
			x = Math.round(x);
			y = Math.round(y);
			txtLog.text = '';
			txtHeader.text = '';
			txtHeader.mouseEnabled = false;
			
			Logger.directTo(txtLog.textField);
			Logger.addEventListener(Event.CHANGE, onLogChanged);
			
			mcDragger.doubleClickEnabled = true;
			mcDragger.buttonMode = true;
			mcDragger.addEventListener(MouseEvent.MOUSE_DOWN, onMouse);
			mcDragger.addEventListener(MouseEvent.DOUBLE_CLICK, onMouse);
			
			btnClear.addEventListener(MouseEvent.CLICK, onClear);
			mcLogging.addEventListener(MouseEvent.CLICK, onSetLogging);
			mcLogging.buttonMode = true;
			_isActive = false;
			
			setLogging() ;
			toggleHeader();
		}

		public function activate() : void
		{
			_isActive = true;
			setLogging() ;
			txtLog.visible = true;
			txtHeader.visible = !txtLog.visible;
		}

		private function onSetLogging(event : MouseEvent) : void 
		{
			_isActive = !_isActive;
			setLogging();
		}

		private function setLogging() : void 
		{
			
			if(_isActive)
			{
				
				Logger.enabled = true;
				txtHeader.text = 'Logging is on';
				mcLogging.color = _green;
			}
			else
			{
				Logger.enabled = false;
				txtHeader.visible = true;
				txtHeader.text = 'Logging is off';
				mcLogging.color = _red;
			}
		}

		private function onMouse(event : MouseEvent) : void 
		{
			switch(event.type)
			{
				case MouseEvent.DOUBLE_CLICK:
					toggleHeader();
					break;
				case MouseEvent.MOUSE_DOWN:
					parent.swapChildren(this, parent.getChildAt(parent.numChildren - 2));
					startDrag();
					stage.addEventListener(MouseEvent.MOUSE_UP, onMouse);
					break;
				case MouseEvent.MOUSE_UP:
					stopDrag();
					stage.removeEventListener(MouseEvent.MOUSE_UP, onMouse);
					break;
			}
		}

		private function toggleHeader() : void 
		{
			txtLog.visible = !txtLog.visible;
			txtHeader.visible = !txtLog.visible;
		}

		private function onLogChanged(event : Event) : void 
		{
			txtHeader.text = Logger.lastEntry;
			txtLog.validateNow();
		}

		
		private function onClear(event : Event) : void 
		{
			txtLog.text = '';
		}
	}
}
