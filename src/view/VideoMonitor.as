package view 
{
	import controls.Slider;

	import utils.DateTimeUtils;

	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.NetStream;
	import flash.text.TextField;

	/**
	 * @author gerard
	 */
	public class VideoMonitor extends MovieClip 
	{
		private static const DEFAULT_BUFFERTIME : Number = 10;
		public var mcBorder : MovieClip;
		public var txtStart : TextField;
		public var txtEnd : TextField;

		public var txtStartLoop : TextField;
		public var txtEndLoop : TextField;
		public var txtTime : TextField;
		public var txtFps : TextField;
		public var txtBufferLength : TextField;
		public var txtBackBufferLength : TextField;
		public var txtActualBufferLength : TextField;
		public var mcBuffer : Slider;
		public var mcTrimSpeed : Slider;
		public var txtBuffer : TextField;

		public var mcMaxBuffer : Slider;
		public var txtMaxBuffer : TextField;

		public var mcPauseBuffer : Slider;
		public var txtPauseBuffer : TextField;
		private var _fmsNetStream : NetStream;
		private var _duration : Number;

		public function VideoMonitor()
		{
			visible=false;
			addEventListener(Event.ADDED_TO_STAGE, initUI);
		}

		
		private function initUI(event : Event) : void
		{
			removeEventListener(Event.ADDED_TO_STAGE, initUI);
			
			mcBuffer.value = 0.5;
			mcBuffer.addEventListener(Event.CHANGE, onBufferTime);
			
			mcPauseBuffer.value = 0.5;
			mcPauseBuffer.addEventListener(Event.CHANGE, onPauseBufferTime);
			
			mcMaxBuffer.value = 1;
			mcMaxBuffer.addEventListener(Event.CHANGE, onMaxBufferTime);
			
			mcBorder.addEventListener(MouseEvent.MOUSE_DOWN, onStartDrag);
		}

		private function onStartDrag(event : MouseEvent) : void 
		{
			stage.addEventListener(MouseEvent.MOUSE_UP, onStopDrag);
			this.startDrag();
		}

		private function onStopDrag(event : MouseEvent) : void 
		{
			stage.removeEventListener(MouseEvent.MOUSE_UP, onStopDrag);
			stopDrag();
		}

		private function onBufferTime(event : Event) : void 
		{
			setBufferTime();
		}

		private function onMaxBufferTime(event : Event) : void 
		{
			setMaxBufferTime();
		}

		private function onPauseBufferTime(event : Event) : void 
		{
			setPauseBufferTime();
		}

		public function setMaxBufferTime() : void 
		{
			
			if(_duration)
			{
				_fmsNetStream.bufferTimeMax = _duration * mcMaxBuffer.value;
				txtMaxBuffer.text = (_duration * mcBuffer.value).toFixed(3);
			}
		}

		public function setBufferTime() : void 
		{
			_fmsNetStream.bufferTime = DEFAULT_BUFFERTIME * mcBuffer.value;
			_fmsNetStream.backBufferTime = DEFAULT_BUFFERTIME * mcBuffer.value;
			txtBuffer.text = (DEFAULT_BUFFERTIME * mcBuffer.value).toFixed(3);
		}

		public function setPauseBufferTime() : void 
		{
			_fmsNetStream.maxPauseBufferTime = DEFAULT_BUFFERTIME * mcPauseBuffer.value;
			txtPauseBuffer.text = (DEFAULT_BUFFERTIME * mcPauseBuffer.value).toFixed(3);
		}

		public function set fmsNetStream(fmsNetStream : NetStream) : void
		{
			_fmsNetStream = fmsNetStream;
		}

		public function set duration(duration : Number) : void
		{
			_duration = duration;
		}

		public function setStart(start : Number) : void 
		{
			txtStart.text = DateTimeUtils.secondsToVideoTime(start);
		}

		public function setEnd(end : Number) : void 
		{
			txtEnd.text = DateTimeUtils.secondsToVideoTime(end);
		}

		public function updateDisplay() : void 
		{
			txtTime.text = DateTimeUtils.secondsToVideoTime(_fmsNetStream.time);
			txtFps.text = _fmsNetStream.currentFPS.toFixed(0);
			txtBufferLength.text = _fmsNetStream.bufferLength.toFixed(3);
			txtBackBufferLength.text = _fmsNetStream.backBufferLength.toFixed(3);
		}

		public function updatePositions(start : Number, end : Number, startLoop : Number, endLoop : Number) : void 
		{
			txtStart.text = DateTimeUtils.secondsToVideoTime(start);
			txtEnd.text = DateTimeUtils.secondsToVideoTime(end);
			
			txtEndLoop.text = DateTimeUtils.secondsToVideoTime(endLoop);
			txtStartLoop.text = DateTimeUtils.secondsToVideoTime(startLoop);
		}
	}
}
