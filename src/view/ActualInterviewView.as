package view 
{
	import model.InterviewData;

	import flash.events.Event;
	import flash.text.TextField;

	/**
	 * @author gerard
	 */
	public class ActualInterviewView extends View 
	{
		private var _interviewID : String;
		public var txtLocation : TextField;
		public var txtSession : TextField;
		public var txtEmployee : TextField;
		public var txtFunction : TextField;
		public var txtCategory : TextField;
		public var txtTheme : TextField;
		public var txtCode : TextField;
		public var txtDetails : TextField;

		public function ActualInterviewView()
		{
			addEventListener(Event.ADDED_TO_STAGE, initUI);
		}

		private function initUI(event : Event) : void 
		{
			txtSession.text = "";
			txtLocation.text = "";
			txtEmployee.text = "";
			txtFunction.text = "";
			txtCategory.text = "";
			txtTheme.text = "";
			txtDetails.text = "";
			txtCode.text = "";
		}

		public function set interviewData(interviewData : InterviewData) : void
		{
			_interviewID = interviewData.id;
			txtLocation.text = interviewData.location;
			txtSession.text = interviewData.session;
			txtEmployee.text = interviewData.employee;
			txtFunction.text = interviewData.funktion;
			txtCategory.text = interviewData.category;
			txtTheme.text = interviewData.theme;
			txtDetails.text = interviewData.details;
			txtCode.text = interviewData.themeCode;
			show();	
		}

		public function init() : void 
		{
			show();
		}

		public function get interviewID() : uint
		{
			return Number(_interviewID);
		}
	}
}
