package view 
{
	import fl.controls.List;
	import fl.data.DataProvider;
	import fl.events.ListEvent;

	import utils.Logger;

	import flash.display.MovieClip;
	import flash.events.Event;

	/**
	 * @author gerard
	 */
	public class TakeOneListView extends MovieClip 
	{
		public var mcList:List;
		private var _selectedID:uint;
		public function TakeOneListView()
		{
			mcList.addEventListener(ListEvent.ITEM_CLICK,onItemSelected);
		}

		private function onItemSelected(event : ListEvent) : void 
		{
			_selectedID=event.item.data;
			Logger.writeLog('selected  ',_selectedID);
			dispatchEvent(new Event(Event.SELECT));
		}
		
		public function get selectedID() : uint
		{
			return _selectedID;
		}
		public function set dataProvider(dataProvider:DataProvider):void
		{
			mcList.dataProvider=dataProvider;
		}

		public function clear() : void 
		{
			mcList.removeAll();
		}
	}
}
