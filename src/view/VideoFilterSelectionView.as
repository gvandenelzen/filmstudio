package view 
{
	import fl.controls.Button;

	import flash.display.SimpleButton;

	/**
	 * @author gerard
	 */
	public class VideoFilterSelectionView extends View 
	{
		public var btnFilter : Button;
		public var btnReload: SimpleButton;
		override protected function initView() : void 
		{
			show();
		}

		public function set selected(selected : Boolean) : void
		{
			if(selected)
				alpha = 1;
			else
				alpha = 0.6;
			btnFilter.enabled = selected;
			btnReload.enabled = selected;
		}

		public function get selected() : Boolean
		{
			return btnFilter.enabled;
		}
	}
}
