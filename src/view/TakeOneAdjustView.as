package view 
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;

	/**
	 * @author gerard
	 */
	public class TakeOneAdjustView extends View 
	{
		private static const HEADER : String = "Bitte setzen Sie sich nun mittig und stellen Sie Ihren Sitz auf die richtige Höhe ein,so dass die Linie auf Augenhöhe ist.";
		public var txtHeader : TextField;
		public var btnProceed : MovieClip;
		public var btnBack : MovieClip;

		public function TakeOneAdjustView()
		{
			super();
		}

		override protected function initView() : void 
		{
			btnProceed.addEventListener(MouseEvent.CLICK, onProceed);
			btnBack.addEventListener(MouseEvent.CLICK, onBack);
			txtHeader.text = HEADER;
		}

		private function onProceed(event : MouseEvent) : void 
		{
			dispatchEvent(new Event(Event.COMPLETE));
		}

		private function onBack(event : MouseEvent) : void 
		{
			dispatchEvent(new Event(Event.CANCEL));
		}

		public function set person(person : String) : void
		{
			txtHeader.text = HEADER;//.replace("{PERSON}", person);
		}

		override public function show() : void
		{
			super.show();
		}
	}
}
