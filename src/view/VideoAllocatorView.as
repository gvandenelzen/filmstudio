package view 
{
	import controls.ColorButton;

	import dbdata.Location;
	import dbdata.Region;

	import settings.SelectionColor;

	import utils.Logger;

	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.utils.Dictionary;

	/**
	 * @author gerard
	 */
	public class VideoAllocatorView extends View 
	{

		private var _regions : Dictionary;
		private var _locations : Dictionary;
		private var _colorButtons : Dictionary;
		private var _videosPerLocation : Dictionary; 
		private var _videoLocations : Array;

		public function set videolocations(videolocations : Array) : void
		{
			_videoLocations = videolocations;
			setButtonColors();
		}

		private function setButtonColors() : void 
		{
			Logger.writeLog('VideoAllocatorView setButtonColors', _videosPerLocation);
			for (var id:* in _videosPerLocation)
			{
				_videosPerLocation[id] = 0;
			}
			var button : ColorButton;
			var color : int;
			for each(id in _videoLocations)
			{
				if(_videosPerLocation[id] >= 0)
					_videosPerLocation[id] += 1;
			}
			for (id in _videosPerLocation)
			{
				button = _colorButtons[id];
				if(!button)
					continue;
				if(!button.selected)
				{
					
					switch(_videosPerLocation[id])
					{
						case 0:
							color = SelectionColor.NO_SELECTION;	
							break;
						case 1:
							color = SelectionColor.ONE_SELECTION;
							break;
						default:
							color = SelectionColor.MULTIPLE_SELECTION;
					}
					button.color = color;	
				}
			}
		}

		public function get selectedLocations() : Array
		{
			var locationsForVideo : Array = [];
			for each(var button:ColorButton in _colorButtons )
			{
				if(button.selected && button.locationID)
				{
					locationsForVideo.push(button.locationID);
				}
			}
			return locationsForVideo;
		}

		public function set currentVideoLocations(selectedLocations : Array) : void
		{
			resetButtons();
			for each(var locationID:uint in selectedLocations)
			{
				for each(var button:ColorButton in _colorButtons )
				{
					if(locationID == button.locationID)
					{
						button.selected = true;
						button.color = SelectionColor.CURRENT_SELECTION;
					}
				}
			}
			setButtonColors();
			dispatchEvent(new Event(Event.CHANGE));
		}

		
		
		private function resetButtons() : void 
		{
			for each(var button:ColorButton in _colorButtons )
			{
				button.selected = false;
				button.color = SelectionColor.NO_SELECTION;
			}
		}

		public function setLocationData(regions : Dictionary,locations : Dictionary) : void
		{
			_regions = regions;
			_locations = locations;
			_colorButtons = new Dictionary(true);
			_videosPerLocation = new Dictionary(true);
			buildButtonMatrix();
		}

		private function buildButtonMatrix() : void 
		{
			var button : ColorButton;
			var posX : uint = 0;
			var posY : uint = 0;
			var startPoint : Point;
			for each(var region:Region in _regions)
			{
				if(region.Active == 0)
					continue;
				button = new ColorButton(region.ShortName);
				button.color = SelectionColor.NO_SELECTION;
				button.regionID = region.ID;
				button.addEventListener(MouseEvent.CLICK, onRegionClicked);
				button.x = posX;
				addChild(button);
				startPoint = new Point(posX, posY + button.height + 10);
				buildLocationsPerRegion(region, startPoint);
				posX += button.width;
			}
			startPoint = new Point(posX, posY + button.height);
			buildLocationsWithoutRegion(startPoint);
		}
		
		private function buildLocationsWithoutRegion(startPoint : Point) : void 
		{
			var button : ColorButton;
			var posX : uint = startPoint.x;
			var posY : uint = startPoint.y;
			for each( var location:Location in _locations)
			{
				if(location.RegionID == 0. && location.Active == 1)
				{
					button = new ColorButton(location.ShortName);
					button.locationID = location.ID;
					_colorButtons[button.locationID] = button;
					button.color = SelectionColor.NO_SELECTION;
					button.addEventListener(MouseEvent.CLICK, onLocationClicked);
					button.x = posX;
					button.y = posY;
					addChild(button);
					posY += button.height;
					_videosPerLocation[location.ID] = 0;
				}
			}
		}

		private function buildLocationsPerRegion(region : Region,startPoint : Point) : void 
		{
			var button : ColorButton;
			var posX : uint = startPoint.x;
			var posY : uint = startPoint.y;
			for each( var location:Location in _locations)
			{
				if( location.Active == 0)
					continue;
				if(location.RegionID == region.ID   )
				{
					button = new ColorButton(location.ShortName);
					button.locationID = location.ID;
					button.regionID = region.ID;
					button.color = SelectionColor.NO_SELECTION;
					button.addEventListener(MouseEvent.CLICK, onLocationClicked);
					button.x = posX;
					button.y = posY;
					addChild(button);
					posY += button.height;
					_colorButtons[button.locationID] = button;
					_videosPerLocation[location.ID] = 0;
				}
			}
		}

		private function onLocationClicked(event : MouseEvent) : void 
		{
			var button : ColorButton = event.currentTarget as ColorButton;
			button.selected = !button.selected;
			if(button.selected)
				button.color = SelectionColor.CURRENT_SELECTION;
			else
				button.color = SelectionColor.NO_SELECTION;
			dispatchEvent(new Event(Event.CHANGE));
		}

		private function onRegionClicked(event : MouseEvent) : void 
		{
			var selButton : ColorButton = event.currentTarget as ColorButton;
			selButton.selected = !selButton.selected;
			if(selButton.selected)
				var color : uint = SelectionColor.CURRENT_SELECTION;
			else
				color = SelectionColor.NO_SELECTION;
			
			for each(var button:ColorButton in _colorButtons )
			{
				if(button.regionID == selButton.regionID)
				{
					button.selected = selButton.selected;
					button.color = color;
				}
			}
			dispatchEvent(new Event(Event.CHANGE));
		}

		public function reset() : void 
		{
			resetButtons();
		}
	}
}
