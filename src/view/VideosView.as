﻿package view 
{
	import fl.controls.DataGrid;
	import fl.controls.dataGridClasses.DataGridColumn;
	import fl.events.DataGridEvent;
	import fl.events.ListEvent;

	import utils.DateTimeUtils;
	import utils.Logger;

	import flash.events.Event;
	import flash.text.TextField;

	/**
	 * @author gerard
	 */
	public class VideosView extends View 
	{
		public var dgVideos : DataGrid;
		public var txtAmount : TextField;
		private var _sortField : String;
		private var _sortDescending : Boolean;
		private var _videoID : Number;
		private var _initialized : Boolean;
		private var _videos : Array;
		private var _lastSortField : String;
		private var _selectedID : int;

		
		public function VideosView() : void
		{
			addEventListener(Event.ADDED_TO_STAGE, initUI);
		}

		private function initUI(event : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initUI);
			txtAmount.selectable = false;
			txtAmount.mouseEnabled = false;
			_initialized = true;
			_sortDescending = true;
			_sortField = 'editdate';
			initGrid();
		}

		private function fillGrid() : void
		{
			dgVideos.removeAll();
			if(_sortDescending)
				_videos.sortOn(_sortField, Array.DESCENDING);
			else
				_videos.sortOn(_sortField);
			for each (var obj:Object in _videos)
			{
				dgVideos.addItem(obj);
			}
			txtAmount.text = _videos.length + ' Videos in Liste';
			dgVideos.validateNow();
			dgVideos.verticalScrollBar.scrollPosition = 0;
			dgVideos.allowMultipleSelection = true;
			if(_selectedID)
				setSelected(_selectedID) ;
			dgVideos.scrollToSelected();
			Logger.writeLog(this, 'fillGrid  ', _sortField, _sortDescending, _selectedID);
		}

		private function initGrid() : void 
		{

			Logger.writeLog('InterviewsView initGrid ');
			var columns : Array = [];
			var dataGridColumn : DataGridColumn = new DataGridColumn('takedate');
			dataGridColumn.headerText = 'Aufnahmedatum';
			dataGridColumn.labelFunction = transFormTakeDate;
			dataGridColumn.sortable = true;
			dataGridColumn.width = 100;
			columns.push(dataGridColumn);
			
			dataGridColumn = new DataGridColumn('editdate');
			dataGridColumn.headerText = 'Editdatum';
			dataGridColumn.sortable = true;
			dataGridColumn.labelFunction = transFormEditDate;
			dataGridColumn.width = 100;
			columns.push(dataGridColumn);
			
			dataGridColumn = new DataGridColumn('duration');
			dataGridColumn.headerText = 'Länge';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 40;
			columns.push(dataGridColumn);
			
			dataGridColumn = new DataGridColumn('id');
			dataGridColumn.headerText = 'VideoID';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 40;
			columns.push(dataGridColumn);
						
			dataGridColumn = new DataGridColumn('notes');
			dataGridColumn.headerText = 'Notizen';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 160;
			columns.push(dataGridColumn);

			dataGridColumn = new DataGridColumn('people');
			dataGridColumn.headerText = 'Name';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 135;
			columns.push(dataGridColumn);
			
			dataGridColumn = new DataGridColumn('location');
			dataGridColumn.headerText = 'Standort';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 70;
			columns.push(dataGridColumn);

			dataGridColumn = new DataGridColumn('category');
			dataGridColumn.headerText = 'Kategorie';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 120;
			columns.push(dataGridColumn);
			
			dataGridColumn = new DataGridColumn('code');
			dataGridColumn.headerText = 'Themencode';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 70;
			columns.push(dataGridColumn);
			
			dataGridColumn = new DataGridColumn('themetext');
			dataGridColumn.headerText = 'Thema';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 70;
			columns.push(dataGridColumn);
			
			dataGridColumn = new DataGridColumn('session');
			dataGridColumn.headerText = 'Session';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 90;
			columns.push(dataGridColumn);


			dataGridColumn = new DataGridColumn('status');
			dataGridColumn.headerText = 'Status';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 70;
			columns.push(dataGridColumn);
			
			dataGridColumn = new DataGridColumn('active');
			dataGridColumn.headerText = 'aktiv';
			dataGridColumn.labelFunction = transFormActive;
			dataGridColumn.sortable = true;
			dataGridColumn.width = 25;
			columns.push(dataGridColumn);
			
			dgVideos.columns = columns;
			dgVideos.addEventListener(ListEvent.ITEM_DOUBLE_CLICK, onVideoSelected);
			dgVideos.addEventListener(DataGridEvent.HEADER_RELEASE, onHeaderSort);
			dgVideos.removeAll();
			if(_videos)
				fillGrid();
		}

		private function transFormActive(item : Object) : String
		{
			var active : Number = item.active;
			switch(active)
			{
				case 0:
					return "0";
					break;
				case 1:
					return "1";
					break;
				case 2:
					return "X";
					break;
			}
			return active.toString();
		}

		private function transFormEditDate(item : Object) : String
		{
			var date : Number = item.editdate;
			return formatDate(date);
		}

		private function transFormTakeDate(item : Object) : String
		{
			var date : Number = item.takedate;
			return formatDate(date);
		}

		private function formatDate(date : Number) : String 
		{
			if(date == 0)
				return "";
				
			return DateTimeUtils.fileStampToFormattedDate(date);
		}

		private function onHeaderSort(event : DataGridEvent) : void 
		{
			var colIndex : int = event.columnIndex;
			var column : DataGridColumn = dgVideos.columns[colIndex];
			_sortDescending = column.sortDescending;
			switch(colIndex)
			{
				case 0:
					_sortField = 'takedate';
					break;
				case 1:
					_sortField = 'editdate';
					break;
				case 2:
					_sortField = 'duration';
					break;
				case 3:
					_sortField = 'id';
					break;
				case 4:
					_sortField = 'notes';
					break;
				case 5:
					_sortField = 'people';
					break;
				case 6:
					_sortField = 'location';
					break;
				case 7:
					_sortField = 'category';
					break;
				case 8:
					_sortField = 'code';
					break;
				case 9:
					_sortField = 'themetext';
					break;
				case 10:
					_sortField = 'session';
					break;
				case 11:
					_sortField = 'status';
					break;
				case 12:
					_sortField = 'active';
					break;
			}
			//				item = {id:id,takedate:takedate,editdate:editdate,duration:duration,notes:notes,people:peoplename,  location:locationname,category:categoryname, code:code, themetext:themetext, session:session, status:statusname,active:active};
			_lastSortField = _sortField;
			Logger.writeLog("onHeaderSort", _sortField, _sortDescending);
		}

		private function onVideoSelected(event : ListEvent) : void
		{
			_videoID = dgVideos.selectedItem.id;
			dispatchEvent(new Event(Event.SELECT));
		}

		public function get selectedVideosIDs() : Array
		{
			var ids : Array = [];
			for each (var obj:Object in dgVideos.selectedItems)
			{
				ids.push(obj['id']);
			}
			return ids;
		}

		public function get videoID() : Number
		{
			return _videoID;
		}

		public function set videos(videos : Array) : void
		{
			txtAmount.text = videos.length.toString() + ' Videos in Liste';
			_videos = videos;
			if(_initialized)
				fillGrid();
		}

		public function  set selectedVideo(id : int) : void
		{
			Logger.writeLog(this, "selectedVideo found ", id)
			_selectedID = id;
			setSelected(id);
		}

		public function setSelected(id : uint) : void 
		{
			var item:Object;
			for  ( var i:int=0;i< dgVideos.dataProvider.length;i++)
			{
				item=dgVideos.getItemAt(i);
				if(item.id == id)
				{
					Logger.writeLog(this, "setSelected found ", id)
					dgVideos.selectedItem = item;
					dgVideos.scrollToIndex(i);
					break;
				}
			}
			
		}

		public function clear() : void
		{
			dgVideos.removeAll();
		}
	}
}
