package view 
{
	import settings.Constants;

	import flash.display.MovieClip;
	import flash.geom.Point;

	/**
	 * @author gerard
	 */
	public class VideoMask extends MovieClip 
	{
		public var mcBackground : MovieClip;
		public var mcLogo : ImageHolder;
		public var mcSubTitle1 : SubTitle;
		public var mcSubTitle2 : SubTitle;

		public function VideoMask()
		{
			var imageURL:String=Constants.LOGO_FOLDER+'/'+'kloepfer.png';
			mcLogo.loadImage(imageURL);
		}

		public function set size(size : Point) : void
		{
			mcBackground.width = size.x;
			mcBackground.height = size.y;
		}

		public function set subTitle1(caption : String) : void
		{
			mcSubTitle1.caption = caption;
		}

		public function set subTitle2(caption : String) : void
		{
			mcSubTitle2.caption = caption;
		}

		public function set logoVisible(visible : Boolean) : void
		{
			mcLogo.visible = visible;
		}

		public function get logoVisible() : Boolean
		{
			return mcLogo.visible;
		}

		public function set subTitlesVisible(visible : Boolean) : void
		{
			mcSubTitle1.visible = visible;
			mcSubTitle2.visible = visible;
		}

		public function get subTitlesVisible() : Boolean
		{
			return mcSubTitle1.visible && mcSubTitle2.visible;
		}

		public function setLogo(logo : String) : void 
		{
			mcLogo.loadImage(logo);
		}
	}
}
