package view 
{
	import flash.events.IEventDispatcher;
	import flash.media.Camera;
	import flash.net.NetStream;

	/**
	 * @author gerard
	 */
	public interface IVideoView extends IEventDispatcher
	{
		function set netStream(netStream:NetStream):void
		function clear():void
		function attachCamera(camera:Camera):void;
	}
}
