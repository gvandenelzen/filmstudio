package  view
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;

	/**
	 * @author gerard
	 */
	public class TakeOneTakeView extends View 
	{
		public var btnStart:MovieClip;
		public function TakeOneTakeView()
		{
			super();
		}
		override protected function initView() : void 
		{
			btnStart.addEventListener(MouseEvent.CLICK, onStart);
		}

		private function onStart(event : MouseEvent) : void 
		{
			dispatchEvent(new Event(Event.COMPLETE));
		}
	}
}
