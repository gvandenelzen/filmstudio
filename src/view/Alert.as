package view 
{
	import fl.controls.Button;

	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;

	/**
	 * @author gerard
	 */
	public class Alert extends View 
	{
		public var txtMessage : TextField;
		public var btnOK : Button;
		public var btnCancel : Button;
		private var _message : String;

		public function Alert(message:String='')
		{
			_message=message;
			addEventListener(Event.ADDED_TO_STAGE, initUI);
		}
		
		private function initUI(event : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initUI);
			if(_message)
				message=_message;
			if(btnCancel && btnOK)
				btnCancel.selected = true;
			if(btnCancel)
			{
				btnCancel.label="Abbrechen";
				btnCancel.addEventListener(MouseEvent.CLICK, onButton);
			}
			if(btnOK)
			{
				btnOK.label='OK';
				btnOK.addEventListener(MouseEvent.CLICK, onButton);
			}
		
			initAlert();
		}


		protected function initAlert() : void 
		{
		}

		private function onButton(event : MouseEvent) : void 
		{
			var button : Button = event.currentTarget as Button;
			switch(button)
			{
				case btnOK:
					dispatchEvent(new Event(Event.SELECT));
					break;
				case btnCancel:
					dispatchEvent(new Event(Event.CANCEL));
					break;
			}
		}

		public function dispose() : void 
		{
			if(!parent)
				return;
			if(stage != parent)
			{
				parent.mouseEnabled = true;
				parent.mouseChildren = true;
			}
			parent.removeChild(this);
		}
		
		public function set message(message : String) : void
		{
			_message = message;
			if(txtMessage)
				txtMessage.text=message;
			
		}
	}
}
