package view 
{
	import controler.TakeOneDataManager;

	import dbdata.Location;
	import dbdata.People;

	import flash.media.Camera;
	import flash.media.Video;
	import flash.text.TextField;

	/**
	 * @author gerard
	 */
	public class TakeOneVideoView extends View 
	{
		public var txtMessage:TextField;
		public var mcVideo : Video;
		public var mcSubTitle1 : SubTitle;

		public function TakeOneVideoView()
		{
			super();
			mcVideo.smoothing = true;
		}
		override public function show():void
		{
			txtMessage.text='';
			super.show();
		}
		public function set message(message:String):void
		{
			txtMessage.text=message;
		}
		public function set camera(camera : Camera) : void
		{
			mcVideo.attachCamera(camera);
		}

		public function set username(username : String) : void
		{
			mcSubTitle1.caption = username;
		}

		public function set person(personID : uint) : void
		{
			var dataManager : TakeOneDataManager = TakeOneDataManager.instance;
			var person : People = dataManager.people[personID];
			if(person)
			{
				mcSubTitle1.caption = person.FirstName + " " + person.LastName;
			}
		}
	}
}
