package view 
{
	import utils.ImageLoader;
	import utils.ImageUtils;
	import utils.Logger;

	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.geom.Rectangle;

	/**
	 * @author gerard
	 */
	public class ImageHolder extends View 
	{
		public var mcBackground:MovieClip;
		public function ImageHolder()
		{
			if(width && height> 10)
				scrollRect=new Rectangle(0,0,width,height);
			if(mcBackground)
				mcBackground.alpha=0;
				
		}


		public function loadImage(imageURL : String) : void 
		{
			while(numChildren>0)
				removeChildAt(0);
				
			var imageLoader : ImageLoader = new ImageLoader();
			imageLoader.addEventListener(Event.COMPLETE, onImageLoadEvent);
			imageLoader.addEventListener(ErrorEvent.ERROR, onImageLoadEvent);
			imageLoader.loadImage(imageURL);
		}

		private function onImageLoadEvent(event : Event) : void 
		{
			event.stopImmediatePropagation();
			var imageLoader : ImageLoader = ImageLoader(event.currentTarget);
			switch (event.type)
			{
				
				case Event.COMPLETE:
					Logger.writeLog("Loaded image:", imageLoader.imageName)	;
					setImage(imageLoader.image);
					break;
				case ErrorEvent.ERROR:
					Logger.writeLog("Error loading:", imageLoader.imageName)	;
					break;
			}
		}

		private function setImage(image : DisplayObject) : void 
		{
			if(width >0 && height>0)
				image=ImageUtils.resizeImage(image, width, height);
			addChild(image)	;
		}
	}
}
