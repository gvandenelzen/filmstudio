package view 
{
	import controls.ColorButton;

	import settings.SelectionColor;

	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.text.TextField;

	/**
	 * @author gerard
	 */
	public class ColorLegenda extends MovieClip 
	{
		public var btnNone : ColorButton;
		public var btnActual : ColorButton;
		public var btnOther : ColorButton;
		public var btnOthers : ColorButton;
		public var txtLegenda : TextField;

		public function ColorLegenda()
		{
			btnNone.color = SelectionColor.NO_SELECTION;
			addListener(btnNone);
			
			btnActual.color = SelectionColor.CURRENT_SELECTION;
			addListener(btnActual);
			
			btnOther.color = SelectionColor.ONE_SELECTION;
			addListener(btnOther);
			
			btnOthers.color = SelectionColor.MULTIPLE_SELECTION;
			addListener(btnOthers);
			txtLegenda.text = '';
			txtLegenda.mouseEnabled=false;
		}

		private function addListener(button : ColorButton) : void 
		{
			button.addEventListener(MouseEvent.MOUSE_OVER, onLegenda);
			button.addEventListener(MouseEvent.MOUSE_OUT, onLegenda);
			button.enabled = true;
			button.caption = "";
			
		}

		private function onLegenda(event : MouseEvent) : void 
		{
			var button : ColorButton = event.currentTarget as ColorButton;
			if(event.type == MouseEvent.MOUSE_OUT)
			{
				txtLegenda.text = '';
				return;
			}
			switch(button)
			{
				case btnNone:
					txtLegenda.text = 'kein Video zugewiesen';
					break;
				case btnActual:
					txtLegenda.text = 'aktuelles Video zugewiesen';
					break;
				case btnOther:
					txtLegenda.text = 'anderes Video zugewiesen';
					break;
				case btnOthers:
					txtLegenda.text = 'mehrere Videos zugewiesen';
					break;
			}
		}
	}
}
