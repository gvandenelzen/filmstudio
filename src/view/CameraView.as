package view 
{
	import flash.display.Sprite;
	import flash.media.Camera;
	import flash.media.Video;

	/**
	 * @author gerard
	 */
	public class CameraView extends Sprite 
	{
		private var _video:Video;
		public function CameraView(camera:Camera)
		{
			_video=new Video();
			addChild(_video);
			if(camera)
				camera=camera;
		}
		public function set camera(camera:Camera):void
		{
			if(camera)
			{
				_video.attachCamera(camera);
				_video.width=camera.width;
				_video.height=camera.height;
			}
		}
	}
}
