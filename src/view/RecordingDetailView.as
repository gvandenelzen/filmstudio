package view 
{
	import controler.DataManager;

	import dbdata.Recording;
	import dbdata.Status;

	import fl.controls.Button;
	import fl.controls.ComboBox;
	import fl.controls.TextArea;

	import utils.DateTimeUtils;

	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;

	/**
	 * @author gerard
	 */
	public class RecordingDetailView extends View 
	{
		public var txtNotes : TextField;

		public var txtStart : TextField;
		public var txtDuration : TextField;
		public var txtMode : TextField;
		public var txtFile : TextField;

		public var cbStatus : ComboBox;

		public var btnDelete : Button;
		public var btnSave : Button;
		private var _dataManager : DataManager;
		private var _recordingID : int;
		private var _currentRecording : Recording;

		public function RecordingDetailView()
		{
			addEventListener(Event.ADDED_TO_STAGE, initUI);
		}

		private function initUI(event : Event) : void 
		{
			btnDelete.visible = false;
			btnDelete.label = 'Löschen';
			btnDelete.enabled = false;
			btnSave.label = 'Speichern';
			btnSave.enabled = false;
			btnSave.addEventListener(MouseEvent.CLICK, onSave);
			cbStatus.addEventListener(Event.CHANGE, onChange);
			txtNotes.addEventListener(Event.CHANGE, onChange);
			initTextFields();
		}

		private function onChange(event : Event) : void 
		{
			if(!_currentRecording)
				return;
			if(cbStatus.selectedIndex == -1)
					return;
			if((cbStatus.selectedItem.data != _currentRecording.StatusID) || (_currentRecording.Notes != txtNotes.text) )
				btnSave.enabled = true;
			else
				btnSave.enabled = false;
		}

		private function onSave(event : MouseEvent) : void 
		{
			_currentRecording.Notes = txtNotes.text;
			_currentRecording.StatusID = cbStatus.selectedItem.data;
			_dataManager.updateRecording(_currentRecording);
			dispatchEvent(new Event(Event.CHANGE));
		}

		private function initTextFields() : void 
		{
			txtNotes.text = "";
			txtStart.text = "";
			txtDuration.text = "";
			txtFile.text = "";
			txtMode.text = "";
		}

		public function set recordingID(recordingID : int) : void
		{
			_recordingID = recordingID;
			fillDetails();
		}

		private function fillDetails() : void 
		{
			_currentRecording = _dataManager.recordings[_recordingID];
			
			txtFile.text = _currentRecording.FileName;
			txtNotes.text = _currentRecording.Notes;
			txtStart.text = DateTimeUtils.fileStampToFormattedDate(_currentRecording.TimestampStart);
			txtDuration.text = _currentRecording.Duration.toString();
			txtMode.text = _currentRecording.RecordMode;
			cbStatus.selectedIndex = -1;
			cbStatus.prompt = 'Bitte wählen';
			if(_currentRecording.StatusID > 0)	
			{
				for (var i : uint = 0;i < cbStatus.dataProvider.length;i++)
				{
					var item : Object = cbStatus.getItemAt(i);
					if(item.data == _currentRecording.StatusID)
					break;
				}
				cbStatus.selectedIndex = i;
			}
			btnSave.enabled = false;
		}

		public function set dataManager(dataManager : DataManager) : void
		{
			_dataManager = dataManager;
			var stati : Array = [];
			for each(var status:Status in _dataManager.statusRecording)
			{
				stati.push(status);
			}
			stati.sortOn('Order');
			for each ( status in stati)
			{
				cbStatus.addItem({label:status.Name, data:status.ID});			
			}
		}

		public function clear() : void 
		{
			initTextFields();
		}
		
		public function get currentRecording() : Recording
		{
			return _currentRecording;
		}
	}
}
