package view 
{
	import flash.display.MovieClip;

	/**
	 * @author gerard
	 */
	public class View extends MovieClip 
	{
		public function View()
		{
			hide();
			initView();
		}

		protected function initView() : void 
		{
		}

		override public function set enabled(enabled:Boolean):void{
			mouseEnabled=enabled;
			mouseChildren=enabled;
			super.enabled;
		}
		public function show():void
		{
			enabled=true;
			visible=true;
		}
		public function hide():void
		{
			enabled=false;
			visible=false;
			
		}
	}
}
