package view 
{
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	/**
	 * @author gerard
	 */
	public class AudioVisualizer extends Sprite 
	{
		private var _size : Point;

		public function AudioVisualizer()
		{
		}
		public function set size(size:Point):void
		{
			_size=size;
			scrollRect=new Rectangle(0,0,size.x,size.y);
		}
	}
}
/*
var sound:Sound = new Sound();

var samples:ByteArray = new ByteArray();

var buffer:BitmapData = new BitmapData(1024,512,false,0);

var cursorBuffer:BitmapData = new BitmapData(1024,512,true,0);

var selectBuffer:BitmapData = new BitmapData(1024,512,true,0);

var selectScreen:Bitmap = new Bitmap(selectBuffer);

var cursorScreen:Bitmap = new Bitmap(cursorBuffer);

var screen:Bitmap = new Bitmap(buffer);

addChild( screen );

addChild( cursorScreen );

addChild( selectScreen );

var label:TextField = new TextField ();

label.defaultTextFormat = new TextFormat ( "Verdana", 10, 0xFFFFFF );

label.autoSize = TextFieldAutoSize.LEFT;

label.text = "simple as3 waveform with Sound.extract() - select any area to loop";

addChild ( label );

var rect:Rectangle = new Rectangle(0,0,1,0);

var playingTime:int;
var ratio:Number;
var step:int;
 
var zone:Rectangle = new Rectangle(0,0,1,512);

var selectionRect:Rectangle = new Rectangle(0,0,0,1024);

var clickedPosition:int;

var looper:Timer = new Timer(0,0);

looper.addEventListener( TimerEvent.TIMER, onTime );

function scrollIt( e:Event ):void
{
	cursorBuffer.fillRect( cursorBuffer.rect,0 );
	zone.x = (buffer.width / playingTime) * channel.position;
	cursorBuffer.fillRect( zone , 0xFF990000 );  
}

function loadComplete(event:Event):void
{
	buffer.fillRect( buffer.rect, 0 );
	
	var extract:Number = Math.floor ((sound.length/1000)*44100);

	playingTime = sound.length;
	
	ratio = playingTime / buffer.width;

	var lng:Number = sound.extract(samples,extract);

	stage.addEventListener( Event.ENTER_FRAME, scrollIt );

	samples.position = 0;

	step = samples.length/4096;

	do step-- while ( step % 4 );
	
	var left:Number;
	var right:Number;
	
	for (var c:int = 0; c < 4096; c++)
	{
		rect.x = c/4;
		left = samples.readFloat()*128;
		right = samples.readFloat()*128;
		samples.position = c*step;

		if (left>0)
		{
			rect.y = 128-left;
			rect.height = left;
		} else
		{
			rect.y = 128;
			rect.height = -left;
		}

		buffer.fillRect( rect, 0xFFFFFF );

		if (right>0)
		{
			rect.y = 350-right;
			rect.height = right;
		} else
		{
			rect.y = 350;
			rect.height = -right;
		}

		buffer.fillRect( rect, 0xFFFFFF );
	}
	channel = sound.play();
}

stage.addEventListener( MouseEvent.CLICK, onClick );

stage.addEventListener( MouseEvent.MOUSE_DOWN, onMouseIsDown );

function onTime( e:TimerEvent ):void
{
	channel.stop();
	channel = sound.play( selectionRect.x*ratio );
}

function onMouseIsDown( e:MouseEvent ):void
{
	stage.addEventListener( MouseEvent.MOUSE_MOVE, onMove );
	selectBuffer.fillRect( selectBuffer.rect, 0 );
	clickedPosition = e.stageX;
	selectionRect.x = clickedPosition;
	selectionRect.width = 0;
}

function onMove( e:MouseEvent ):void
{
	if ( e.stageX > clickedPosition )
	{
		selectionRect.width = e.stageX-clickedPosition;
	} else
	{
		selectionRect.x = e.stageX;
		selectionRect.width = Math.abs (e.stageX-clickedPosition);
	}
	selectBuffer.fillRect( selectionRect, 0x33CCCCCC );
}

function onClick( e:MouseEvent ):void
{
	channel.stop();
	channel = sound.play( selectionRect.x*ratio );
	stage.removeEventListener( MouseEvent.MOUSE_MOVE, onMove );

	if (selectionRect.width)
	{
		looper.delay = ratio*selectionRect.width;
		looper.start();
	} else looper.stop();
}

sound.addEventListener(Event.COMPLETE, loadComplete);
sound.load(new URLRequest("Sunglasses.mp3"));
var channel:SoundChannel;
*/