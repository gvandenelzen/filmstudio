package view 
{
	import fl.controls.DataGrid;
	import fl.controls.dataGridClasses.DataGridColumn;
	import fl.events.DataGridEvent;
	import fl.events.ListEvent;

	import utils.Logger;

	import flash.events.Event;
	import flash.text.TextField;

	/**
	 * @author gerard
	 */
	public class InterviewsView extends View 
	{
		public var dgInterviews : DataGrid;
		public var txtAmount : TextField;
		private var _sortField : String;
		private var _sortDescending : Boolean;
		private var _interviewID : Number;
		private var _initialized : Boolean;
		private var _interviews : Array;

		public function InterviewsView() : void
		{
			addEventListener(Event.ADDED_TO_STAGE, initUI);
		}

		private function initUI(event : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initUI);
			_initialized = true;
			initGrid();
		}

		private function fillGrid() : void
		{
			dgInterviews.removeAll();
			for each (var obj:Object in _interviews)
			{
				dgInterviews.addItem(obj);
			}
			dgInterviews.validateNow();
		}

		private function initGrid() : void 
		{
			Logger.writeLog('InterviewsView initGrid ');
			var columns : Array = [];
			var dataGridColumn : DataGridColumn = new DataGridColumn('id');
			dataGridColumn.headerText = '';
			dataGridColumn.sortable = false;
			dataGridColumn.width = 0;
			dataGridColumn.visible = false;
			columns.push(dataGridColumn);

			dataGridColumn = new DataGridColumn('category');
			dataGridColumn.headerText = 'Kategorie';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 118;
			columns.push(dataGridColumn);

			dataGridColumn = new DataGridColumn('code');
			dataGridColumn.headerText = 'Code';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 85;
			columns.push(dataGridColumn);
			
			dataGridColumn = new DataGridColumn('themetext');
			dataGridColumn.headerText = 'Thema';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 160;
			columns.push(dataGridColumn);

			dataGridColumn = new DataGridColumn('detail');
			dataGridColumn.headerText = 'Detail';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 80;
			columns.push(dataGridColumn);

			dataGridColumn = new DataGridColumn('people');
			dataGridColumn.headerText = 'Mitarbeiter';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 85;
			columns.push(dataGridColumn);
			
			dataGridColumn = new DataGridColumn('location');
			dataGridColumn.headerText = 'Standort';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 85;
			columns.push(dataGridColumn);
			
			dataGridColumn = new DataGridColumn('session');
			dataGridColumn.headerText = 'Session';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 80;
			columns.push(dataGridColumn);
			
			dataGridColumn = new DataGridColumn('order');
			dataGridColumn.headerText = 'Prio';
			dataGridColumn.sortable = true;
			columns.push(dataGridColumn);
			
			dataGridColumn = new DataGridColumn('status');
			dataGridColumn.headerText = 'Status';
			dataGridColumn.sortable = true;
			columns.push(dataGridColumn);
			dgInterviews.columns = columns;
			dgInterviews.addEventListener(ListEvent.ITEM_DOUBLE_CLICK, onInterviewSelected);
			dgInterviews.addEventListener(DataGridEvent.HEADER_RELEASE, onHeaderSort);
			dgInterviews.removeAll();
			_sortField = "category";
			if(_interviews)
				fillGrid();
		}

		private function onHeaderSort(event : DataGridEvent) : void 
		{
			var colIndex : int = event.columnIndex;
			var column : DataGridColumn = dgInterviews.columns[colIndex];
			_sortDescending = column.sortDescending;
			switch(colIndex)
			{
				case 1:
					_sortField = 'category';
					break;
				case 2:
					_sortField = 'code';
					break;
				case 3:
					_sortField = 'themetext';
					break;
				case 4:
					_sortField = 'detail';
					break;
				case 5:
					_sortField = 'people';
					break;
				case 6:
					_sortField = 'location';
					break;
				case 7:
					_sortField = 'order';
					break;
				case 8:
					_sortField = 'status';
					break;
			}
			Logger.writeLog("onHeaderSort", _sortField, _sortDescending);
		}

		private function onInterviewSelected(event : ListEvent) : void
		{
			_interviewID = dgInterviews.selectedItem.id;
			dispatchEvent(new Event(Event.SELECT));
		}

		public function get interviewID() : Number
		{
			return _interviewID;
		}

		public function set interviews(interviews : Array) : void
		{
			txtAmount.text = interviews.length.toString() + ' Interviews in Liste';
			_interviews = interviews;
			if(_initialized)
				fillGrid();
		}

		public function setSelected(id : uint) : void 
		{
			for each (var item:Object in dgInterviews.dataProvider)
			{
				if(item.id == id)
				{
					dgInterviews.selectedItem = item;
					break;
				}
			}
		}

		public function clear() : void
		{
			dgInterviews.removeAll();
		}
	}
}
