package view 
{
	import fl.controls.Button;
	import fl.controls.RadioButton;

	import model.Role;

	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;

	/**
	 * @author gerard
	 */
	public class MainLoginView extends View 
	{
		public var rbCentral : RadioButton;
		public var rbSupport : RadioButton;
		public var rbStudio : RadioButton;
		public var rbAutonom : RadioButton;
		public var btnLogin : Button;
		public var txtUser : TextField;
		public var txtPassword : TextField;
		private var _role : String;

		public function MainLoginView()
		{
			addEventListener(Event.ADDED_TO_STAGE, initUI);
		}

		private function initUI(event : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initUI);
			rbCentral.addEventListener(Event.CHANGE, onRButton);
			rbSupport.addEventListener(Event.CHANGE, onRButton);
			rbStudio.addEventListener(Event.CHANGE, onRButton);
			rbAutonom.addEventListener(Event.CHANGE, onRButton);
			btnLogin.addEventListener(MouseEvent.CLICK, onLogin);
			rbCentral.selected = true;
			checkRButtons();
			btnLogin.addEventListener(MouseEvent.CLICK, onLogin);
		}

		private function onRButton(event : Event) : void 
		{
			checkRButtons();
		}

		private function checkRButtons() : void 
		{
			txtPassword.visible = true;
			if(rbStudio.selected)
			{
				_role = Role.STUDIO;
			}
			if(rbCentral.selected)
			{
				_role = Role.CONTROL;
			}
			if(rbAutonom.selected)
			{
				_role = Role.AUTONOM;
			}
			if(rbSupport.selected)
			{
				_role = Role.SUPPORT;
			}
		}

		private function onLogin(event : MouseEvent) : void 
		{
			dispatchEvent(new Event(Event.COMPLETE));
		}

		public function get role() : String
		{
			return _role;
		}

		public function get user() : String
		{
			return txtUser.text;
		}

		public function get password() : String
		{
			return txtPassword.text;
		}
	}
}
