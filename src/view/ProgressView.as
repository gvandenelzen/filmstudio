package view 
{
	import flash.events.MouseEvent;
	import flash.display.MovieClip;
	import flash.display.Sprite;

	/**
	 * @author gerard
	 */
	public class ProgressView extends Alert 
	{
		public var mcProgress : MovieClip; 
		private var _maxWidth : Number;
		public function ProgressView(message:String='')
		{
			super(message);
		}
		override protected function initAlert() : void 
		{
		
				x = stage.stageWidth / 2 - width / 2;
				y = stage.stageHeight / 2 - height / 2;
			
			_maxWidth = mcProgress.width;
			if(btnOK  )
				btnOK.visible = false;
			progress = 0;
				setBlocker();
			show();
		}

		private function setBlocker() : void 
		{
			var blocker:Sprite=new Sprite();
			var xPos:int=-(stage.stageWidth / 2 - width / 2);
			var yPos:int=-(stage.stageHeight / 2 - height / 2);
			with(blocker.graphics)
			{
				beginFill(0,0.5);
				drawRect(xPos,yPos,stage.stageWidth,stage.stageHeight);
			}
			blocker.addEventListener(MouseEvent.MOUSE_DOWN, onMouseIsDown);
			addChildAt(blocker,0);
		}

		private function onMouseIsDown(event : MouseEvent) : void 
		{
		}

		public function set progress(value : Number) : void
		{
			if(value > 1)
				value = 1;
			if(value < 0)
				value = 0;
			mcProgress.width = value * _maxWidth;
		}
	}
}
