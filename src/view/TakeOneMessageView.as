package view 
{
	import flash.display.MovieClip;
	import flash.text.TextField;

	/**
	 * @author gerard
	 */
	public class TakeOneMessageView extends View 
	{
		public var txtMessage : TextField;
		public var  mcWaitIndicator : MovieClip;

		public function TakeOneMessageView()
		{
			super();
			txtMessage.text = '';
		}

		override public function  hide() : void
		{
			super.hide();
			isLoading = false;
		}

		public function set message(message : String) : void
		{
			txtMessage.text = message;
		}

		public function set isLoading(state : Boolean) : void
		{
			mcWaitIndicator.visible = state;
			if(state)
				mcWaitIndicator.play();
			else
				mcWaitIndicator.stop();
		}
	}
}
