package view 
{
	import controler.DataManager;

	import dbdata.People;
	import dbdata.WebSite;

	import fl.controls.CheckBox;
	import fl.controls.DataGrid;
	import fl.controls.dataGridClasses.DataGridColumn;
	import fl.events.DataGridEvent;
	import fl.events.ListEvent;

	import settings.Constants;

	import utils.DateTimeUtils;
	import utils.Logger;

	import flash.events.Event;
	import flash.text.TextField;
	import flash.utils.Dictionary;

	/**
	 * @author gerard
	 */
	public class WebSiteListView extends View 
	{
		public var dgWebsites : DataGrid;
		public var txtAmount : TextField;
		public var cbxActive : CheckBox;
		private var _sortField : String;
		private var _sortDescending : Boolean;
		private var _selectionID : Number;
		private var _initialized : Boolean;
		private var _websites : Array;
		private var _currentWebSite : WebSite;
		private var _webSitesDict : Dictionary;
		private var _dataManager : DataManager;

		
		public function WebSiteListView() : void
		{
			addEventListener(Event.ADDED_TO_STAGE, initUI);
		}

		private function initUI(event : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initUI);
			cbxActive.addEventListener(Event.CHANGE, onActiveChanged);
			cbxActive.selected=true;
			_initialized = true;
			
			initGrid();
		}

		private function onActiveChanged(event : Event) : void 
		{
			selectWebsites();
		}

		private function fillGrid() : void
		{
			dgWebsites.removeAll();
			_websites.sortOn('editdate', Array.DESCENDING);
			for each (var obj:Object in _websites)
			{
				dgWebsites.addItem(obj);
			}
			txtAmount.text = _websites.length + ' Websites in Liste';
			dgWebsites.validateNow();
			dgWebsites.verticalScrollBar.scrollPosition = 0;
		}

		private function initGrid() : void 
		{

			Logger.writeLog('WebsitesView initGrid ');
			var columns : Array = [];
			var dataGridColumn : DataGridColumn = new DataGridColumn('id');
			dataGridColumn.headerText = '';
			dataGridColumn.sortable = false;
			dataGridColumn.width = 0;
			dataGridColumn.visible = false;
			columns.push(dataGridColumn);

			dataGridColumn = new DataGridColumn('title');
			dataGridColumn.headerText = 'Titel';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 100;
			columns.push(dataGridColumn);
			
			dataGridColumn = new DataGridColumn('text');
			dataGridColumn.headerText = 'Text';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 100;
			columns.push(dataGridColumn);
			
			dataGridColumn = new DataGridColumn('created');
			dataGridColumn.headerText = 'Erstellt';
			dataGridColumn.sortable = true;
			dataGridColumn.labelFunction = transFormCreationDate;
			dataGridColumn.width = 100;
			columns.push(dataGridColumn);
			
			dataGridColumn = new DataGridColumn('expire');
			dataGridColumn.headerText = 'expire';
			dataGridColumn.sortable = true;
			dataGridColumn.labelFunction = transFormExpireDate;
			dataGridColumn.width = 100;
			columns.push(dataGridColumn);
			
						
			dataGridColumn = new DataGridColumn('notes');
			dataGridColumn.headerText = 'Notizen';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 160;
			columns.push(dataGridColumn);

			dataGridColumn = new DataGridColumn('person');
			dataGridColumn.headerText = 'Adressat';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 135;
			columns.push(dataGridColumn);
			
			
			
			dgWebsites.columns = columns;
			dgWebsites.addEventListener(ListEvent.ITEM_DOUBLE_CLICK, onWebsiteSelected);
			dgWebsites.addEventListener(DataGridEvent.HEADER_RELEASE, onHeaderSort);
			dgWebsites.removeAll();
			_sortField = "editdate";
			
			if(_websites)
			{
				selectWebsites();
				fillGrid();
			}
		}

		private function transFormCreationDate(item : Object) : String
		{
			var date : Number = item.created;
			return formatDate(date);
		}

		private function transFormExpireDate(item : Object) : String
		{
			var date : Number = item.expire;
			return formatDate(date);
		}

		private function formatDate(date : Number) : String 
		{
			if(date == 0 || !date)
				return "";
			return DateTimeUtils.fileStampToFormattedDate(date);
		}

		private function onHeaderSort(event : DataGridEvent) : void 
		{
			var colIndex : int = event.columnIndex;
			var column : DataGridColumn = dgWebsites.columns[colIndex];
			_sortDescending = column.sortDescending;
			switch(colIndex)
			{

				case 1:
					_sortField = 'title';
					break;
				case 2:
					_sortField = 'text';
					break;
				case 3:
					_sortField = 'created';
					break;
				case 4:
					_sortField = 'expire';
					break;
				case 5:
					_sortField = 'notes';
					break;
				case 6:
					_sortField = 'person';
					break;
			}
			Logger.writeLog("onHeaderSort", _sortField, _sortDescending);
		}

		private function onWebsiteSelected(event : ListEvent) : void
		{
			_selectionID = dgWebsites.selectedItem.id;
			_currentWebSite = _webSitesDict[_selectionID];
			dispatchEvent(new Event(Event.SELECT));
		}

		public function get websiteID() : Number
		{
			return _selectionID;
		}

		public function set webSites(webSites : Dictionary) : void
		{
			_webSitesDict = webSites;
			selectWebsites();
		}

		private function selectWebsites() : void 
		{
			_websites = [];
			var todayStamp : Number = Number(DateTimeUtils.dateToTimeStamp());
			for each( var webSite:WebSite in _webSitesDict)
			{
				if(cbxActive.selected && webSite.ExpireTimestamp < todayStamp)
					continue;
				_websites.push({id:webSite.ID, title:webSite.Title, text:webSite.Text, created:webSite.CreatedTimestamp, expire:webSite.ExpireTimestamp, notes:webSite.Notes, person:getPerson(webSite.PersonID)});
			}
			txtAmount.text = _websites.length.toString() + ' Websites in Liste';
			if(_initialized)
				fillGrid();
		}

		private function getPerson(personID : uint) : String 
		{
			if(personID==0)
				return "";
			Logger.writeLog("getPerson ",personID,_dataManager);
			var person:People=_dataManager.people[personID] as People;
			if(person)
				return person.FullName;
			return personID.toString();
		}

		public function setSelected(id : uint) : void 
		{
			for each (var item:Object in dgWebsites.dataProvider)
			{
				if(item.id == id)
				{
					dgWebsites.selectedItem = item;
					break;
				}
			}
		}

		public function clear() : void
		{
			dgWebsites.removeAll();
		}

		public function get currentWebSite() : WebSite
		{
			return _currentWebSite;
		}
		public function set dataManager(dataManager : DataManager) : void
		{
			_dataManager = dataManager;
		}
	}
}
