package view 
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;

	/**
	 * @author gerard
	 */
	public class TakeOnePrintView extends View 
	{
		public var btnPrint : MovieClip;
		public var btnBack : MovieClip;

		public function TakeOnePrintView()
		{
			super();
		}

		override protected function initView() : void 
		{
			btnPrint.addEventListener(MouseEvent.CLICK, onPrint);
			btnBack.addEventListener(MouseEvent.CLICK, onBack);
		}

		private function onPrint(event : MouseEvent) : void 
		{
			dispatchEvent(new Event(Event.COMPLETE));
		}

		private function onBack(event : MouseEvent) : void 
		{
			dispatchEvent(new Event(Event.CANCEL));
		}

		override public function show() : void
		{
			super.show();
		}
	}
}
