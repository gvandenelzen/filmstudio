package view 
{
	import fl.controls.DataGrid;
	import fl.controls.dataGridClasses.DataGridColumn;
	import fl.events.DataGridEvent;
	import fl.events.ListEvent;

	import utils.DateTimeUtils;
	import utils.Logger;

	import flash.events.Event;
	import flash.text.TextField;

	/**
	 * @author gerard
	 */
	public class RecordingsView extends View 
	{
		public var dgRecordings : DataGrid;
		public var txtAmount : TextField;

		private var _sortField : String;
		private var _sortDescending : Boolean;
		private var _recordingID : Number;
		private var _initialized : Boolean;
		private var _recordings : Array;

		public function RecordingsView() : void
		{
			addEventListener(Event.ADDED_TO_STAGE, initUI);
		}

		private function initUI(event : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initUI);
			_initialized = true;
			txtAmount.text = "";
			initGrid();
		}

		private function fillGrid() : void
		{
			Logger.writeLog('RecordingsvIew fillGrid', txtAmount.text);
			txtAmount.text = _recordings.length.toString()+' Aufnahmen in Liste';
			try
			{
				dgRecordings.removeAll();
				for each (var obj:Object in _recordings)
				{
					if(obj)
					dgRecordings.addItem(obj);
				}
			}
			catch(e : Error)
			{
				Logger.writeLog('FILL GRID ', e.message);
			}
			dgRecordings.sortItemsOn('timestamp', Array.DESCENDING);
			dgRecordings.validateNow();
		}

		private function initGrid() : void 
		{
			Logger.writeLog('InterviewsView initGrid ');
			var columns : Array = [];
			var dataGridColumn : DataGridColumn = new DataGridColumn('id');
			dataGridColumn.headerText = '';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 0;
			dataGridColumn.visible=false;
			columns.push(dataGridColumn);

			dataGridColumn = new DataGridColumn('timestamp');
			dataGridColumn.headerText = 'Startzeit';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 128;
			dataGridColumn.labelFunction = formatDate;
			columns.push(dataGridColumn);

			dataGridColumn = new DataGridColumn('notes');
			dataGridColumn.headerText = 'Notizen';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 90;
			columns.push(dataGridColumn);
			
			dataGridColumn = new DataGridColumn('duration');
			dataGridColumn.headerText = 'Länge';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 48;
			columns.push(dataGridColumn);

			dataGridColumn = new DataGridColumn('status');
			dataGridColumn.headerText = 'Status';
			dataGridColumn.sortable = true;
			columns.push(dataGridColumn);
			
			dataGridColumn = new DataGridColumn('videos');
			dataGridColumn.headerText = 'V';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 24;
			columns.push(dataGridColumn);
			
			dgRecordings.columns = columns;
			dgRecordings.addEventListener(ListEvent.ITEM_DOUBLE_CLICK, onRecordingSelected);
			dgRecordings.addEventListener(DataGridEvent.HEADER_RELEASE, onHeaderSort);
			dgRecordings.removeAll();
			_sortField = "timestamp";
			if(_recordings)
				fillGrid();
		}

		private function formatDate(item : Object) : String 
		{
			return DateTimeUtils.fileStampToFormattedDate(item.timestamp);
		}

		private function onHeaderSort(event : DataGridEvent) : void 
		{
			var colIndex : int = event.columnIndex;
			var column : DataGridColumn = dgRecordings.columns[colIndex];
			_sortDescending = column.sortDescending;
			switch(colIndex)
			{
				case 1:
					_sortField = 'timestamp';
					break;
				case 2:
					_sortField = 'notes';
					break;
				case 3:
					_sortField = 'duration';
					break;
				case 4:
					_sortField = 'status';
					break;
			}
			Logger.writeLog("onHeaderSort", _sortField, _sortDescending);
		}

		private function onRecordingSelected(event : ListEvent) : void
		{
			_recordingID = dgRecordings.selectedItem.id;
			dispatchEvent(new Event(Event.SELECT));
		}

		public function get recordingID() : Number
		{
			return _recordingID;
		}

		public function set recordings(recordings : Array) : void
		{
			Logger.writeLog('RecordingsView set Recordings')
			_recordings = recordings;
			if(_initialized)
				fillGrid();
		}

		public function clear() : void 
		{
			dgRecordings.removeAll();
		}
	}
}
