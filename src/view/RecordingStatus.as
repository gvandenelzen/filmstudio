package view 
{
	import controls.ProgressBar;

	import utils.DateTimeUtils;
	import utils.Logger;

	import flash.display.MovieClip;
	import flash.events.TimerEvent;
	import flash.geom.ColorTransform;
	import flash.text.TextField;
	import flash.utils.Timer;
	import flash.utils.getTimer;

	/**
	 * @author gerard
	 */
	public class RecordingStatus extends View 
	{
		private static const ORANGE : ColorTransform = new ColorTransform();
								ORANGE.color = 0xFC9539;
		private static const GREEN : ColorTransform = new ColorTransform();
								GREEN.color = 0x00FF00;

		public var txtStatus : TextField;
		public var mcRecording : MovieClip;

		private var _recordTimer : Timer;
		private var _startRecording : Number;
		private var _lapsed : Number;
		private var _recordProgress : ProgressBar; 
		private var _desiredDuration : Number = 0;

		public function RecordingStatus()
		{
			super();
			_recordTimer = new Timer(100, 0);
			_recordTimer.addEventListener(TimerEvent.TIMER, onRecordTimer);
			_lapsed = 0;
		}

		public function set isRecording(isRecording : Boolean) : void
		{
			Logger.writeLog('RecordStatus isRecording', isRecording, _desiredDuration);

			if(isRecording)
			{
				_startRecording = getTimer();
				_recordTimer.start();
				mcRecording.visible = true;
				if(_desiredDuration != 0)
				{
					if(_recordProgress)
						_recordProgress.visible = true;
				}
			}
			else
			{
				_recordTimer.stop();
				txtStatus.text = '';
				mcRecording.visible = false;
				if(_recordProgress)
				{
					_recordProgress.visible = false;
					_recordProgress.value = 0;
				}
			}
		}

		private function showRecordProgress() : void 
		{
			if(!_recordProgress)
				return;
			_recordProgress.value = _lapsed / _desiredDuration;
			if(_recordProgress.value >= 1)
				_recordProgress.transform.colorTransform = ORANGE;
			else
				_recordProgress.transform.colorTransform = GREEN;
		}

		private function onRecordTimer(event : TimerEvent) : void 
		{
			_lapsed = (getTimer() - _startRecording) / 1000;
			txtStatus.text = 'Aufnahme läuft: ' + DateTimeUtils.secondsToMS(_lapsed);
			if(_recordProgress)
			{
				if(_recordProgress.visible)
					showRecordProgress();
			}
		}

		public function init() : void 
		{
			reset();
			show();
		}

		public function reset() : void 
		{
			_lapsed = 0;
			txtStatus.text = '';
			mcRecording.visible = false;
		}

		public function set recordProgress(recordProgress : ProgressBar) : void
		{
			_recordProgress = recordProgress;
			_recordProgress.value=0;
		}

		public function set desiredDuration(desiredDuration : Number) : void
		{
			Logger.writeLog('RecordingStatus desiredDuration',desiredDuration);
			_desiredDuration = desiredDuration;
		}
	}
}
