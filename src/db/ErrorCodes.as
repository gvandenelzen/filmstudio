package db 
{

	/**
	 * @author Gérard van den Elzen, © DMT 2010 
	 */
	public class ErrorCodes 
	{
		
		public static const BAD_ELEMENT_XML:String="badelementxml";

		public static const FILE_NOT_SAVED:String="filenotsaved";
		public static const FILE_NOT_FOUND:String="filenotfound";

		public static const IMAGE_NOT_FOUND : String = 'imagenotfound';

		public static const NOT_FOUND : String = 'notfound';
		public static const IO_ERROR : String = 'ioerror'	;
		public static const TIME_OUT : String = 'timeout'	;
		public static const DB_ERROR : String = 'dberror';
		public static const MESSAGE_NOT_SEND : String = 'messagenotsend';
		public static const EMAIL_ERROR : String = 'mailfailed';
		public static const MESSAGE_EMPTY : String = 'messagempty';
		public static const SECURITY_ERROR : String = 'securityerror';
		public static const BAD_XML : String = 'badxml';
		public static const BAD_JSON : String = 'badjson';
		public static const UNKNOWN_ELEMENT : String = "unknownelement";
		public static const UNDEFINED_ERROR : String = "undefinederror";
		
		private static const VALID_CODES:Array=[TIME_OUT,BAD_ADDRESS,MESSAGE_EMPTY,BAD_ELEMENT_XML,FILE_NOT_SAVED,FILE_NOT_FOUND,IMAGE_NOT_FOUND,NOT_FOUND,IO_ERROR,DB_ERROR,BAD_XML,UNKNOWN_ELEMENT,MESSAGE_NOT_SEND,UNDEFINED_ERROR];
		public static const BAD_ADDRESS : String = "badaddres";

		public static function iValidErrorCode(code:String):Boolean
		{
			if(VALID_CODES.indexOf(code)==-1)
				return false;
				
			return true;
		}
	}
}
