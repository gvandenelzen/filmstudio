package app 
{
	import events.NavigationEvent;

	import fl.controls.Button;

	import settings.Application;

	import flash.events.MouseEvent;

	/**
	 * @author gerard
	 */
	public class Navigation extends WindowedApp 
	{
		public var btnRecording : Button;
		public var btnInterviews : Button;
		public var btnVideoAdmin : Button;
		public var btnPublishing : Button;
		public var btnPersons : Button;
		public var btnThemes : Button;
		public var btnHome : Button;
		public var btnQuit : Button;
		private var _buttons : Array;

		public function Navigation()
		{
		}

		override protected function init() : void 
		{
			_id=Application.NAVIGATION;
			initButtons();
			dispatchReadyEvent() ;
		}

		private function initButtons() : void 
		{
			btnRecording.label = "Aufnahme";
			btnInterviews.label = "Interviews";
			btnVideoAdmin.label = "Videoverwaltung";
			btnPublishing.label = "Publishing";
			btnPersons.label = "Personen";
			btnThemes.label = "Themen";
			btnHome.label = "Service";
			btnQuit.label = "Beenden";
			
			_buttons = [btnRecording,btnInterviews,btnVideoAdmin,btnVideoAdmin,btnPublishing,btnPersons,btnThemes,btnHome,btnQuit];
			for each(var button:Button in _buttons)
			{
				button.addEventListener(MouseEvent.CLICK, onButtonClicked);
			}
		}

		private function onButtonClicked(event : MouseEvent) : void 
		{
			var navEvent : NavigationEvent;
			var button : Button = event.currentTarget as Button;
			switch(button)
			{
				case btnRecording:
					navEvent = new NavigationEvent(NavigationEvent.WINDOW, Application.RECORDER);
					break;
				case btnInterviews:
					navEvent = new NavigationEvent(NavigationEvent.WINDOW, Application.INTERVIEWS_ADMIN);
					break;
				case btnVideoAdmin:
					navEvent = new NavigationEvent(NavigationEvent.WINDOW, Application.VIDEO_ADMIN);
					break;
				case btnPublishing:
					navEvent = new NavigationEvent(NavigationEvent.WINDOW, Application.PUBLISHING);
					break;
				case btnPersons:
					navEvent = new NavigationEvent(NavigationEvent.WINDOW, Application.USERS_ADMIN);
					break;
				case btnThemes:
					navEvent = new NavigationEvent(NavigationEvent.WINDOW, Application.THEMES_ADMIN);
					break;
				case btnHome:
					navEvent = new NavigationEvent(NavigationEvent.HOME);
					break;
				case btnQuit:
					navEvent = new NavigationEvent(NavigationEvent.EXIT);
					break;
				default:
					navEvent = new NavigationEvent(NavigationEvent.HOME);
			}
			dispatchEvent(navEvent);
		}
	}
}
