package app 
{
	import settings.Application;

	import utils.Logger;

	import view.SimplePlayerView;

	import flash.net.NetStream;

	/**
	 * @author gerard
	 */
	public class BigVideoPlayer extends WindowedApp
	{
		public var mcPlayer : SimplePlayerView;
		private var _netStream : NetStream;

		override protected function init() : void 
		{
			_id=Application.BIG_VIDEO_PLAYER;
			initUIElements();
			dispatchReadyEvent();
		}

		private function initUIElements() : void 
		{
			mcPlayer.show();
		}

		
		override protected function onParameter() : void 
		{
			_netStream  = _params[0] as NetStream;
			if(!_netStream)
				return;
			mcPlayer.netStream = _netStream;
			_netStream.seek(0);
			mcPlayer.canSeek=true;
			mcPlayer.init();
		}

		override public function dispose() : void
		{
			mcPlayer.clearVideo();
		}
		override public function onWindowClosing():void
		{
			Logger.writeLog("BigVideoPlayer onWindowClosing caller ",_caller.id);
			_caller.setCalledWindowResponse(id,[_netStream]);
		}
		override public function setCalledWindowResponse(id:String,param:Array):void
		{
			
		}
	}
}
