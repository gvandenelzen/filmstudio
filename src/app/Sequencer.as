package app 
{
	import settings.Application;

	/**
	 * @author gerard
	 */
	public class Sequencer extends WindowedApp 
	{
		override protected function init() : void 
		{
			_id=Application.SEQUENCER;
			dispatchReadyEvent();
		}
	}
}
