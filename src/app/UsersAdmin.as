﻿package app 
{
	import dbdata.Location;
	import dbdata.People;

	import events.DataEvent;

	import fl.controls.Button;
	import fl.controls.CheckBox;
	import fl.controls.ComboBox;
	import fl.controls.DataGrid;
	import fl.controls.RadioButton;
	import fl.controls.dataGridClasses.DataGridColumn;
	import fl.events.DataGridEvent;
	import fl.events.ListEvent;

	import model.Data;

	import settings.Application;

	import utils.Logger;

	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.text.TextField;
	import flash.ui.Keyboard;
	import flash.utils.Timer;

	/**
	 * @author gerard
	 */
	public class UsersAdmin extends WindowedApp 
	{
		public var mcAlertLocation : MovieClip;
		public var btnNew : Button;
		public var btnSave : Button;
		public var txtTitle : TextField;
		public var txtLastname : TextField;
		public var txtFirstname : TextField;
		public var txtFunction : TextField;
		public var txtAmount : TextField;
		public var txtInternalID : TextField;
		public var txtEmail : TextField;
		public var rbF : RadioButton;
		public var rbM : RadioButton;
		public var cbLocation : ComboBox;
		public var cbxActiv : CheckBox;
		public var cbxActivPersons : CheckBox;
		public var cbxExtern : CheckBox;
		public var txtMessage : TextField;
		public var dgPeople : DataGrid;
		private var _currentUser : People;
		private var _sortField : String;
		private var _sortDescending : Boolean;
		private var _changed : Boolean;
		private var _scrollTimer : Timer;
		private var _dir : Number = 1;

		public function UsersAdmin()
		{
			_currentUser = new People();
		}

		private function scrollDG(event : TimerEvent = null) : void 
		{
			
			var dy : int = 1 * _dir;
			dgPeople.selectedIndex += dy;
			if(dgPeople.selectedIndex < 0)
				dgPeople.selectedIndex == 0;
			if(dgPeople.selectedIndex >= dgPeople.dataProvider.length - 1)
				dgPeople.selectedIndex = dgPeople.dataProvider.length - 1;
			dgPeople.scrollToIndex(dgPeople.selectedIndex);
		}

		override protected function init() : void
		{
			_id = Application.USERS_ADMIN;
			clearDetails();
			initDataGrid();

			
			cbLocation.enabled = true;
			
			_scrollTimer = new Timer(100, 0);

			
			btnNew.enabled = true;
			btnNew.label = 'Neuer Mitarbeiter';
			
			
			btnSave.enabled = false;
			btnSave.label = 'Speichern';
			
			cbxActiv.selected = true;
			btnNew.label = 'Neu';
			clearDetails();
			txtAmount.text = "";
			initListener();
			super.init();
			mcAlertLocation.visible = false;
		}

		private function initListener() : void 
		{
			txtTitle.addEventListener(Event.CHANGE, onUserChanged);
			txtFirstname.addEventListener(Event.CHANGE, onUserChanged);
			txtLastname.addEventListener(Event.CHANGE, onUserChanged);
			txtFunction.addEventListener(Event.CHANGE, onUserChanged);
			txtEmail.addEventListener(Event.CHANGE, onUserChanged);
			cbLocation.addEventListener(Event.CHANGE, onUserChanged);
			cbxActiv.addEventListener(Event.CHANGE, onUserChanged);
			cbxActivPersons.addEventListener(Event.CHANGE, onActiveChanged);
			cbxExtern.addEventListener(Event.CHANGE, onUserChanged);
			rbF.addEventListener(Event.CHANGE, onUserChanged);
			rbM.addEventListener(Event.CHANGE, onUserChanged);
			_scrollTimer.addEventListener(TimerEvent.TIMER, scrollDG);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyIsDown);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyIsUp);
			
			btnSave.addEventListener(MouseEvent.CLICK, onSaveUser);
			btnNew.addEventListener(MouseEvent.CLICK, onNewUser);
		}
		private function removeListener() : void 
		{
			txtTitle.removeEventListener(Event.CHANGE, onUserChanged);
			txtFirstname.removeEventListener(Event.CHANGE, onUserChanged);
			txtLastname.removeEventListener(Event.CHANGE, onUserChanged);
			txtFunction.removeEventListener(Event.CHANGE, onUserChanged);
			txtEmail.removeEventListener(Event.CHANGE, onUserChanged);
			cbLocation.removeEventListener(Event.CHANGE, onUserChanged);
			cbxActiv.removeEventListener(Event.CHANGE, onUserChanged);
			cbxActivPersons.removeEventListener(Event.CHANGE, onActiveChanged);
			cbxExtern.removeEventListener(Event.CHANGE, onUserChanged);
			rbF.removeEventListener(Event.CHANGE, onUserChanged);
			rbM.removeEventListener(Event.CHANGE, onUserChanged);
			_scrollTimer.removeEventListener(TimerEvent.TIMER, scrollDG);
			stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyIsDown);
			stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyIsUp);
			
			btnSave.addEventListener(MouseEvent.CLICK, onSaveUser);
			btnNew.addEventListener(MouseEvent.CLICK, onNewUser);
		}

		private function onKeyIsUp(event : KeyboardEvent) : void 
		{
			_scrollTimer.stop();
		}

		private function onKeyIsDown(event : KeyboardEvent) : void 
		{
			if(stage.focus is TextField)
				return;
			if(event.keyCode == Keyboard.UP || event.keyCode == Keyboard.DOWN)
			{
				if(event.keyCode == Keyboard.UP )
					_dir = -1;
				else
					_dir = 1;
				_scrollTimer.start();
				scrollDG();
				return;
			}
			if(event.keyCode == 186 || event.keyCode == 219 || event.keyCode == 222 || (event.keyCode >= 65 && event.keyCode <= 90))
				setDatagridTo(String.fromCharCode(event.keyCode));
		}

		private function setDatagridTo(letter : String) : void 
		{
			for(var i : int = 0;i < dgPeople.dataProvider.length;i++)
			{
				var item : Object = dgPeople.getItemAt(i);
				var firstletter : String = item.lastname.substr(0, letter.length).toUpperCase();
				if(firstletter == letter)
				{
					dgPeople.selectedIndex = i;
					dgPeople.scrollToIndex(dgPeople.selectedIndex);
					break;
				}
			}
		}

		private function onActiveChanged(event : Event) : void 
		{
			fillDatagrid();
		}

		private function onNewUser(event : MouseEvent) : void
		{

			_changed = true;
			_currentUser = new People();
			cbxActiv.selected = true;
			cbxExtern.selected = false;
			rbM.selected = true;
			clearDetails();
		}

		private function onSaveUser(event : MouseEvent) : void
		{
			Logger.writeLog('onSaveUSer', _currentUser, _changed, 'standALone', _isStandAlone);
			if(_changed)
			{
				saveUser();
			}
			else
			{
				Logger.writeLog('saveUserII', _caller.id, _currentUser.ID);
				if(!_isStandAlone)
				{
					_caller.setCalledWindowResponse(id, [_currentUser.ID]);
					close();
				}
			}
		}

		private function saveUser() : void 
		{
			if(!checkUserData())
				return;
			fillUserRecord();
			Logger.writeLog('saveUser', _currentUser.toString());
			_dataManager.updatePeople(_currentUser);
		}

		private function checkUserData() : Boolean 
		{
			if(cbLocation.selectedIndex == -1)
			{
				
				mcAlertLocation.visible = true;
				return false
			}
			return true;
		}

		private function fillUserRecord() : void 
		{
			_currentUser.LocationID = cbLocation.selectedItem.data;
			_currentUser.Titel = (txtTitle.text);
			_currentUser.LastName = (txtLastname.text) ;
			_currentUser.FirstName = (txtFirstname.text) ; 
			_currentUser.Function = (txtFunction.text);
			_currentUser.InternalID = txtInternalID.text;
			_currentUser.Email = txtEmail.text;
			_currentUser.Sex = (rbF.selected) ? 0 : 1;
			if(cbxActiv.selected) 
				_currentUser.Active = 1;
			else
				_currentUser.Active = 0;
				
			if(cbxExtern.selected) 
				_currentUser.Extern = 1;
			else
				_currentUser.Extern = 0;
		}

		
		private function onUserChanged(event : Event) : void
		{
			if(_currentUser)
				setUserHasChanged();
		}

		private function setUserHasChanged() : void
		{
			txtMessage.text = "";
			_changed = true;
			btnSave.enabled = true;
		}

		private function clearDetails() : void
		{
			txtTitle.text = "";
			txtLastname.text = "";
			txtFirstname.text = ""; 
			txtFunction.text = "";
			txtInternalID.text = ""; 
			txtEmail.text = ""; 
			txtMessage.text = '';
		}

		
		private function initDataGrid() : void
		{
			var columns : Array = [];
			var dataGridColumn : DataGridColumn = new DataGridColumn('id');
			dataGridColumn.headerText = '';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 0;
			dataGridColumn.visible = false;
			columns.push(dataGridColumn);

			dataGridColumn = new DataGridColumn('lastname');
			dataGridColumn.headerText = 'Nachname';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 120;
			columns.push(dataGridColumn);
			
			dataGridColumn = new DataGridColumn('firstname');
			dataGridColumn.headerText = 'Vorname';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 80;
			columns.push(dataGridColumn);

			dataGridColumn = new DataGridColumn('func');
			dataGridColumn.headerText = 'Funktion';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 120;
			columns.push(dataGridColumn);
			
			dataGridColumn = new DataGridColumn('location');
			dataGridColumn.headerText = 'Standort';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 220;
			columns.push(dataGridColumn);
			
			dataGridColumn = new DataGridColumn('activ');
			dataGridColumn.headerText = 'aktiv';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 30;
			columns.push(dataGridColumn);

			dataGridColumn = new DataGridColumn('extern');
			dataGridColumn.headerText = 'Extern';
			dataGridColumn.sortable = true;
			dataGridColumn.width = 30;
			columns.push(dataGridColumn);

			dgPeople.columns = columns;
			dgPeople.addEventListener(ListEvent.ITEM_DOUBLE_CLICK, onUserSelected);
			dgPeople.addEventListener(DataGridEvent.HEADER_RELEASE, onHeaderSort);
			dgPeople.removeAll();
			_sortField = 'lastname';
		}

		private function fillDatagrid() : void
		{
			Logger.writeLog("UserView fillDatagrid");
			dgPeople.removeAll();
			var people : People;
			var location : Location;
			var item : Object;
			var id : uint;
			var firstname : String;
			var lastname : String;
			var func : String;
			var locationname : String;
			var internalID : String;
			var activ : String;
			var extern : String;
			var n : uint = 0;
			for each (people in _dataManager.people)
			{
				if(people.Active == 0 && cbxActivPersons.selected)
					continue;
				
				id = people.ID;
				firstname = people.FirstName;
				lastname = people.LastName ;
				func = people.Function;
				location = _dataManager.locations[people.LocationID];
				if(location)
					locationname = location.Name;
				else
					locationname = people.LocationID.toString();
					
				internalID = people.InternalID;
				activ = "ja";
				if(people.Active != 1)
					activ = "nein";
				extern = "ja";
				if(people.Extern != 1)
					extern = "nein";
				n++;
				item = {id:id, firstname:firstname, lastname:lastname, func:func, location:locationname, activ:activ, extern:extern};
				dgPeople.addItem(item);
			}
			if(_sortDescending)
				dgPeople.dataProvider.sortOn(_sortField, Array.DESCENDING | Array.CASEINSENSITIVE);
			else
				dgPeople.dataProvider.sortOn(_sortField, Array.CASEINSENSITIVE);
				
			txtAmount.text = n + " Personen in der Liste";
			clearUser();
		}

		private function onHeaderSort(event : DataGridEvent) : void 
		{
			var colIndex : int = event.columnIndex;
			var column : DataGridColumn = dgPeople.columns[colIndex];
			_sortDescending = column.sortDescending;
			switch(colIndex)
			{
				case 1:
					_sortField = 'fullname';
					break;
				case 2:
					_sortField = 'func';
					break;
				case 3:
					_sortField = 'location';
					break;
				case 4:
					_sortField = 'activ';
					break;
				case 5:
					_sortField = 'extern';
					break;
			}
			Logger.writeLog("onHeaderSort", _sortField, _sortDescending);
		}

		private function onUserSelected(event : ListEvent) : void
		{
			var userID : uint = dgPeople.selectedItem.id;
			_currentUser = _dataManager.people[userID];
			if (_currentUser)
			{
				fillUserDetails();
			}
		}

		private function fillUserDetails() : void
		{
			mcAlertLocation.visible = false;
			_changed = false;
			if(!_isStandAlone)
				btnSave.enabled = true;
			else
				btnSave.enabled = false;
			clearDetails();
			txtTitle.text = (_currentUser.Titel);
			txtFirstname.text = (_currentUser.FirstName);
			txtLastname.text = (_currentUser.LastName);
			txtFunction.text = (_currentUser.Function);
			txtEmail.text = (_currentUser.Email);
			rbM.selected = (_currentUser.Sex == 1) ? true : false;
			rbF.selected = (_currentUser.Sex == 0) ? true : false;
			if(_currentUser.InternalID)
				txtInternalID.text = _currentUser.InternalID;
				
			for (var i : uint = 0;i < cbLocation.dataProvider.length;i++)
			{
				var item : Object = cbLocation.getItemAt(i);
				if(item.data == _currentUser.LocationID)
				{
					cbLocation.selectedIndex = i;
					break;
				}
			}
			
			cbLocation.enabled = true;
			cbxActiv.selected = (_currentUser.Active == 0) ? false : true;
			cbxActiv.enabled = true;
			
			cbxExtern.selected = (_currentUser.Extern == 0) ? false : true;
			cbxExtern.enabled = true;
		}

		private function setLocation() : void
		{
			cbLocation.removeAll();
			for each (var location:Location in _dataManager.locations)
			{
				cbLocation.addItem({label:location.Name, data:location.ID});
			}
			cbLocation.selectedIndex = -1;
			cbLocation.prompt = 'Bitte wählen';
			cbLocation.addEventListener(Event.CHANGE, onUserChanged);
		}

		
		private function clearUser() : void
		{
			cbLocation.selectedIndex = -1;
			cbLocation.prompt = 'Bitte wählen';
		}

		
		
		override protected function onDataManager() : void
		{
			setLocation();
			fillDatagrid();
		}

		override protected function onDataChanged(event : DataEvent) : void 
		{
			if(closed)
				return;
			switch(event.kind)
			{
				case Data.PEOPLE:
					var people : People = event.data[0] as People;
					if(!people)
					{
						return;
					}
					_currentUser = people;
					txtMessage.text = _currentUser.LastName + "," + _currentUser.FirstName + ' ist geändert.';
					fillDatagrid();
					fillUserDetails();
					setSelectedUser();
					if(!_isStandAlone)
					{
						_caller.setCalledWindowResponse(id, [_currentUser.ID]);
						close();
					}
					break;
			}
		}

		private function setSelectedUser() : void 
		{
			for each( var item:Object in dgPeople.dataProvider)
			{
				if(item.id == _currentUser.ID)
				{
					dgPeople.selectedItem = item;
					break;
				}
			}
		}

		override protected function onParameter() : void 
		{
			if(closed)
				return;
			Logger.writeLog("Themesadmin onParameter", _caller);
			if(!_caller)
				return;
				
			if(!_isStandAlone)
			{
				btnSave.label = 'Auswählen';
				btnSave.enabled = true;
				var userID : int = _params[0];
				_currentUser = _dataManager.people[userID];
				if(_currentUser)
					fillUserDetails();
			}
		}
		override public function dispose():void
		{
			removeListener();
			super.dispose();
		}
		override public function onWindowClosing() : void
		{
			Logger.writeLog('UsersAdmin closing');
			dispose();
			//give the caller something with getCalled window response when neede
			//_caller.setCalledWindowResponse(id,papam);
		}

		override public function setCalledWindowResponse(called : String,param : Array) : void
		{
			//this function can decided through caller what to do with the params
			//can be called by called window as needed
		}
	}
}
