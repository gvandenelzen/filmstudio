package model 
{

	/**
	 * @author gerard
	 */
	public class InterviewFilter 
	{
		public var session:Number;
		public var people:Number;
		public var location:Number;
		public var category:Number;
		public var status:Number;
		public var active:Number;
		public function toString():String
		{
			return "Session ;"+session +" Employee:"+people+' Location:'+location+' Category:'+category+  'Status:'+status+ ' Active:'+active;
		}
	}
}
