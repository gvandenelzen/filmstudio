package model 
{
	import flash.utils.Dictionary;

	/**
	 * @author gerard
	 * 
	 * 
	 * 
	 */

	
	public class APIError 
	{
		private static const  ERRORS : Dictionary = new Dictionary();

	ERRORS[5] = " - VirtualDub Fehler";
	ERRORS[1073741819] = " - VirtualDub Transcoding ist abgestürzt 78002 - Schreiben in lokale Datenbank nicht möglich";
	ERRORS[78003] = " - Job Datensatz konnte nicht gelesen werden (lokale DB)";
	ERRORS[78004] = " - Job Datensatz konnte nicht aktualisiert werden (lokale DB)";
	ERRORS[78005] = " - Job-Prozess konnte nicht gestartet werden";
	ERRORS[78006] = " - Eintrag in der lokalen Queue konnte nicht geschrieben werden";
	ERRORS[78007] = " - Job Status konnte nicht gelesen werden (lokale DB)";
	ERRORS[78008] = " - Unbekannter Befehl";
	ERRORS[78009] = " - Externe JobID nicht in der lokalen Datenbank gefunden";
	ERRORS[78010] = " - Kommandozeile konnte nicht interpretiert werden";
	ERRORS[78011] = " - Ungültige Kommandozeilenparameter";
	ERRORS[78020] = " - Transcoding Script nicht gefunden";
	ERRORS[78021] = " - Transcoding Script konnte nicht gelesen werden";
	ERRORS[78030] = " - Datei zum Upload nicht gefunden";
	ERRORS[78040] = " - Fehler beim lesen der lokalen Queue";
	ERRORS[78050] = " - Capture 'Start' mit bereits verwendeter externer JobID";
	ERRORS[78051] = " - Capture Stop/Cancel mit unbekannter externer JobID"; 

		public static function getErrorMessage(nr : uint) : String
		{
			if(ERRORS[nr])
				return ERRORS[nr];
			return 'Unbekannter Fehler';
		}
	}
}
