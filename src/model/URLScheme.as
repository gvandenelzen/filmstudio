package model 
{

	/**
	 * @author gerard
	 */
	public class URLScheme 
	{
		public static const APP:String='app:';
		public static const FILE:String='file:';
	}
}
