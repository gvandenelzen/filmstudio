package model 
{

	/**
	 * @author gerard
	 */
	public class Data 
	{
		public static const UPLOAD : String = 'upload';
		public static const FMSCONNECTION : String = 'fmsconnection';
		public static const FMSFILE : String = 'fmsfile';
		public static const INTERVIEWS : String = 'interviews';
		public static const PEOPLE : String = 'people';
		public static const VIDEOS : String = 'video';
		public static const VIDEO_EDIT : String = 'videoedit';
		public static const VIDEO_FOR_LOCATION : String = 'videoforlocation';
		public static const RECORDING_VIDEOS : String = 'recordingvideos';
		public static const VIDEOSEQUENCES : String = 'videosequence';
		public static const THEMES : String = "themes";
		public static const RECORDINGS : String = "recordings";
		public static const RECORDINGFILES : String = "RecordingFiles";
		public static const ENCODING_JOB : String = 'encodingjob';
		public static const BASE_TABLES : String = "BaseTables";
		public static const RECORDING_FILE : String = "RecordingFile";
		public static const VIDEO_FOR_WEBSITE : String = "videoforwebsite";
		public static const WEBSITES : String = 'websites';
	}
}
