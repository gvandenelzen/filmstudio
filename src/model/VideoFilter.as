package model 
{

	/**
	 * @author gerard
	 */
	public class VideoFilter 
	{
		public var session:Number;
		public var people:Number;
		public var location:Number;
		public var category:Number;
		public var themecode:String;
		public var status:Number;
		public var active:Number;
		public var reference:Number;
		
		public function toString():String
		{
			return "Session ;"+session +" Employee:"+people+' Location:'+location+' Category:'+category+ 'ThemenCode: '+themecode+ ' Status:'+status+ ' Active:'+active+ ' Reference:'+reference;
		}
	}
}
