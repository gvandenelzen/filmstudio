package model 
{

	/**
	 * @author gerard
	 */
	public class InterviewData extends PublicDataHolder 
	{
		public var id : String ;
		public var session : String = "";
		public var location : String = "";
		public var employee : String = "";
		public var funktion : String = "";
		public var category : String = "";
		public var theme : String = "";
		public var details : String = "";
		public var promptText : String = "";
		public var themeCode : String="";

		public function InterviewData()
		{
			super();
		}
	}
}
