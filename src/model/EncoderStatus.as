package model 
{
	import utils.Logger;

	/**
	 * @author gerard
	 * start-flash - Startet den Browser mit der Flash Anwendung

	 * Alle Aufrufe liefern immer den aktuellen Status als JSON-Objekt zurück:
	{"status":{"browser-running":0,"flash-fullscreen":0,"video-signal":1,"encoder-streaming":0}}

	0 = nein, false
	1 = ja, true 

	 */
	public class EncoderStatus 
	{
		public static const BROWSER_RUNNING : String = "browser-running";
		public static const FLASH_FULLSCREEN : String = "flash-fullscreen";
		public static const VIDEO_SIGNAL : String = "video-signal";
		public static const ENCODER_STREAMING : String = "encoder-streaming";
		public static const ENCODER_RECORDING : String = "recording";
		public var browserRunning : Boolean;
		public var flashFullScreen : Boolean;
		public var videoSignal : Boolean;
		public var encoderStreaming : Boolean;
		public var recording : Boolean;

		function EncoderStatus(object : Object = null)
		{
			if(object)
				parse(object);
		}

		private function parse(object : Object) : void
		{
			var bool : Boolean;
			for  (var prop:String in object)
			{
				bool = (object[prop] == 1) ? true : false;
				switch(prop)
				{
					case BROWSER_RUNNING:
						browserRunning = bool;
						break;
					case FLASH_FULLSCREEN:
						flashFullScreen = bool;
						break;
					case VIDEO_SIGNAL:
						videoSignal = bool;
						break;
					case ENCODER_STREAMING:
						encoderStreaming = bool;
						break;
					case ENCODER_RECORDING:
						recording = bool;
						break;
				}
			}
		}
	}
}
