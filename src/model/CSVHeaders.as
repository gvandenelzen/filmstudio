package model 
{

	/**
	 * @author gerard
	 */
	public class CSVHeaders 
	{
		// category:categoryname, code:code, themetext:themetext, detail:detail, people:peoplename, location:locationname, status:statusname};
		
		public static const INTERVIEW:Array=['Kategorie','Themencode','Thementext','Themendetail','Mitarbeiter','Funktion','Standort','Region','Notizen','Prompter'];
	}
}
