package model 
{

	/**
	 * @author gerard
	 */
	public class DBRequest 
	{
		public static const UPLOAD_FILE : String = "uploadfile";
		public static const REMOVE_FMS_FILE : String = "deletefmsfile";
		public static const REMOVE_ENCODING_JOB : String = "removeencodingjob";
		public static const GET_INTERVIEWS : String = "getinterviews";
		public static const GET_VIDEO_FOR_LOCATION : String = "getvideoforlocation";
		public static const GET_VIDEO_FOR_WEBSITE : String = "getvideoforwebsite";
		public static const GET_THEMES : String = "getthemes";
		public static const GET_RECORDINGS : String = "getrecordings";
		public static const GET_RECORDING : String = "getrecording";
		public static const GET_VIDEOEDIT : String = "getvideoedit";
		public static const GET_VIDEOS : String = "getvideos";
		public static const GET_WEBSITES : String = "getwebsites";
		public static const UPDATE_RECORDING : String = "updaterecording";
		public static const UPDATE_ENCODING_JOB : String = "updateencodingjob";
		public static const GET_ENCODING_JOB : String = "getencodingjob";
		public static const UPDATE_RECORDINGFILE : String = "updaterecordingfile";
		public static const UPDATE_VIDEO_FOR_WEBSITE : String = "updatevideoforwebsite";
		public static const UPDATE_WEBSITE : String = "updatewebsite";
		public static const UPDATE_PEOPLE : String = "updatepeople";
		public static const UPDATE_THEME : String = "updatetheme";
		public static const UPDATE_VIDEO : String = "updatevideo";
		public static const UPDATE_INTERVIEW : String = "updateinterview";
		public static const GET_BASE_TABLES : String = "getbasetables";

		public static const GET_THEME : String = "gettheme";
		public static const GET_INTERVIEW : String = "getinterview";
		public static const GET_PEOPLE : String = "getpeople";
		public static const SELECT_THEMES : String = "getselectedthemes";
		public static const GET_RECORDINGFILES : String = "getrecordingfiles";
		public static const DELETE_RECORDING : String = 'deleterecording';
		public static const UPDATE_VIDEO_EDIT : String = "updatevideoedit";
		public static const GET_RECORDING_FILE : String = "getrecordingfile";

		public static const GET_SELECTION : String = "getselection";
		public static const GET_SELECTIONFILE : String = "getselectionfile";
		public static const GET_SELECTIONPLAYLIST : String = "getselectionplaylist";
		public static const GET_SELECTIONWEBSITE : String = "getselectionplaylist";

		public static const UPDATE_SELECTION : String = "updateselection";
		public static const UPDATE_SELECTIONFILE : String = "updateselectionfile";
		public static const UPDATE_SELECTIONPLAYLIST : String = "updateselectionplaylist";
		public static const UPDATE_SELECTIONWEBSITE : String = "updateselectionplaylist";
		public static const UPDATE_LOCATIONS_FOR_VIDEO : String = "updatelocationsforvideo";
		
		public static const UPDATE_SELECTED_VIDEO : String = "updateselectedvideo";
		public static const GET_RECORDING_VIDEOS : String = "getrecordingvideos";
	}
}

