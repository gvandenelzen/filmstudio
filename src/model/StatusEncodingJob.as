package model 
{

	/**
	 * @author gerard
	 */
	public class StatusEncodingJob 
	{
		public static const ERROR : uint = 1;
		public static const OPEN : uint = 2;
		public static const IN_PROGRESS : uint = 3;
		public static const QUEUD : uint = 4;
		public static const UPLOAD : uint = 6;
		public static const CANCELLED : uint = 8;
		public static const OK : uint = 9;
	}
}
