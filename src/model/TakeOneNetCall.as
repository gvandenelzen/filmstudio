package model 
{

	/**
	 * @author gerard
	 */
	public class TakeOneNetCall 
	{
		//FromController
		public static const INIT_RECORDING : String = ' TakeOneinitRecording';
		public static const RESTART : String = ' TakeOnerestart';
		public static const PRINT : String = "TakeOneSetPrint";
		public static const SELECTION_STARTED : String = "TakeOneSelectionStarted";
		public static const PERSON_SELECTED : String = "TakeOnePersonSelected";
		public static const PERSON_INPUT : String = "TakeOnePersonInput";
		public static const ADJUSTED : String = "TakeOnesetPersonAsjusted";
		public static const CANCEL : String = "TakeOneCANCEL";
		
		public static const ARE_YOU_THERE : String = 'TakeOneRUTHERE';
		public static const I_AM_HERE : String = 'TakeOneIAMHERE';
		public static const MESSAGE : String = 'TakeOnemessage';
		public static const INITIALIZED: String = 'TakeOneInitiaized';
		public static const ERROR : String = "TakeOneError";
		//From Recorder
		public static const RECORDING_STARTED : String = " TakeOneRecordingStarted";
		public static const CAMERA_READY : String = " TakeOneCameraReady";
		public static const RECORDING_READY : String = " TakeOnecancelRecordingReady";
		public static const RECORDING_ERROR : String = " TakeOneRecordingError";
		public static const IMAGE_READY : String = "TakeOneSetImageReady";
		public static const PRINT_READY : String = "TakeOneSetPrintReady";
		public static const PRINT_ERROR : String = "TakeOneSetPrintError";
	}
}
