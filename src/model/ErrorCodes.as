package model 
{

	/**
	 * @author Gérard van den Elzen, © DMT 2010 
	 */
	public class ErrorCodes 
	{
		public static const EXPAND_ERROR:String="expanderror";
		public static const BAD_ELEMENT_XML:String="badelementxml";
		public static const BAD_JSON:String="badjson";
		public static const IMAGE_LIBRARY_NOT_LOADED:String="imagelibrarynotloaded";
		public static const FILEMANAGER_NOT_READY:String="filemanagernotready";
		public static const FILE_NOT_SAVED:String="filenotsaved";
		public static const FILE_NOT_FOUND:String="filenotfound";
		public static const FONT_NOT_FOUND:String="fontnotfound";
		public static const IMAGE_NOT_FOUND : String = 'imagenotfound';
		public static const IMAGE_NOT_UPLOADED : String = 'imagenotuploaded';
		public static const NOT_FOUND : String = 'notfound';
		public static const IO_ERROR : String = 'ioerror'	;
		public static const DB_ERROR : String = 'dberror';
		public static const SECURITY_ERROR : String = 'securityerror';
		public static const BAD_XML : String = 'badxml';
		public static const UNKNOWN_ELEMENT : String = "unknownelement";
		public static const UNDEFINED_ERROR : String = "undefinederror";
		public static const FILE_NOT_PRINTED : String = "filenotprinted";
		public static const NO_CURRENT_DOCUMENT :  String = "nocurrentdocument";
		public static const DOCUMENT_NOT_SAVED : String = "documentnotsaved";
	}
}
