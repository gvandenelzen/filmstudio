package model 
{

	/**
	 * @author gerard
	 */
	public class NetCall 
	{
		public static const GET_DIR_LIST : String = 'getDirList';
		public static const RENAME_FILE : String = 'renameFile';

		public static const INIT_RECORDING : String = 'initRecording';
		public static const START_RECORDING : String = 'startRecording';
		public static const STOP_RECORDING : String = 'stopRecording';
		public static const CANCEL_RECORDING : String = "cancelRecording";
		public static const PLAY_RECORDING : String = 'playRecording';
		public static const RECORDING_CHANGED : String = 'RecordingChanged';
		public static const NO_HD_RECORDING : String = 'NoHDRecording';
		public static const MESSAGE : String = 'message';

		public static const SET_CHAT : String = "setChat";
		public static const CLEAR_CHAT : String = "clearChat";
		public static const DELAY : String = "delay";
		public static const SET_STUDIO_MONITOR : String = "setStudioMonitor";

		public static const SET_ACTUAL_INTERVIEW : String = "setActualInterview";
		public static const INTERVIEW_CHANGED : String = "INTERVIEW_CHANGED";
		public static const PEOPLE_CHANGED : String = "IPEOPLE_CHANGED";
		public static const THEME_CHANGED : String = "THEME_CHANGED";
		public static const RESET_RECORDING : String = "RESET_RECORDING";
		public static const CHANGE_PUBLISH_CODEC : String = "CHANGE_PUBLISH_CODEC";
		public static const CHANGE_ARCHIVE_CODEC : String = "CHANGE_ARCHIVE_CODEC";
	}
}
