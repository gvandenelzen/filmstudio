package model 
{

	/**
	 * @author gerard
	 * start-flash - Startet den Browser mit der Flash Anwendung
	stop-flash - Schließt den Browser
	start-encoder - Startet den Encoder
	stop-encoder - Stoppt den Encoder
	status - Liefert nur den aktuellen Status zurück
	force-start-flash
	 * Alle Aufrufe liefern immer den aktuellen Status als JSON-Objekt zurück:
	{"status":{"browser-running":0,"flash-fullscreen":0,"video-signal":1,"encoder-streaming":0}}

	0 = nein, false
	1 = ja, true 
	 */
	public class EncoderAction 
	{
		public static const START_SWF_ENCODER : String = "?action[]=start-flash&action[]=start-encoder";
		public static const STOP_RECORDING : String = "?action=stop-recording";
		public static const START_RECORDING : String = "?action=start-recording";
		public static const FORCE_START_SWF : String = "?action=force-start-flash";
		public static const START_SWF : String = "?action=start-flash";
		public static const STOP_SWF : String = "?action=stop-flash";
		public static const START_ENCODER : String = "?action=start-encoder";
		public static const STOP_ENCODER : String = "?action=stop-encoder";
		public static const GET_STATUS : String = "?action=status";
		public static const FORCE_START_SWF_AND_ENCODER : String = "?action[]=force-start-flash&action[]=start-encoder";
		public static const STOP_ENCODER_AND_FLASH : String = "?action[]=stop-encoder&action[]=stop-flash&action[]=stop-recording";
		public static const VALID_ACTIONS : Array = [START_SWF_ENCODER,STOP_RECORDING,START_RECORDING,FORCE_START_SWF,START_SWF,STOP_SWF,START_ENCODER,STOP_ENCODER,GET_STATUS,FORCE_START_SWF_AND_ENCODER,STOP_ENCODER_AND_FLASH];
	}
}
