package model 
{

	/**
	 * @author gerard
	 */
	public class Role 
	{
		public static const CONTROL : String = 'control';
		public static const STUDIO : String = 'studio';
		public static const SUPPORT : String = 'support';
		public static const AUTONOM : String = 'autonom';
		private static const VALID_ROLES : Array = [CONTROL,STUDIO,SUPPORT,AUTONOM];

		public static function isValidRole(role:String) : Boolean
		{
			if(VALID_ROLES.indexOf(role) >= 0)
				return true;
			return false;
		}
	}
}
