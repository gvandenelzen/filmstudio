package controler 
{
	import db.ErrorCodes;

	import events.EncoderStatusEvent;

	import model.EncoderAction;
	import model.EncoderStatus;

	import utils.Logger;

	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.utils.Timer;

	/**
	 * @author gerard
	 */
	public class EncoderController extends EventDispatcher 
	{
		private static var _urlLoader : URLLoader;
		private static var _instance : EncoderController;
		private static var _requestPHP : String;
		private static const MAX_RETRIES : Number = 20;
		private static const DELAY : Number = 5000;
		private static var _initTimer : Timer;
		private static var _statusTimer : Timer;

		public function EncoderController()
		{
			super();
		}

		public static function setRequestPHP(requestPHP : String) : void
		{
			_requestPHP = requestPHP;
			_initTimer = new Timer(DELAY, MAX_RETRIES);
			_initTimer.addEventListener(TimerEvent.TIMER, onStatusTimer,false,0,true);
			
			_statusTimer=new Timer(DELAY*20, 0);
			_statusTimer.addEventListener(TimerEvent.TIMER, onStatusTimer,false,0,true);
			_urlLoader = new URLLoader();
			_urlLoader.dataFormat = URLLoaderDataFormat.TEXT;
			_urlLoader.addEventListener(Event.COMPLETE, onLoadComplete);
			_urlLoader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			_urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityError);
		}

		private static function onStatusTimer(event : TimerEvent) : void 
		{
			getEncoderStatus();
		}

		public static function executeAction(action : String) : void
		{
			if(EncoderAction.VALID_ACTIONS.indexOf(action) == -1)
				throw new Error("Invalid Encoder Action");
			if(_requestPHP == null) 
				return;
			var url : String = _requestPHP + action;
			var urlRequest : URLRequest = new URLRequest(url);
			urlRequest.method = URLRequestMethod.GET;
			Logger.writeLog(_instance, "executeAction", url);
			_urlLoader.load(urlRequest);
			switch(action)
			{
				case EncoderAction.STOP_ENCODER:
					_initTimer.stop();
					_statusTimer.stop();
					break;
				case EncoderAction.START_ENCODER:
					_initTimer.start();
					break;
				case EncoderAction.FORCE_START_SWF:
				case EncoderAction.FORCE_START_SWF_AND_ENCODER:
				case EncoderAction.START_SWF:
					_initTimer.start();
					break;
			}
		}	
		private static function getEncoderStatus() : void 
		{
			EncoderController.executeAction(EncoderAction.GET_STATUS);
		}

		private static function onLoadComplete(e : Event) : void
		{
			e.stopImmediatePropagation();
			if (!_urlLoader.data)
			{
				if(_initTimer.currentCount == MAX_RETRIES)
					dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, ErrorCodes.UNKNOWN_ELEMENT));
			}
			else
			{
				parseResponse(_urlLoader.data);
			}
		}

		private static function parseResponse(data : String) : void
		{
			Logger.writeLog("EncoderController encoderResponse: ", data);
			try
			{
				_initTimer.stop();
				_initTimer.reset();
				if(!_statusTimer.running)
					_statusTimer.start();
				var object : Object = JSON.parse(data);
				var statusObject : Object = object['status'];
				dispatchEvent(new EncoderStatusEvent(EncoderStatusEvent.STATUS, new EncoderStatus(statusObject)));
			}
			catch (e : Error)
			{
					dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, ErrorCodes.BAD_JSON));
				return;
			}
		}

		private static function onSecurityError(event : SecurityErrorEvent) : void 
		{
			if(_initTimer.currentCount == MAX_RETRIES)
				dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, ErrorCodes.SECURITY_ERROR));
		}

		private static function onIOError(event : IOErrorEvent) : void 
		{
			if(_initTimer.currentCount == MAX_RETRIES)
				dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, ErrorCodes.IO_ERROR));
		}

		private static function get instance() : EncoderController
		{
			if(!_instance)
				_instance = new EncoderController();
			Logger.writeLog(_instance);
			return _instance;
		}
		public static function addEventListener(eventType : String, onCallBack : Function,useCapture:Boolean=false, priority:int=0,weak:Boolean=true) : void 
		{
			instance.addEventListener(eventType, onCallBack,useCapture,priority,weak);
		}

		public static function dispatchEvent(event : Event) : void 
		{
			_instance.dispatchEvent(event);
		}

		public static function removeEventListener(eventType : String, onCallBack : Function) : void 
		{
			instance.removeEventListener(eventType, onCallBack);
		}
	}
}
