package fms 
{

	/**
	 * @author gerard
	 */
	public class FMSUtils 
	{
		private static const PREFIX:String='mp4:';
		private static const FLV:String='flv';
		private static const F4V:String='f4v';
		private static const MP4:String='mp4';
		public static function reformatVideoSource(videoSource : String) : String 
		{
			if(!videoSource)
				return "";
			var tmpArray:Array=videoSource.split('/');
			if(tmpArray.length>1)
			{
				var baseName:String=tmpArray.pop();
				var path:String=tmpArray.join('/');
			}
			else
			{
				baseName=videoSource;
			}
			tmpArray=baseName.split('.');
			var videoName:String=tmpArray[0];
			var suffix:String=tmpArray[1];
			if(!suffix)
				return videoSource;
				
			if(suffix.toLowerCase()==FLV)
			{
				if(path)
					videoSource=path+'/'+videoName;
				else
					videoSource=videoName;
			}
				
			if(suffix.toLowerCase()==F4V || suffix.toLowerCase()==MP4)
			{
					
				if(path)
					videoSource=path+'/'+baseName;
				else
					videoSource=baseName;
					
				if(videoSource.indexOf(PREFIX)!=0)
					videoSource= PREFIX+videoSource;
			}
			
			return videoSource;
		}
	}
}
