package fms 
{
	import events.FMSEvent;

	import utils.Logger;

	import view.SimplePlayerView;

	import flash.display.MovieClip;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.net.NetConnection;
	import flash.net.NetStream;

	/**
	 * @author gerard
	 */
	public class SimplePlayer extends MovieClip 
	{
		public var mcPlayerView : SimplePlayerView;
		
		private var _netStream : NetStream;
		private var _netConnection : NetConnection;

		public function SimplePlayer()
		{
			addEventListener(Event.ADDED_TO_STAGE, initUI);
		}

		private function initUI(event : Event) : void 
		{
			
			mcPlayerView.show();
			mcPlayerView.addEventListener(FMSEvent.STREAM_NOT_FOUND, onStreamNotFound);
			mcPlayerView.addEventListener(FMSEvent.INVALID_FORMAT, onInvalidFormat);
			removeEventListener(Event.ADDED_TO_STAGE, initUI);		
		}


		private function onInvalidFormat(event : FMSEvent) : void 
		{
			dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, 'Recording hatt falsches format'));
		}

		private function onStreamNotFound(event : FMSEvent) : void 
		{
			dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, 'Recording nicht gefunden'));
		}

		public function playVideo(video : String) : void
		{
			if(!_netConnection)
			{
				dispatchEvent(new ErrorEvent(ErrorEvent.ERROR));
				return;
			}
			if(_netConnection.uri.indexOf('rtmp')==0)
				video = FMSUtils.reformatVideoSource(video);
		
			if(_netStream)
			{
				_netStream.close();
			}
			_netStream = createNetStream();
			canSeek = true;
			mcPlayerView.firstStart=true;
			mcPlayerView.netStream = _netStream;
			Logger.writeLog("SimplePlayer playVideo ",video);
			_netStream.play(video);
		}

		private function createNetStream() : NetStream 
		{
			var netStream : NetStream = new NetStream(_netConnection);
			netStream.bufferTime=0.5;
			netStream.client = {};
			return netStream;
		}

		public function set netConnection(netConnection : NetConnection) : void
		{
			_netConnection = netConnection;
		}

		public function set canSeek(canSeek : Boolean) : void
		{
			mcPlayerView.canSeek = canSeek;
		}

		public function dispose() : void
		{
			if(_netStream)
				_netStream.close();
		}

		public function clearVideo() : void
		{
		
			mcPlayerView.clearVideo();	
			if(_netStream)
				_netStream.close();
		}

		public function clearStream() : void
		{
			mcPlayerView.clearView();
			if(_netStream)
				_netStream.play("");	
		}
		public function set autoStart(value:Boolean):void
		{
			mcPlayerView.autoStart = value;
		}
		public function clearView() : void
		{
			mcPlayerView.clearView();
		}

		public function get netStream() : NetStream
		{
			return _netStream;
		}

		public function set netStream(netStream : NetStream) : void
		{
			if(netStream)
			{
				_netStream = netStream;
				mcPlayerView.netStream = netStream;
				_netStream.resume();
			}
		}

		public function pause() : void 
		{
			mcPlayerView.pauseVideo();
		}
	}
}
