package fms 
{
	import utils.Logger;

	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.utils.Proxy;
	import flash.utils.flash_proxy;

	/**
	 * @author gerard
	 */
	dynamic public class NetCallCatcher extends Proxy 
	{
		private var _eventDispatcher : EventDispatcher;
		private var _method : String;
		private var _parameter : Array;

		public function NetCallCatcher()
		{
			_eventDispatcher = new EventDispatcher();
		}

		public function addEventListener(type : String, listener : Function) : void
		{
			_eventDispatcher.addEventListener(type, listener);
		}

		public function removeEventListener(type : String, listener : Function) : void
		{
			_eventDispatcher.removeEventListener(type, listener);	
		}

		override flash_proxy function callProperty(methodName : *, ... args) : * 
		{
			Logger.writeLog("NetCallCatcher callProperty ", methodName,args);
			_method = methodName.toString();
			_parameter = args;
			_eventDispatcher.dispatchEvent(new Event(Event.CHANGE));
		}


		override flash_proxy function setProperty(name : *, value : *) : void 
		{
			Logger.writeLog("NetCallCatcher callProperty setProperty ", name,value);
			_method = value;
		}

		
		public function get method() : String
		{
			return _method;
		}

		public function get parameter() : Array
		{
			return _parameter	;
		}
	}
}
