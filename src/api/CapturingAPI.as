package  api
{
	import dbdata.CaptureResult;
	import dbdata.EncodingJob;

	import settings.Constants;

	import utils.Logger;

	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.utils.Timer;

	/**
	 * @author gerard
	 */
	public class CapturingAPI extends EventDispatcher 
	{
		public static const START : String = 'start';
		public static const STOP : String = 'stop';
		public static const START_ENCODING : String = 'start_encoding';
		public static const STOP_ENCODING : String = 'stop_encoding';
		public static const CANCEL : String = 'cancel';
		public static const IDLE : String = 'idle';

		private static const TIMEOUT : uint = 5000;
		private var _capUrl : String = Constants.CAPTURE_API;
		private var _apiLoader : URLLoader;
		private var _timeOutTimer : Timer;
		private var _request : URLRequest;
		private var _encodingJob : EncodingJob;
		private var _status : String;
		private var _hasError : Boolean;
		private var _inProgress : Boolean;
		private var _captureResult : CaptureResult;
		private var _idBase : Number;
		private var _photoMode : Boolean;

		public function CapturingAPI()
		{
			_apiLoader = new URLLoader();
			_apiLoader.addEventListener(Event.COMPLETE, onLoadComplete);
			_apiLoader.addEventListener(IOErrorEvent.IO_ERROR, onError);
			_apiLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onError);
			_request = new URLRequest();
			_request.method = URLRequestMethod.POST;
			
			_timeOutTimer = new Timer(TIMEOUT, 1);
			_timeOutTimer.addEventListener(TimerEvent.TIMER, onTimeOut);
		}

		private function onTimeOut(event : TimerEvent) : void 
		{
			Logger.writeLog('CaptureAPI TIMEOUT');
			_inProgress = false;
			
			if(_status == START)
					cancelRecording();
			_hasError = true;	
		}

		private function onError(event : ErrorEvent) : void 
		{
			_timeOutTimer.stop();
			_inProgress = false;
			Logger.writeLog('CaptureAPI onError', event.text);
			_hasError = true;
			dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, event.text));
			//txtResult.text+="Error "+event.text+CR;
		}

		private function onLoadComplete(event : Event) : void 
		{
			Logger.writeLog('CaptureAPI onLoadComplete');
			_timeOutTimer.stop();
			_inProgress = false;
			try
			{
				var jsonObject : Object = JSON.parse(_apiLoader.data);
				_captureResult = new CaptureResult(jsonObject);
				if(_captureResult.Error)
				{
					_hasError = true;
					_inProgress = false;
					if(_status == START)
						cancelRecording();
					Logger.writeLog('CaptureAPI onResult Error');
				}
				else
				{
					_inProgress = true;
				}
			}
			catch(e : Error)
			{
				_inProgress = false;
				Logger.writeLog('CaptureAPI onResult Error json Error', _apiLoader.data, e.message);
			}
			;
			dispatchEvent(new Event(Event.COMPLETE));
		}

		public function startRecording(encodingJob : EncodingJob) : void 
		{
			Logger.writeLog('CaptureAPI startRecording', encodingJob.toString());
			_idBase = new Date().getTime();
			_hasError = false;
			_status = START;
			_inProgress = false;
			_encodingJob = new EncodingJob();
			_encodingJob.ID = _idBase ;
			_encodingJob.FileName = encodingJob.FileName;
			var vars : URLVariables = new URLVariables();
			vars['action'] = START;
			vars['job'] = _encodingJob.ID;
			vars['file'] = _encodingJob.FileName;
			_request.data = vars;
			sendRequest();
		}

		private function sendRequest() : void 
		{
			var req : String = _capUrl + '?action=' + _request.data.action + '&job=' + _request.data.job + '&file=' + _request.data.file;
			Logger.writeLog('CaptureAPI sendRequest;', req);
			_request.url = _capUrl;
			_apiLoader.load(_request);
			_timeOutTimer.reset();
			_timeOutTimer.start();
			_inProgress = true;
		}

		public function cancelRecording() : void 
		{
			Logger.writeLog('CaptureAPI cancelRecording emcoding job?',_encodingJob);
			_timeOutTimer.stop();
			if(!_encodingJob)
				return;
			_hasError = false;
			_status = CANCEL;
			var vars : URLVariables = new URLVariables();
			vars['action'] = CANCEL;
			vars['job'] = _encodingJob.ID;
			vars['file'] = _encodingJob.FileName;
			_request.data = vars;
			sendRequest();
		}

		public function stopRecording() : void 
		{
			Logger.writeLog('CaptureAPI stopRecording', _encodingJob.toString());
			_timeOutTimer.stop();
			if(!_encodingJob)
				return;

			_hasError = false;
			_status = STOP;
			var vars : URLVariables = new URLVariables();
			vars['action'] = STOP;
			vars['job'] = _encodingJob.ID;
			vars['file'] = _encodingJob.FileName;
			_request.data = vars;
			sendRequest();
		}

		public function get status() : String
		{
			return _status;
		}

		public function get hasError() : Boolean
		{
			return _hasError;
		}

		public function get inProgress() : Boolean
		{
			return _inProgress;
		}

		public function get errorLog() : String
		{
			if(_captureResult)
				return _captureResult.ErrorLog;
			return "";
		}

		public function reset() : void
		{
			Logger.writeLog('CaptureAPI reset');
			_captureResult = undefined;
			_hasError = false;
			_inProgress = false;
			_status = IDLE;
		}

		public function get captureResult() : CaptureResult
		{
			return _captureResult;
		}

		public function set capUrl(url : String) : void
		{
			_capUrl = url;
		}

		public function set photoMode(photoMode : Boolean) : void
		{
			_photoMode = photoMode;
		}
	}
}
