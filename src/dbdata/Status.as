package dbdata 
{

	/**
	 * @author gerard
	 */
	public class Status extends DBData 
	{
		public var ID : uint;
		public var Name : String="";
		public var Order : int;
		public function Status(object : Object = null)
		{
			super(object);
		}
	}
}
/*
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Name` char(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Order` int(2) DEFAULT NULL,
   */
