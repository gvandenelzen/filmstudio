package dbdata 
{

	/**
	 * @author gerard
	 */

	
	public class People extends DBData
	{
		public var ID : uint;
		public var LocationID : uint;
		public var InternalID : String;
		public var Titel : String = '';
		public var FirstName : String = '';
		public var LastName : String = '';
		public var Function : String = '';
		public var Extern : uint = 0;
		public var Active : uint = 1;
		public var StatusID : uint;
		public var Email:String="";
		public var Sex:uint=0;

		public function People(object : Object = null)
		{
			super(object);
		}

		public function get FullName() : String
		{
			return LastName + ", " + FirstName;
		}
		public function get Name() : String
		{
			return FirstName + " " + LastName;
		}
	}
}
/*
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LocationID` int(11) NOT NULL,
  `InternalID` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `Titel` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `FirstName` char(50) COLLATE latin1_general_ci DEFAULT NULL,
  `LastName` varchar(80) COLLATE latin1_general_ci NOT NULL,
  `Function` varchar(120) COLLATE latin1_general_ci NOT NULL,
  `Active` int(1) NOT NULL DEFAULT '1',
  `Extern` int(1) NOT NULL DEFAULT '0',
  `StatusID` int(10) NOT NULL,
  */