package dbdata 
{

	/**
	 * @author gerard
	 */

	public class FileLocation extends DBData 
	{
		public var ID : uint;
		public var Name : String;
		public var RelativeURL:String='';
		public var Active:int=1;
		public function FileLocation(object : Object = null)
		{
			super(object);
		}
	}
}
/*
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` char(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RelativeURL` char(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Active` int(1) DEFAULT '1',
  */