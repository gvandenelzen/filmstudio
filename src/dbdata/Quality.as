package dbdata 
{

	/**
	 * @author gerard
	 */

	public class Quality extends DBData 
	{
		public var ID : uint;
		public var Name : String;
		public var StreamQuality : int = 0;
		public var TranscodeQuality : int = 0;
		public var Prefix : String = 'F';

		public function Quality(object : Object = null)
		{
			super(object);
		}
	}
}
/*
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `StreamQuality` int(10) DEFAULT '0',
  `TranscodeQuality` int(10) DEFAULT '0',
  `Prefix` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  */