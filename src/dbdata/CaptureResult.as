package dbdata 
{
	import utils.Logger;

	/**
	 * @author gerard
	 * 
	 * {"JobId":"0","InternalId":25,"Action":"start","ExitCode":null,"Error":false,"ApiError":false,"IsActive":true,"ErrorLog":null}
	 */
	public class CaptureResult extends DBData 
	{
		public var JobId : uint;
		public var InternalId : uint;
		public var Action : String;
		public var ExitCode : uint;
		public var Error : Boolean;
		public var ApiError : Boolean;
		public var IsActive : Boolean;
		public var ErrorLog : String;

		public function CaptureResult(object : Object = null)
		{
			super(object);
			Logger.writeLog("Capture Result ", this.toString());
		}
	}
}
