package dbdata 
{

	/**
	 * @author gerard
	 */
	public class WebSite extends DBData 
	{
		public var ID : uint;
		public var WebTemplate : uint;
		public var Title : String="";
		public var Text : String="";
		public var URL : String="";
		public var Notes : String="";
		public var StatusRating : int=0;
		public var RatingActive : uint=1;
		public var DownloadActive : uint=0;
		public var CreatedTimestamp : Number;
		public var ExpireTimestamp : Number;
		public var RatingTimestamp : Number;
		public var PersonID:uint;

		public function WebSite(object : Object = null)
		{
			super(object);
		}
	}
}
/*

CREATE TABLE IF NOT EXISTS `website` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `WebTemplate` int(10) DEFAULT '0',
  `URL` char(200) COLLATE utf8_unicode_ci DEFAULT '',
  `Title` char(200) COLLATE utf8_unicode_ci DEFAULT '',
  `Text` text COLLATE utf8_unicode_ci,
  `Notes` text COLLATE utf8_unicode_ci,
  `RatingActive` int(1) DEFAULT '0',
  `DownloadActive` int(1) DEFAULT '0',
  `CreatedTimestamp` bigint(15) DEFAULT NULL,
  `ExpireTimestamp` bigint(15) DEFAULT NULL,
  `RatingTimestamp` bigint(15) DEFAULT NULL,
  `StatusRating` int(5) DEFAULT NULL,

  */

  