package dbdata 
{

	/**
	 * @author gerard
	 * CREATE TABLE IF NOT EXISTS `standort` (
	`ID` int(11) NOT NULL AUTO_INCREMENT,
	`BrandID` int(11) NOT NULL,
	`Name` varchar(80) COLLATE latin1_general_ci NOT NULL,
	`Preset` tinyint(4) NOT NULL,
	PRIMARY KEY (`ID`)
	 */
	public class PostRoll extends DBData 
	{
		public var ID : uint;
		public var FileName : String="";
		public var Title : String="";
		public var Active : uint=1;
		public function PostRoll(object : Object = null)
		{
			super(object);
		}
	}
}
/*
  */