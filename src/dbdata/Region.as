package dbdata 
{

	/**
	 * @author gerard
	 */
	public class Region extends DBData 
	{
		public var ID : uint;
		public var InternalID : uint;
		public var Name : String;
		public var ShortName : String;
		public var Active : uint;

		public function Region(object : Object = null)
		{
			super(object);
		}
	}
}


