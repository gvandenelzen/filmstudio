package dbdata 
{

	/**
	 * @author gerard
	 */

	public class Category extends DBData 
	{
		public var ID : uint;
		public var PostRollID : uint=0;
		public var Name : String;
		public var Prefix : String;

		public function Category(object : Object = null)
		{
			super(object);
		}
	}
}
/*
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(30) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `Prefix` char(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  */