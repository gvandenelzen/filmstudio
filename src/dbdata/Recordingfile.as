package dbdata 
{

	/**
	 * @author gerard
	 */
	public class Recordingfile extends DBData 
	{
		public var ID:uint;
		public var RecordingID:Number;
		public var Quality:Number;
		public var FileLocationID:uint;
		public var FileName:String;
		public function Recordingfile(object:Object=null)
		{
			super(object);
		}
	}
}
/*
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `RecordingID` int(10) DEFAULT '0',
  `Quality` int(10) DEFAULT '0',
  `FileLocationID` int(10) DEFAULT '0',
  `FileName` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  */