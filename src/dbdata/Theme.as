package dbdata 
{

	/**
	 * @author gerard
	 */
	public class Theme extends DBData 
	{
		public var ID : uint;
		public var KategorieID : Number;
		public var PostRollID : uint=0;
		public var ThemaCode : String = "";
		public var ThemaText : String = "";
		public var DetailText : String = "";
		public var PrompterText : String = "";
		public var Webtext1 : String = "";
		public var Webtext2 : String = "";
		public var Notes : String = "";
		public var Status : uint = 2;
		public var Active : uint = 1;
		public var ChangeDate : String = "";
		public function Theme(object : Object = null)
		{
			super(object);
		}
	}
}

/*
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `KategorieID` int(11) NOT NULL,
  `ThemaCode` char(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ThemaText` varchar(150) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `DetailText` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `PrompterText` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `Notes` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `Status` int(11) NOT NULL,
 */
