package dbdata 
{
	import dbdata.DBData;
	
	/**
	 * @author gerard
	 */
	public class VideoForWebSite extends DBData 
	{
		public var ID : uint;
		public var WebSiteID : uint;
		public var VideoID : uint;

		public function VideoForWebSite(object : Object = null)
		{
			super(object);
		}
	}
}
/*
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `WebSiteID` int(10) DEFAULT NULL,
  `VideoID` int(10) DEFAULT NULL,

*/