package dbdata 
{

	/**
	 * @author gerard
	 */

	
	public class Interview extends DBData 
	{
		public var ID : uint;
		public var Priority : uint=3;
		public var ThemeID : uint;
		public var PeopleID : uint;
		public var StatusID : uint=2;
		public var SessionID : uint=0;
		public var Prompter : String='';
		public var Notes : String='';
		public var Active:uint=1;

		public function Interview(object : Object = null)
		{
			super(object);
		}
	}
}
/*
 *   `ID` int(10) NOT NULL AUTO_INCREMENT,
  `ThemaID-alt` int(10) DEFAULT '0',
  `ThemeID` int(11) DEFAULT NULL,
  `PeopleID` int(10) DEFAULT '0',
  `StatusID` int(10) DEFAULT '0',
  `SessionID` int(10) DEFAULT '0',
  `Prompter` text COLLATE utf8_unicode_ci,
  `Notes` text COLLATE utf8_unicode_ci,
  `Order` int(11) DEFAULT '0',
   
   */
