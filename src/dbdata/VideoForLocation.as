package dbdata 
{

	/**
	 * @author gerard
	 */
	public class VideoForLocation extends DBData 
	{
		public var ID : uint;
		public var VideoID : uint;
		public var LocationID : uint;

		public function VideoForLocation(object : Object = null)
		{
			super(object);
		}
	}
}
