package dbdata 
{

	/**
	 * @author gerard
	 */

	
	public class Employee extends DBData
	{
		public var ID : uint;
		public var StandortID : uint;
		public var InternalID : uint;
		public var Sex : String;
		public var Title : String;
		public var FullName : String;
		public var Name : String;
		public var Position : String;

		public function Employee(object:Object=null)
		{
			super(object);
		}
	}
}
