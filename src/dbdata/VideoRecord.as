package dbdata 
{

	/**
	 * @author gerard
	 */
	public class VideoRecord extends DBData 
	{
		public var ID : uint;
		public var FileName : String = '';
		public var VideoEditID : int;
		public var PeopleID : int;
		public var ThemeID : int;
		public var PostRollID : uint=0;
		public var VideoTitle : String = '';
		public var VideoSubline : String = '';
		public var Notes : String = '';
		public var Description : String = '';
		public var Length : Number;
		public var RecordingStatus : int;
		public var StatusEncodingJob : int;
		public var Active : int = 0;
		public var InternalPublic : int = 0;
		public var ReferenceVideo : int = 0;
		public var UpdateTimestamp : Number;
		public var UserRating : int;
		public var RatingTimestamp : Number;

		public function VideoRecord(object : Object = null)
		{
			super(object);
		}
	}
}
