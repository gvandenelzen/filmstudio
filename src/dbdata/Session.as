package dbdata 
{

	/**
	 * @author gerard
	 */
	public class Session extends DBData 
	{
		public var ID:uint;
		public var Name:String='';
		public var StartDate:String='';
		public var EndDate:String='';
		public function Session(object : Object = null)
		{
			super(object);
		}
	}
}
/*
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `SessionName` char(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SessionStartDate` date DEFAULT NULL,
  `SessionEndDate` date DEFAULT NULL,
  `Active` int(1) DEFAULT '1',
  */