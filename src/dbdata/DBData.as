package dbdata 
{
	import model.PublicDataHolder;

	/**
	 * @author gerard
	 */
	public class DBData extends PublicDataHolder 
	{
		public function DBData(object : Object = null)
		{
			super();
			if(object)
				parseJSON(object);	
		}

		public function parseJSON(object : Object) : void
		{
			var attr : Object;
			var value : String;
			for (var property:String in object )
			{
				value = object[property];
				if(this.hasProperty(property) && value != "")
				{
					attr = _properties[ property] ;
					if(attr['type'] == "string" && value == "0" )
						value = '';
					if(value != null)
					{
						switch(value)
						{
							
							case 'false':
								this[property] = false;
								break;
							case 'true':
								this[property] = true;
								break;
							default:
								this[property] = decodeValue(value);	
						}
					}
					else
					{
						this[property] = "";
					}
				}
			}
		}

		private function decodeValue(value : String) : String 
		{
			var pattern : RegExp = /\+/g;
			value = value.replace(pattern, " ");
			value = unescape(value);
			return value;
		}

		public function createDBObject() : Object
		{
			return this.createObject();
		}
	}
}
