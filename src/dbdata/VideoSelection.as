package dbdata 
{

	/**
	 * @author gerard
	 */

	public class VideoSelection extends DBData 
	{
		public var ID : uint;
		public var VideoID : uint;
		public var TimeStamp : Number;

		public function VideoSelection(object : Object = null)
		{
			super(object);
		}
	}
}