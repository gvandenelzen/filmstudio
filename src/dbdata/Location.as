package dbdata 
{

	/**
	 * @author gerard
	 * CREATE TABLE IF NOT EXISTS `standort` (
	`ID` int(11) NOT NULL AUTO_INCREMENT,
	`BrandID` int(11) NOT NULL,
	`Name` varchar(80) COLLATE latin1_general_ci NOT NULL,
	`Preset` tinyint(4) NOT NULL,
	PRIMARY KEY (`ID`)
	 */
	public class Location extends DBData 
	{
		public var ID : uint;
		public var RegionID : uint;
		public var BrandID : uint;
		public var InternalID : uint;
		public var Name : String;
		public var ShortName : String;
		public var Subtitle : String;
		public var Preset : uint;
		public var Active : uint;
		public function Location(object : Object = null)
		{
			super(object);
		}
	}
}
/*
CREATE TABLE IF NOT EXISTS `location` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RegionID` int(11) NOT NULL DEFAULT '0',
  `BrandID` int(11) NOT NULL,
  `InternalID` int(11) NOT NULL,
  `Name` varchar(80) COLLATE latin1_general_ci NOT NULL,
  `Subtitle` varchar(80) COLLATE latin1_general_ci NOT NULL,
  `Preset` tinyint(4) NOT NULL,
  `Active` int(1) NOT NULL,
  */