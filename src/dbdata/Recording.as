package dbdata 
{

	/**
	 * @author gerard
	 */
	public class Recording extends DBData 
	{
		public var ID:uint;
		public var TimestampStart:Number;
		public var Duration:Number=0;
		public var InterviewID:uint;
		public var FileName:String="";
		public var PeopleID:uint;
		public var StatusID:uint=0;
		public var RecordMode:String='';
		public var Notes:String='';
		public function Recording(object:Object=null)
		{
			super(object);
		}
	}
}
/*
 `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TimestampStart` bigint(20) NOT NULL,
  `Duration` bigint(20) NOT NULL,
  `InterviewID` int(11) NOT NULL,
  `FileName` varchar(80) COLLATE latin1_general_ci NOT NULL,
  `PeopleID` int(11) NOT NULL,
  `StatusID` int(11) NOT NULL,
  `RecordMode` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `Notes` text COLLATE latin1_general_ci NOT NULL,
  */