package dbdata 
{

	/**
	 * @author gerard
	 */
	public class VideoEdit extends DBData 
	{
		public var ID : uint;
		public var RecordingID : int;
		public var RecordingFileID : int;
		public var TrimIn : Number;
		public var TrimOut : Number;
		public var XOffset:int=0;
		public var Subtitle_1 : String = '';
		public var Subtitle_2 : String = '';
		public var AudioFadeIn : Number;
		public var AudioFadeOut : Number;
		public var LogoID : int;
		public var LogoActive : int = 1;
		public var SubtitleActive : int;

		public function VideoEdit(object : Object = null)
		{
			super(object);
		}
	}
}
/*


  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `RecordingID` int(10) DEFAULT '0',
  `RecordingFileID` int(10) DEFAULT '0',
  `StatusID` int(11) DEFAULT '0',
  `TrimIn` int(11) DEFAULT '0',
  `TrimOut` int(11) DEFAULT '0',
  `SubtitleActive` int(1) DEFAULT '0',
  `Subtitle_1` char(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Subtitle_2` char(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LogoActive` int(1) DEFAULT '0',
  `LogoID` int(11) DEFAULT '0',
  `AudioFadeIn` int(11) DEFAULT '0',
  `AudioFadeOut` int(11) DEFAULT '0',

*/