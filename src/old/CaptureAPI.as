package  old{
	import fl.controls.Button;
	import fl.controls.ComboBox;
	import fl.controls.TextArea;
	import fl.controls.TextInput;

	import settings.Constants;

	import flash.display.MovieClip;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.SecurityErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.utils.Timer;

	/**
	 * @author gerard
	 */
	public class CaptureAPI extends MovieClip 
	{
		private static const CR : String = "\n";
		private static const TIMEOUT : uint = 5000;
		public var btnSend : Button;
		public var txtResult : TextArea;
		public var txtURL : TextInput;
		public var cbAction : ComboBox;
		public var txtJob : TextInput;
		public var txtFile : TextInput;
		public var txtDevice : TextInput;
		private var _apiLoader : URLLoader;
		private var _timeOutTimer:Timer;

		public function CaptureAPI()
		{
			addEventListener(Event.ADDED_TO_STAGE, initUI);
		}

		private function initUI(event : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initUI);
			_apiLoader = new URLLoader();
			_apiLoader.addEventListener(Event.COMPLETE, onLoadComplete);
			_apiLoader.addEventListener(IOErrorEvent.IO_ERROR, onError);
			_apiLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onError);
			txtURL.text=Constants.CAPTURE_API;
			btnSend.addEventListener(MouseEvent.CLICK, onSend);
			cbAction.selectedIndex=0;
			
			_timeOutTimer=new Timer(TIMEOUT,1);
			_timeOutTimer.addEventListener(TimerEvent.TIMER, onTimeOut);
			stage.nativeWindow.alwaysInFront=true;
		}

		private function onTimeOut(event : TimerEvent) : void 
		{
			txtResult.text+="Error Timeout"+CR;
		}

		private function onError(event : ErrorEvent) : void 
		{
			_timeOutTimer.stop();
			txtResult.text+="Error "+event.text+CR;
		}

		private function onLoadComplete(event : Event) : void 
		{
			_timeOutTimer.stop();
			txtResult.text+="Result "+_apiLoader.data+CR;
		}

		private function onSend(event : MouseEvent) : void 
		{
			var vars:URLVariables=new URLVariables();
			vars['action']=cbAction.selectedItem.data;
			vars['job']=txtJob.text;
			vars['file']=txtFile.text;
			vars['device']=txtFile.text;
			var request:URLRequest=new URLRequest();
			request.method=URLRequestMethod.POST;
			request.data=vars;
			request.url=txtURL.text;
			_apiLoader.load(request);
			txtResult.text+="Send "+vars+CR;
			_timeOutTimer.start();
		}

		public function startRecording(recordingBasename : String) : void 
		{
		}
	}
}
