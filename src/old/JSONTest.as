package  old
{
	import dbdata.Theme;

	import fl.controls.Button;
	import fl.controls.TextArea;
	import fl.controls.TextInput;

	import model.ErrorCodes;

	import utils.HTMLUtils;
	import utils.XMLUtils;

	import flash.display.Sprite;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.text.TextField;

	/**
	 * @author gerard
	 */
	public class JSONTest extends Sprite 
	{
		public var txtURL : TextInput;
		public var txtLog : TextArea;
		public var txtPrompt:TextField;
		public var txtJSON:TextField;
		public var btnLoad : Button;
		public var btnConvert : Button;
		private var _xmlLoader : URLLoader;
		private var _xml : XML;
		private var _data : *;
		private var _request : *;

		public function JSONTest() : void
		{
			btnLoad.addEventListener(MouseEvent.CLICK, onLoad);
			btnConvert.addEventListener(MouseEvent.CLICK, onConvert);
			_xmlLoader = new URLLoader();
			_xmlLoader.addEventListener(Event.COMPLETE, onLoadComplete);
			_xmlLoader.addEventListener(IOErrorEvent.IO_ERROR, onError);
			_xmlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onError);
		}

		private function onConvert(event : MouseEvent) : void 
		{
			if(_data)
				parseResult(_data);
		}

		private function onLoad(event : MouseEvent) : void 
		{
			_xmlLoader.load(new URLRequest(txtURL.text));
		}

		private function onError(e : Event = null) : void
		{
			txtLog.text += 'ERRR LOADING';
		}

		private function onLoadComplete(e : Event) : void
		{

			if (!_xmlLoader.data)
			{
				txtLog.text += ErrorCodes.BAD_XML;
			}
			else
			{
				parseResponse(_xmlLoader.data);
			}
		}

		private function parseResponse(data : String) : void
		{

			try
			{
				data = cleanData(data);
				_xml = new XML(data);
			}
			catch (e : Error)
			{
				txtLog.text += 'badXML';
				return;
			}

			if (!_xml)
			{
				;
				txtLog.text += 'badXML';
				return;
			}
			if (_xml.hasSimpleContent())
			{
				_xml = new XML("<result>" + data + "</result>");
				_xml = _xml.children()[0];
			}
			if(_xml['status'])
			{
				if(_xml['status'] == 'ok')
				{
					_request = _xml['request'];
					_data = _xml['data'];
					txtJSON.text = _data;
				}
				else
				{
					_request = _xml['request'];
					txtLog.text += 'badXML';
				}
				return;
			}

		}

		private function cleanData(data : String) : String
		{
			return  XMLUtils.clean(data);
		}

		private function parseResult(rawdata : String) : void
		{
			
			var data : String = HTMLUtils.convertHTMLEntities(rawdata);
			try
			{
				var jsonObject : Object = JSON.parse(data);
				if( !jsonObject)
				{
					txtLog.text += 'NO JSON';
					return;	
				}
			}
			catch(e : Error)
			{
				txtLog.text  = 'JSON ERROR';	
				return;
			}
			var theme:Theme=new Theme(jsonObject);
			if(theme.PrompterText)
				txtPrompt.text = theme.PrompterText;
		}
	}
}
