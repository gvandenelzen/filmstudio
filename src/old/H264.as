﻿package old 
{
	import controls.Communicator;

	import fl.controls.Button;
	import fl.controls.CheckBox;
	import fl.controls.ComboBox;
	import fl.controls.TextArea;
	import fl.controls.TextInput;
	import fl.data.DataProvider;

	import fms.FMSConnection;

	import model.NetCall;
	import model.Role;

	import settings.ConnectionSettings;

	import utils.DateTimeUtils;
	import utils.Logger;

	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.NetStatusEvent;
	import flash.media.Camera;
	import flash.media.H264Level;
	import flash.media.H264Profile;
	import flash.media.H264VideoStreamSettings;
	import flash.media.Video;
	import flash.media.VideoStreamSettings;
	import flash.net.NetStream;
	import flash.net.Responder;
	import flash.text.TextField;

	public class H264 extends MovieClip 
	{

		private static const RTMP : String = "rtmp://82.165.41.34";
		private static const APP : String = "streamingstudio";
		private static const INST : String = "recording";
		private static const STREAM : String = "test";
		private static const H264 : String = 'H264';
		private static const FLV : String = 'H263';
		private static const F4V_SUFFIX:String='.f4v';
		private static const MP4_PREFIX:String='mp4:';
		private static const STUDIO : String = 'studio';
		public var mcVideo : Video;
		public var mcControl: Video;
		public var btnConnect : Button;
		public var txtOut : TextField;
		public var txtIn : TextField;
		public var txtRTMP : TextInput;
		public var txtApplication : TextInput;
		public var txtInstance : TextInput;

		public var txtStream : TextInput;
		public var btnPublish : Button;
		public var btnPlay : Button;
		public var btnClear : Button;
		public var cbH264 : CheckBox;
		public var txtLog : TextArea;

		public var cbbServerCommand : ComboBox;
		public var cbbCommCommand : ComboBox;
		public var txtParamServer : TextInput;
		public var txtParamComm : TextInput;
		public var btnCallServer : Button;
		public var btnCallComm : Button;

		private var _publishStream : NetStream;
		private var _playStream : NetStream;
		private var _fmsConnection : FMSConnection;
		private var _isH264 : Boolean;
		private var _camera : Camera;
		private var _responder : Responder;
		private var _communicator : Communicator;
		private var _isPublishing : Boolean;

		public function H264() 
		{
			if(!stage)
				addEventListener(Event.ADDED_TO_STAGE, initUI);
			else
				initUI(null);
		}

		private function initUI(event : Event) : void 
		{
			Logger.init(stage);
			Logger.directTo(txtLog.textField);
			if(hasEventListener(Event.ADDED_TO_STAGE))
				removeEventListener(Event.ADDED_TO_STAGE, initUI);
			mcVideo.smoothing = true;
			txtRTMP.text = RTMP;
			txtApplication.text = APP;
			txtInstance.text = INST;
			btnConnect.label = defineConnectLabel();
			btnConnect.addEventListener(MouseEvent.CLICK, onConnectDisconnect);
			
			btnPublish.addEventListener(MouseEvent.CLICK, onPublish);
			
			btnPlay.addEventListener(MouseEvent.CLICK, onPlay);
			
			btnCallServer.addEventListener(MouseEvent.CLICK, onCallServer);
			
			btnCallComm.addEventListener(MouseEvent.CLICK, onCallComm);
			
			btnClear.addEventListener(MouseEvent.CLICK, onClear);
			
			cbbServerCommand.dataProvider = new DataProvider([NetCall.START_RECORDING,NetCall.STOP_RECORDING]);
			cbbCommCommand.dataProvider = new DataProvider([NetCall.CHANGE_PUBLISH_CODEC]);
			
			
			enableButtons(false);
		}

		private function onClear(event : MouseEvent) : void 
		{
			txtLog.text="";
		}

		private function onCallComm(event : MouseEvent) : void 
		{
			if(cbbCommCommand.selectedLabel)
			{
				writeLog(this,"onCallComm ",cbbCommCommand.selectedLabel, txtParamComm.text)
				_communicator.call(cbbCommCommand.selectedLabel, txtParamComm.text);
			}
		}

		private function onCallServer(event : MouseEvent) : void 
		{
			if(cbbServerCommand.selectedLabel)
			{
				var param:String=txtParamServer.text;
				if(cbbServerCommand.selectedLabel==NetCall.START_RECORDING && _isH264)
				{
					if(param.indexOf(F4V_SUFFIX)==-1)
						param=param+F4V_SUFFIX;
				}
				_fmsConnection.call(cbbServerCommand.selectedLabel, param);
			}
		}

		private function onPlay(event : MouseEvent) : void 
		{
			if(_publishStream)
			{
				_publishStream.removeEventListener(NetStatusEvent.NET_STATUS, onNetStatus);
				_publishStream.dispose();
				_publishStream = null;
			}
			mcVideo.clear();
			mcVideo.attachNetStream(null);
			if(!_playStream)
			{
				_playStream = new NetStream(_fmsConnection.netConnection)	;
				_playStream.bufferTime = 0.1;
				_playStream.client = this;
				_playStream.addEventListener(NetStatusEvent.NET_STATUS, onNetStatus);
			}
			mcVideo.attachNetStream(_playStream);
			var param:String=txtStream.text;
			if(param.indexOf(F4V_SUFFIX)>0 && param.indexOf(MP4_PREFIX)!= 0 )
				param=MP4_PREFIX+param;
			_playStream.play(param, 0);
			_isPublishing = false;
		}

		private function onPublish(event : MouseEvent) : void 
		{
			setPublishPlaySettings();
			_isPublishing = true;
		}

		private function getPublishStreamSettings() : VideoStreamSettings
		{
			_camera.setMode(320, 240, 21);
			_camera.setQuality(0, 90);
			if(cbH264.selected)
			{
				var h264Settings : H264VideoStreamSettings = new H264VideoStreamSettings();
				h264Settings.setProfileLevel(H264Profile.BASELINE, H264Level.LEVEL_2);
				h264Settings.setMode(mcVideo.width, mcVideo.height, 21);
				h264Settings.setQuality(0, 90);
				h264Settings.setKeyFrameInterval(21);
				_isH264 = true;
				return  h264Settings;
			}
			var streamSettings : VideoStreamSettings = new VideoStreamSettings();
			streamSettings.setMode(mcVideo.width, mcVideo.height, 21);
			streamSettings.setQuality(0, 90);
			streamSettings.setKeyFrameInterval(21);
			_isH264 = false;
			return  streamSettings;
		}

		
		private function setPublishPlaySettings() : void 
		{
			_camera = Camera.getCamera();
			if(!_camera)
				return;
			mcVideo.clear();
			mcVideo.attachNetStream(null);
			if(!_publishStream)
			{
				_publishStream = new NetStream(_fmsConnection.netConnection)	;
				//_publishStream.client = this;
				_publishStream.bufferTime = 0;
				_publishStream.addEventListener(NetStatusEvent.NET_STATUS, onNetStatus);
			}
			if(_publishStream)
			{
				_publishStream.attachCamera(null);
				_publishStream.videoStreamSettings = getPublishStreamSettings();
				_publishStream.attachCamera(_camera);
				sendMetadata();
				var stream:String=STUDIO;
				if(_isH264)
					stream=MP4_PREFIX + STUDIO ;
				
				_publishStream.publish(stream);
				txtStream.text=stream;
			}
			if(!_playStream)
			{
				_playStream = new NetStream(_fmsConnection.netConnection)	;
				_playStream.client = this;
				_playStream.addEventListener(NetStatusEvent.NET_STATUS, onNetStatus);
			}
			if(_playStream)
			{
				_playStream.bufferTime = 0;
				if(_isH264)
					_playStream.play(stream);
				else
					_playStream.play(stream);
					
				mcVideo.attachNetStream(_playStream);
			}
		}

		public function onMetaData(metaData : Object) : void
		{
			writeLog(this, 'onMetaData');
			for (var prop:String in metaData)
			{
				writeLog(this, prop,metaData[prop]);
			}
		}

		public function onCodecChange(codec : String) : void
		{
			writeLog(this, 'onCodecChange', codec);
		}

		public function onPlayStatus(metaData : Object) : void
		{
			writeLog(this, 'onPlayStatus');
		}

		public function sendMetadata() : void 
		{
			if(!_publishStream)
				return;
			var metaData : Object = new Object();
			metaData.codec = _publishStream.videoStreamSettings.codec;
			metaData.isH264 = _isH264;
			metaData.hasMetadata = true;
			_publishStream.send("@setDataFrame", "onMetaData", metaData);
			_publishStream.send("onCodecChange", metaData.codec);
			writeLog(this, 'sendMetaData', metaData.codec, metaData.isH264);
		}  

		private function onConnectDisconnect(event : MouseEvent) : void 
		{
			if(_fmsConnection && _fmsConnection.netConnection.connected)
			{
				writeLog("retry Connection")
				disposeStreams();
				_fmsConnection.closeConnection();
				return;
			}
			if(_fmsConnection && !_fmsConnection.netConnection.connected)
			{
				_fmsConnection.reconnect();
				return;
			}
			if(!_fmsConnection)
			{
				writeLog("new Connection")
				_fmsConnection = new FMSConnection();
				_responder = new Responder(onServerResponse, onServerRequestError);
				_fmsConnection.responder = _responder;
				_fmsConnection.subscribe(onFMSConnection);
				var connectionSettings : ConnectionSettings = new ConnectionSettings();
				connectionSettings.rtmp = txtRTMP.text;
				connectionSettings.application = txtApplication.text;
				connectionSettings.instance = txtInstance.text;
				connectionSettings.autoconnect = true;
				connectionSettings.role = Role.CONTROL;
				_fmsConnection.init(connectionSettings);
			}
		}

		private function onServerResponse(result : Object) : void 
		{
			var request : String = result['request'];
			var parameter : String = result['data'];
			writeLog(" FROM FMS", request, parameter);
		}

		private function onServerRequestError(info : Object) : void 
		{
			writeLog(" onServerRequestError", info['description']);
		}

		private function writeLog(...string ) : void 
		{
			var msg : String = string.join(',');
			txtLog.text += DateTimeUtils.formattedDate() + ":" + msg + "\n";
		}

		private function onFMSConnection(event : Event) : void 
		{
			switch(event.type)
			{
				case FMSConnection.CONNECTED:
					_isPublishing = false;
					writeLog("onFMSConnection connected START", event.type);
					btnConnect.label = defineConnectLabel();
					enableButtons(true);
					initCommunicator();
					initStreams();
					addEventListener(Event.ENTER_FRAME, onEnterFrame);
					break;
				case FMSConnection.FAILED:
					writeLog(this, "onNetStatus Failed", event.type);
					btnConnect.label = defineConnectLabel();
					_isPublishing = false;
					break;
				case FMSConnection.CLOSED:
					writeLog("onFMSConnection closed", event.type);
					btnConnect.label = defineConnectLabel();
					enableButtons(false);
					removeEventListener(Event.ENTER_FRAME, onEnterFrame);
					disposeStreams();
					_isPublishing = false;
					break;
			}
		}

		private function initCommunicator() : void 
		{
			Logger.writeLog("initCommunicator");
			_communicator = new Communicator(_fmsConnection.netConnection, Role.CONTROL);
			_communicator.addEventListener(Event.CHANGE, onCommunicator);
		}

		private function onCommunicator(event : Event) : void 
		{
			//Commands From Control to udio and from Studio to COntrol
			var method : String = _communicator.method;
			var parameter : * = _communicator.parameter;
			
			Logger.writeLog("onCommunicator", method, parameter);
			
			switch(method)
			{
	
				case NetCall.CHANGE_PUBLISH_CODEC:
					changePublishCodec(parameter);
					break;
			}
		}

		private function changePublishCodec(codec : String) : void 
		{
			var isH264 : Boolean = (codec == H264) ? true : false;
			if(isH264 == cbH264.selected)
				return;
				
			cbH264.selected = isH264;
		}

		private function enableButtons(enable : Boolean) : void
		{
			btnCallServer.enabled = enable;
			btnCallComm.enabled = enable;
			btnPublish.enabled = enable;
			btnPlay.enabled = enable;
		}

		private function onNetStatus(event : NetStatusEvent) : void 
		{
			var netStream : NetStream = event.currentTarget as NetStream;
			if(netStream)
				writeLog(this, "onNetStream", netStream, event.info['code']);
		}

		private function onEnterFrame(event : Event) : void 
		{
			var totOut : Number = 0;
			var totIn : Number = 0;
			if(_publishStream)
				totOut += _publishStream.info.currentBytesPerSecond;
			if(_playStream)
				totIn += _playStream.info.currentBytesPerSecond;
			txtOut.text = (totOut / 1024).toPrecision(3) + " kB/s";
			txtIn.text = (totIn / 1024).toPrecision(3) + " kB/s";
		}

		private function disposeStreams() : void 
		{
			if(_publishStream)
			{
				_publishStream.dispose();
				_publishStream = null;
			}
			if(_playStream)
			{
				_playStream.dispose();
				_playStream = null;
			}
			mcVideo.clear();
			mcVideo.attachNetStream(null);
		}

		private function initStreams() : void 
		{
			if(!_fmsConnection || !_fmsConnection.netConnection.connected)
				return;
			_publishStream = new NetStream(_fmsConnection.netConnection)	;
			//_publishStream.client = this;
			_publishStream.bufferTime = 0;
			_publishStream.addEventListener(NetStatusEvent.NET_STATUS, onNetStatus);
			
			_playStream = new NetStream(_fmsConnection.netConnection)	;
			_playStream.bufferTime = 0;
			_playStream.client = this;
			_playStream.addEventListener(NetStatusEvent.NET_STATUS, onNetStatus);
		}

		private function defineConnectLabel() : String 
		{
			if(_fmsConnection && _fmsConnection.netConnection.connected)
				return "disconnect";
			
			return 'connect';
		}
	}
}
