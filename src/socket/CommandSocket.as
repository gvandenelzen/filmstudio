package socket 
{
	import flash.errors.*;
	import flash.events.*;
	import flash.net.Socket;

	/**
	 * @author gerard
	 */
	public class CommandSocket extends Socket 
	{
		private static const TIME_OUT :uint= 5;
		private var response : String;
		private var _host : String;
		private var _port : int;

		public function CommandSocket(host : String, port : int )
		{
			super();
			_host=host;
			_port=port;
			timeout=TIME_OUT;
			configureListeners();
		}
		public function connectSocket():void
		{
			super.connect(_host, _port);		
			}
		
		private function configureListeners() : void 
		{
			addEventListener(Event.CLOSE, closeHandler);
			addEventListener(Event.CONNECT, connectHandler);
			addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			addEventListener(ProgressEvent.SOCKET_DATA, socketDataHandler);
		}

		private function writeln(str : String) : void 
		{
			str += "\n";
			try 
			{
				writeUTFBytes(str);
			}
        catch(e : IOError) 
			{
				trace(e);
			}
		}

		private function sendRequest() : void 
		{
			trace("sendRequest");
			response = "";
			writeln("GET /");
			flush();
		}

		private function readResponse() : void 
		{
			var str : String = readUTFBytes(bytesAvailable);
			response += str;
		}

		private function closeHandler(event : Event) : void 
		{
			trace("closeHandler: " + event);
			trace(response.toString());
		}

		private function connectHandler(event : Event) : void 
		{
			trace("connectHandler: " + event);
			sendRequest();
		}

		private function ioErrorHandler(event : IOErrorEvent) : void 
		{
			trace("ioErrorHandler: " + event);
		}

		private function securityErrorHandler(event : SecurityErrorEvent) : void 
		{
			trace("securityErrorHandler: " + event);
		}

		private function socketDataHandler(event : ProgressEvent) : void 
		{
			trace("socketDataHandler: " + event);
			readResponse();
		}
	}
}

