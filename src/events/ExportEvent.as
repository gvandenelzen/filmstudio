package events 
{
	import flash.events.Event;

	/**
	 * @author gerard
	 * for communicating throufg the stage
	 */
	public class ExportEvent extends Event 
	{
		public static const TO_FILE : String = "exportTofile";
		public static const TO_DESKTOP : String = "todesktop";
		private var _data : String;
		private var _name:String;

		public function ExportEvent(type : String,name:String,data : String, bubbles : Boolean = false, cancelable : Boolean = false)
		{
			super(type, bubbles, cancelable);
			_name=name;
			_data = data;
		}

		public override function clone() : Event 
		{
			return new ExportEvent(type,name, data,false, false);
		}

		public function get data() : String
		{
			return _data;
		}
		
		public function get name() : String
		{
			return _name;
		}
		
	}
}
