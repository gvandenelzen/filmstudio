package events
{

	public dynamic class EventList extends Array
	{
		public function EventList()
		{
			super( );
		}

		public function add(element : *) : *
		{
			super.push( element );
			return element;
		}

		public function getCopy() : EventList
		{
			var eList : EventList = new EventList( );
			for each (var element:* in this)
			{
				eList.add( element );
			}
			return eList;
		}
	}
}
