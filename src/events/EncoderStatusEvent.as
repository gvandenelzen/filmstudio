package events 
{
	import model.EncoderStatus;

	import flash.events.Event;

	/**
	 * @author gerard
	 */
	public class EncoderStatusEvent extends Event 
	{
		public static const STATUS : String = 'EncoderStatus';
		private var _encoderStatus : EncoderStatus;

		public function EncoderStatusEvent(type : String,encoderStatus : EncoderStatus, bubbles : Boolean = false, cancelable : Boolean = false)
		{
			_encoderStatus=encoderStatus;
			super(type, bubbles, cancelable);
		}

		public override function clone() : Event 
		{
			return new EncoderStatusEvent(type, encoderStatus, false, false);
		}

		public function get encoderStatus() : EncoderStatus
		{
			return _encoderStatus;
		}

		public function set encoderStatus(encoderStatus : EncoderStatus) : void
		{
			_encoderStatus = encoderStatus;
		}
	}
}
