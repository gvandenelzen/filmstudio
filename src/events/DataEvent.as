package events 
{
	import flash.events.Event;

	/**
	 * @author gerard
	 * for communicating throufg the stage
	 */
	public class DataEvent extends Event 
	{
		public static const BUSY : String = "changingdata";
		public static const FREE : String = "notchangingdata";
		public static const UPDATE : String = "changedataMGRData";
		public static const DELETE : String = "deletedataMGRData";
		public static const DATA : String = "getedataMGRData";
		private var _data : Array;
		private var _kind:String;
		private var _requestor : String;

		public function DataEvent(type : String,kind:String,data : Array=null,requestor:String=null, bubbles : Boolean = false, cancelable : Boolean = false)
		{
			super(type, bubbles, cancelable);
			_kind=kind;
			_data = data;
			_requestor=requestor;
		}

		public override function clone() : Event 
		{
			return new DataEvent(type,kind, data, requestor,false, false);
		}

		public function get data() : Array
		{
			return _data;
		}
		
		public function get kind() : String
		{
			return _kind;
		}
		
		public function get requestor() : String
		{
			return _requestor;
		}
	}
}
