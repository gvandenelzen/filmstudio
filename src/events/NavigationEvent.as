package events 
{
	import flash.events.Event;

	/**
	 * @author gerard
	 * for communicating throufg the stage
	 */
	public class NavigationEvent extends Event 
	{
		public static const WINDOW : String = "navShowWindow";
		public static const EXIT : String = "navExit";
		public static const HOME : String = "navHOME";
		private var _window : String;
		private var _param : Array;

		public function NavigationEvent(type : String,window : String=null,param:Array=null, bubbles : Boolean = false, cancelable : Boolean = false)
		{
			super(type, bubbles, cancelable);
			_window = window;
			_param=param;
		}

		public override function clone() : Event 
		{
			return new NavigationEvent(type, window,param, false, false);
		}

		public function get window() : *
		{
			return _window;
		}

		public function get param() : Array
		{
			return _param;
		}
	}
}
