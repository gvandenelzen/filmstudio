package events
{
	import flash.events.IEventDispatcher;
	import flash.utils.Dictionary;

	/**
	 * 	add eventlistner for a EventList which ca contain 1-n Events
	 * 	saves a lot of writing when you want to subribe to a long list of Events
	 * 	the only drawback ist that you can declsre only one evnthandler per list
	 * 
	 * 
	 */
	public class EventSubscribtor
	{
		private static var _subscribers : Dictionary = new Dictionary( true );
		public function EventSubscribtor()
		{
		}
		public static function subscribe(subscriber : IEventDispatcher,dispatcher : IEventDispatcher,events : EventList,callback : Function) : void
		{
			
			if(!_subscribers[subscriber])_subscribers[subscriber] = new Dictionary( true );
			if(_subscribers[subscriber][dispatcher])return;
			_subscribers[subscriber][dispatcher] = {events:events, callback:callback};
			for each (var event:String in events)
			{
				dispatcher.addEventListener( event, callback );
			}
		}
		public static function unsubscribe(subscriber : IEventDispatcher) : void
		{
			if(!_subscribers[subscriber])
				return;
			for  (var disp:Object in _subscribers[subscriber])
			{
				if( disp is IEventDispatcher)
					var dispatcher : IEventDispatcher = IEventDispatcher( disp );
				else 
					continue;
				var evtObj : Object = _subscribers[subscriber][dispatcher];
				var events : EventList = evtObj.events;
				var callback : Function = evtObj.callback;
				for each (var event:String in events)
				{
					dispatcher.removeEventListener( event, callback );
				}
			}
			delete _subscribers[subscriber];
		}
	}
}