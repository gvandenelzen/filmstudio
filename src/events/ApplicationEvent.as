package events 
{
	import flash.events.Event;

	/**
	 * @author gerard
	 * for communicating throufg the stage
	 */
	public class ApplicationEvent extends Event 
	{
		public static const INITIALIZED:String="classInitiailized";
		private var _sender:*;
		public function ApplicationEvent(type : String,sender:*, bubbles : Boolean = false, cancelable : Boolean = false)
		{
			super(type, bubbles, cancelable);
			_sender=sender;
		}
		public override function clone() : Event 
		{
			return new ApplicationEvent( type,sender,false,false);
		}
		
		public function get sender() : *
		{
			return _sender;
		}
		
		public function set sender(sender : *) : void
		{
			_sender = sender;
		}
	}
}
