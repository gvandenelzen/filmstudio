package controls 
{
	import flash.display.MovieClip;
	import flash.geom.ColorTransform;

	/**
	 * @author gerard
	 */
	public class ColorButton extends TextButton 
	{
		public var regionID:uint;
		public var locationID:uint;
		public var mcBackground : MovieClip;

		private var _selected : Boolean;
		private var _color : uint;

		public function ColorButton(caption : String = '')
		{
			super(caption);
		}

		public function set color(color : uint) : void
		{
			_color=color;
			if(!mcBackground)
				return;
			var cT : ColorTransform = new ColorTransform();
			cT.color = color;
			mcBackground.transform.colorTransform = cT;
		}

		public function set selected(selected : Boolean) : void
		{
			if(!enabled)
				return;
			_selected = selected;
		}

		public function get selected() : Boolean
		{
			if(enabled)
				return _selected;
			return false;
		}
	}
}
