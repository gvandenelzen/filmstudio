package controls 
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	/**
	 * @author gerard
	 */
	public class VolumeControl extends MovieClip 
	{
		public var mcVolumebar : MovieClip;
		private var _mask : Sprite;

		public function VolumeControl()
		{
			setHitArea();
			setMask();
			addEventListener(MouseEvent.MOUSE_DOWN, onMouse);
		}

		private function setHitArea() : void 
		{
			var background:Sprite = new Sprite();
			with(background.graphics)
			{
				beginFill(0, 0);
				drawRect(0, 0, mcVolumebar.width, mcVolumebar.height);
			}
			addChildAt(background,0);
		}

		private function setMask() : void 
		{
			_mask = new Sprite();
			with(_mask.graphics)
			{
				beginFill(0, 0);
				drawRect(0, 0, mcVolumebar.width, mcVolumebar.height);
			}
			addChild(_mask);
			mcVolumebar.mask=_mask;
		}

		private function onMouse(event : MouseEvent) : void 
		{
			switch(event.type)
			{
				case MouseEvent.MOUSE_DOWN:
					stage.addEventListener(MouseEvent.MOUSE_UP, onMouse);
					stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouse);
					adjustHitarea();
					break;
				case MouseEvent.MOUSE_MOVE:
					adjustHitarea();
					break;
				case MouseEvent.MOUSE_UP:
					stage.removeEventListener(MouseEvent.MOUSE_UP, onMouse);
					stage.removeEventListener(MouseEvent.MOUSE_MOVE, onMouse);
					adjustHitarea();
					break;
			}
		}

		private function adjustHitarea() : void 
		{
			_mask.width=mouseX;
			dispatchEvent(new Event(Event.CHANGE));
		}

		public function get value() : Number
		{
			return _mask.width / mcVolumebar.width;
		}
		public function set value(value:Number) : void
		{
			if(value>1)
				value=1;
			 _mask.width = mcVolumebar.width*value;
		}
	}
}
