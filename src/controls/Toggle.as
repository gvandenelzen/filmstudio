package controls 
{
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.events.MouseEvent;

	/**
	 * @author gerard
	 */
	public class Toggle extends MovieClip 
	{
		public var btnOn : SimpleButton;
		public var btnOff : SimpleButton;

		public function Toggle()
		{
			addEventListener(MouseEvent.CLICK, onClick);
			selected = true;
		}

		private function onClick(event : MouseEvent) : void 
		{
			selected = !selected;
		}

		public function get selected() : Boolean
		{
			return btnOn.visible;
		}

		public function set selected(selected : Boolean) : void
		{
			btnOn.visible = selected;
			btnOff.visible = !btnOn.visible;
		}
	}
}
