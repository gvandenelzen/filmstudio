package controls 
{
	import utils.Logger;

	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.media.SoundMixer;
	import flash.media.SoundTransform;

	/**
	 * @author gerard
	 */
	public class MasterSoundControl extends MovieClip 
	{
		public var mcSoundControl:SoundControl;
		public function MasterSoundControl()
		{
			Logger.writeLog("MasterSoundControl init");
			mcSoundControl.addEventListener(Event.CHANGE, onSoundChanged);
		}

		private function onSoundChanged(event : Event) : void 
		{
			SoundMixer.soundTransform=new SoundTransform(mcSoundControl.value);
		}

	}
}
