package  controls
{
	import model.Role;

	import utils.Logger;

	import view.RemoteControlView;

	import flash.events.AsyncErrorEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.NetStatusEvent;
	import flash.net.NetConnection;
	import flash.net.NetStream;

	/**
	 * @author gerard
	 */
	public class RemoteControl extends EventDispatcher 
	{
		private static const LOG_STREAM_STUDIO : String = 'logstreamstudio';
		private static const LOG_STREAM_CONTROL : String = 'logstreamcontrol';
		private static const CONTROL_STREAM : String = 'controlstream';
		private static const START_LOG : String = "START_LOG";
		private static const STOP_LOG : String = "STOP_LOG";
		private static const RESTART : String = "RESTART";
		private var _logStreamStudio : NetStream;
		private var _logStreamControl : NetStream;
		private var _controlStream : NetStream;
		private var _remoteControlView : RemoteControlView;
		private var _role : String; 

		public function RemoteControl(netConnection : NetConnection,role : String)
		{
			Logger.writeLog('RemoteLogger init', role);
			_role = role;
			
			_logStreamStudio = new NetStream(netConnection);
			_logStreamStudio.addEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
			_logStreamStudio.addEventListener(AsyncErrorEvent.ASYNC_ERROR, asyncErrorHandler);
			_logStreamStudio.client = this;
			
			_logStreamControl = new NetStream(netConnection);
			_logStreamControl.addEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
			_logStreamControl.addEventListener(AsyncErrorEvent.ASYNC_ERROR, asyncErrorHandler);
			_logStreamControl.client = this;
			
			_controlStream = new NetStream(netConnection);
			_controlStream.addEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
			_controlStream.addEventListener(AsyncErrorEvent.ASYNC_ERROR, asyncErrorHandler);
			_controlStream.client = this;
			switch(role)
			{

				case Role.AUTONOM:
				case Role.CONTROL:
					Logger.addEventListener(Event.ADDED, onLogEntryControl);
					_logStreamControl.publish(LOG_STREAM_CONTROL);
					_controlStream.play(CONTROL_STREAM);
					break;
				case Role.STUDIO:
					Logger.addEventListener(Event.ADDED, onLogEntryStudio);
					_logStreamStudio.publish(LOG_STREAM_STUDIO);
					_controlStream.play(CONTROL_STREAM);
					break;
				case Role.SUPPORT:
					_logStreamStudio.play(LOG_STREAM_STUDIO);
					_logStreamControl.play(LOG_STREAM_CONTROL);
					_controlStream.publish(CONTROL_STREAM);
					break;
			}
		}

		public function switchRemoteLogging(active : Boolean) : void
		{
			Logger.writeLog('switchRemoteLogging', active);
			var log : String;
			if(active)
				log = START_LOG;
			else
				log = STOP_LOG;
			_controlStream.send('onRemoteControl', log);
		}

		public function restart(role : String) : void
		{
			_controlStream.send('onRemoteControl', RESTART, role);
		}

		private function onLogEntryControl(event : Event) : void 
		{
			_logStreamControl.send('CONTROL >' + Logger.lastEntry);
		}

		private function onLogEntryStudio(event : Event) : void 
		{
			_logStreamStudio.send('fromRemote', 'STUDIO >' + Logger.lastEntry);
		}

		public function onRemoteControl(method : String,param : String) : void
		{
			if(_role == Role.SUPPORT)
				return;
			switch(method)
			{
				case START_LOG:
					Logger.remote = true;
					break;
				case STOP_LOG:
					Logger.remote = false;
					break;
			}
		}

		public function fromRemote(msg : String) : void
		{
			if(_role == Role.SUPPORT)
				Logger.writeLog('REMOTE LOG: ', msg);
		}

		
		private function netStatusHandler(e : NetStatusEvent) : void 
		{
		}

		
		public function onPlayStatus(infoObj : Object) : void 
		{
		}

		public function onCuePoint(infoObj : Object) : void 
		{
		}

		public function asyncErrorHandler(e : AsyncErrorEvent) : void 
		{
			//
		}

		public function set remoteControlView(remoteControlView : RemoteControlView) : void
		{
			_remoteControlView = remoteControlView;
			_remoteControlView.addEventListener(Event.CHANGE, onViewChange);
			_remoteControlView.remoteLogging = false;
		}

		private function onViewChange(event : Event) : void 
		{
			//switch(_remoteControlView.action)
			Logger.writeLog('onViewChange');
			switchRemoteLogging(_remoteControlView.remoteLogging);
		}
	}
}
