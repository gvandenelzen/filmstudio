package controls 
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;

	/**
	 * @author gerard
	 */
	public class Slider extends MovieClip 
	{
		public var mcRange : MovieClip;
		public var mcIndicator : MovieClip;
		private var _dragRect : Rectangle;

		public function Slider()
		{
			addEventListener(Event.ADDED_TO_STAGE, initUI);
		}


		public function get value() : Number
		{
			return (mcIndicator.x ) / (mcRange.width );
		}
		public function set value(value:Number) : void
		{
			mcIndicator.x=value*mcRange.width;
		}

		private function initUI(event : Event = null) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initUI);
			
			mcIndicator.useHandCursor = true;
			mcIndicator.buttonMode = true;
			mcIndicator.addEventListener(MouseEvent.MOUSE_DOWN, onMouseEvent);
			
		}


		private function onMouseEvent(event : MouseEvent) : void 
		{
			switch(event.type)
			{
				case MouseEvent.MOUSE_DOWN:
					_dragRect = new Rectangle(0, 0, mcRange.width,0);
					mcIndicator.startDrag(false, _dragRect);
					stage.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseEvent);
					stage.addEventListener(MouseEvent.MOUSE_UP, onMouseEvent);
					addEventListener(Event.ENTER_FRAME, onScroll);
					break;
				case MouseEvent.MOUSE_UP:
					stopDrag();
					dispatchEvent(new Event(Event.CHANGE));
					removeEventListener(Event.ENTER_FRAME, onScroll);
					stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseEvent);
					break;
			}
		}

		private function onScroll(event : Event) : void 
		{
			dispatchEvent(new Event(Event.CHANGE));
		}

		public function reset() : void 
		{
			mcIndicator.x = mcRange.x;
		}
	}
}
