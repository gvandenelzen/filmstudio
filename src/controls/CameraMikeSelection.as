package controls {
	import model.EncoderAction;

	import events.EncoderStatusEvent;

	import flash.events.ErrorEvent;

	import controler.DataManager;
	import controler.EncoderController;

	import fl.controls.Button;
	import fl.controls.ComboBox;

	import utils.Logger;

	import view.View;

	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.media.Camera;
	import flash.media.Microphone;
	import flash.media.Video;
	import flash.media.scanHardware;
	import flash.text.TextField;

	/**
	 * @author gerard
	 */
	public class CameraMikeSelection extends View {
		private static const EXTERNAL : int = 999;
		public var btnOK : Button;
		public var myVideo : Video;
		public var mcGain : MovieClip;
		public var mcGainControl : VolumeControl;
		public var cbCameras : ComboBox;
		public var cbMikes : ComboBox;
		public var txtCamera : TextField;
		public var txtMike : TextField;
		private var _mike : Microphone;
		private var _cam : Camera;
		private var _external : Boolean;

		public function CameraMikeSelection() {
			addEventListener(Event.ADDED_TO_STAGE, initUI);
		}

		private function initUI(event : Event) : void {
			removeEventListener(Event.ADDED_TO_STAGE, initUI);
			EncoderController.setRequestPHP(DataManager.encoderPHP);
			hide();
		}

		override public function hide() : void {
			clearMedia();
			super.hide();
		}

		override public function show() : void {
			Logger.writeLog('CameraMikeSelection show');
			scanHardware();
			
		
			mcGain.scaleX = 0;
			setCameras();
			setMikes();
			cbCameras.selectedIndex = 0;
			onCamera();
			cbMikes.selectedIndex = 0;
			setListener();
			onMike();

			super.show();
			Logger.writeLog('CameraMikeSelection show END');
		}

		private function setListener() : void {
			cbCameras.addEventListener(Event.CHANGE, onCamera);
			cbCameras.addEventListener(Event.SELECT, onCamera);
			cbMikes.addEventListener(Event.CHANGE, onMike);
			cbMikes.addEventListener(Event.SELECT, onMike);
			btnOK.addEventListener(MouseEvent.CLICK, onComplete);
			addEventListener(Event.ENTER_FRAME, onActivity);
			mcGainControl.addEventListener(Event.CHANGE, onGainChanged);
			EncoderController.addEventListener(ErrorEvent.ERROR, onEncodeError);
			EncoderController.addEventListener(EncoderStatusEvent.STATUS, onEncoderStatus);
		}

		private function removeListener() : void {
			cbCameras.removeEventListener(Event.CHANGE, onCamera);
			cbCameras.removeEventListener(Event.SELECT, onCamera);
			cbMikes.removeEventListener(Event.CHANGE, onMike);
			cbMikes.removeEventListener(Event.SELECT, onMike);
			btnOK.removeEventListener(MouseEvent.CLICK, onComplete);
			removeEventListener(Event.ENTER_FRAME, onActivity);
			mcGainControl.removeEventListener(Event.CHANGE, onGainChanged);
			EncoderController.removeEventListener(ErrorEvent.ERROR, onEncodeError);
			EncoderController.removeEventListener(EncoderStatusEvent.STATUS, onEncoderStatus);
		}

		private function onGainChanged(event : Event) : void {
			if(_mike)
				_mike.gain = mcGainControl.value * 100;
		}

		private function onComplete(event : MouseEvent) : void {
			dispatchEvent(new Event(Event.SELECT));
		}

		private function setCameras() : void {
			for (var i : uint;i < Camera.names.length;i++) {
				var camName : String = Camera.names[i];
				cbCameras.addItem({label:camName, data:i});
			}
			cbCameras.addItem({label:'Externer Stream', data:EXTERNAL});
		}

		private function setMikes() : void {
			for (var i : uint;i < Microphone.names.length;i++) {
				var mikeName : String = Microphone.names[i];
				cbMikes.addItem({label:mikeName, data:i});
			}
		}

		
		
		private function onActivity(event : Event) : void {
			if(_mike) {
				var gain : Number = _mike.activityLevel / 100;
				if(gain > 1)
					gain = 1;
				mcGain.scaleX = gain;
			}
		}

		private function onCamera(event : Event = null) : void {
			Logger.writeLog('CameraMikeSelection onCamera');
			myVideo.clear();
			myVideo.attachCamera(null);
			if(cbCameras.selectedItem.data == EXTERNAL) {
				
				setExternalStream();
				return;
			}
			stopExternalStream();
			var i : uint = cbCameras.selectedIndex;
	
			var ind : String = i.toString();
			_cam = Camera.getCamera(ind);
			if(cam) {
				txtCamera.text = _cam.name;
				myVideo.attachCamera(_cam);
			}
		}

		private function stopExternalStream() : void {
			_external = false;
			EncoderController.executeAction(EncoderAction.STOP_ENCODER);
		}

		private function setExternalStream() : void {
			_external = true;
			EncoderController.executeAction(EncoderAction.START_ENCODER);
		}

		private function onEncoderStatus(event : EncoderStatusEvent) : void {
			Logger.writeLog(this, "onEncoderStatus streaming ", event.encoderStatus.encoderStreaming);
		}

		private function onEncodeError(event : ErrorEvent) : void {
			Logger.writeLog(this, "onEncodeError ", event.text);
		}

		private function onMike(event : Event = null) : void {
			Logger.writeLog('CameraMikeSelection onMike');
			
			var i : uint = cbMikes.selectedIndex;
			_mike = Microphone.getMicrophone(i);
			if(_mike) {
				txtMike.text = _mike.name;
				_mike.setLoopBack(true);
				_mike.setUseEchoSuppression(true);
				_mike.setSilenceLevel(10);
				mcGainControl.value = _mike.gain / 100;
			}
			Logger.writeLog('CameraMikeSelection onMike END');
		}

		public function get cam() : Camera {
			return _cam;
		}

		public function get mike() : Microphone {
			_mike.setLoopBack(false);
			return _mike;
		}

		public function dispose() : void {
			EncoderController.executeAction(EncoderAction.STOP_ENCODER);
			clearMedia();
		}

		private function clearMedia() : void {
			if(_mike) {
				_mike.setLoopBack(false);
			}
			removeListener();
			myVideo.clear();
			myVideo.attachCamera(null);
		}

		public function get external() : Boolean {
			return _external;
		}
	}
}

