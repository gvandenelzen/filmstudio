package controls 
{
	import utils.Logger;

	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.text.TextField;
	import flash.utils.Timer;

	/**
	 * @author gerard
	 */
	public class CountDown extends MovieClip 
	{
		public var txtCount:TextField;
		private var _repeat:uint=5;
		private var _timer:Timer=new Timer(1000,_repeat);
		public function CountDown()
		{
			_timer.addEventListener(TimerEvent.TIMER_COMPLETE,onEndTimer);
			_timer.addEventListener(TimerEvent.TIMER,onTimer);
			reset();
		}

		private function onTimer(event : TimerEvent) : void 
		{
			txtCount.text=(_repeat-_timer.currentCount).toString();
		}

		private function onEndTimer(event : TimerEvent) : void 
		{
			dispatchEvent(new Event( Event.COMPLETE));
		}
		public function set repeat(repeat:uint):void
		{
			_repeat=repeat;
			_timer.repeatCount=repeat;
		}
		public function start():void
		{
			Logger.writeLog('Countdoen started:',_repeat);
			_timer.reset();
			txtCount.text=_repeat.toString();
			visible = true;
			_timer.start();
		}
		public function get isRunning():Boolean
		{
			return _timer.running;
		}
		public function reset() : void 
		{
			_timer.stop();
			_timer.reset();
			visible = false;
		}
	}
}
