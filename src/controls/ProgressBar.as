package controls 
{
	import utils.Logger;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	/**
	 * @author gerard
	 */
	public class ProgressBar extends MovieClip 
	{
		public var mcProgress : MovieClip;
		private var _mask : Sprite;
		private var _background : Sprite;
		private var _isSeekBar : Boolean;
		private var _isLiveSeek : Boolean;
		private var _mouseIsDown : Boolean;

		public function ProgressBar()
		{
			_mask = createBarClone();
			addChild(_mask);
			mcProgress.mask = _mask;
			_background = createBarClone();
			addChild(_background);
			isSeekBar = _isSeekBar;
			addEventListener(Event.ADDED_TO_STAGE, initUI);
		}

		private function initUI(event : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initUI);
			

			value = 0;
		}

		private function onMouse(event : MouseEvent) : void 
		{
			Logger.writeLog("Progressbar mouseEvent", event.type);
			switch(event.type)
			{
				case MouseEvent.MOUSE_DOWN:
					_mouseIsDown = true;
					stage.addEventListener(MouseEvent.MOUSE_UP, onMouse);
					stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouse);
					adjustMask();
					signalChange();
					break;
				case MouseEvent.MOUSE_UP:
					stage.removeEventListener(MouseEvent.MOUSE_UP, onMouse);
					stage.removeEventListener(MouseEvent.MOUSE_MOVE, onMouse);
					_mouseIsDown = false;
					break;
				case MouseEvent.MOUSE_MOVE:
					if(_isLiveSeek)
						signalChange();
					break;
			}
		}

		private function signalChange() : void 
		{
			dispatchEvent(new Event(Event.CHANGE));
		}

		private function adjustMask(event : Event=null) : void 
		{
			_mask.width = mouseX;
			Logger.writeLog("adjsuMask ",mcProgress.width,mouseX,_mask.width);
		}

		private function createBarClone() : Sprite 
		{
			var sprite : Sprite = new Sprite();
			with(sprite.graphics)
			{
				beginFill(0, 0);
				drawRect(0, 0, mcProgress.width, mcProgress.height);
			}
			sprite.x = mcProgress.x;
			sprite.y = mcProgress.x;
			return sprite;
		}

		public function set isSeekBar(isSeekBar : Boolean) : void
		{
			Logger.writeLog("isSeekBar ", isSeekBar, _background);
			_isSeekBar = isSeekBar;
			if(_background)
			{
				if(_isSeekBar)
					_background.addEventListener(MouseEvent.MOUSE_DOWN, onMouse);
				else
					_background.removeEventListener(MouseEvent.MOUSE_DOWN, onMouse);
			}
		}

		public function set value(value : Number) : void
		{
			if(_mouseIsDown)
				return;
			if(value > 1)
				value = 1;
			_mask.width = value * mcProgress.width;
			;
		}

		public function get value( ) : Number
		{
			var val : Number = _mask.width / mcProgress.width;
			if(val > 1)
				val = 1;
			if(val < 0)
				val = 0;
			return val ;
		}

		public function set isLiveSeek(isLiveSeek : Boolean) : void
		{
			_isLiveSeek = isLiveSeek;
		}
	}
}
