package controls 
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;

	/**
	 * @author gerard
	 */
	public class TextListItem extends MovieClip 
	{
		public var txtCaption:TextField;
		private var _caption:String;
		public function TextListItem()
		{
			addEventListener(Event.ADDED_TO_STAGE,initUI);
			
		}

		private function onMouseDown(event : MouseEvent) : void 
		{
			if(hitTestPoint(stage.mouseX,stage.mouseX))
				trace('mosueDown');
		}

		private function initUI(event : Event) : void 
		{
			stage.addEventListener(MouseEvent.MOUSE_DOWN,onMouseDown);
			txtCaption.autoSize=TextFieldAutoSize.LEFT;
			txtCaption.mouseEnabled=false;
			txtCaption.selectable=false;
			if(_caption)
				txtCaption.text=_caption;
		}

		public function set caption(caption : String) : void
		{
			_caption = caption;
			if(txtCaption)
				txtCaption.text=_caption;
		}
	}
}
