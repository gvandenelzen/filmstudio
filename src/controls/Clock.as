package controls 
{
	import utils.DateTimeUtils;

	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.text.TextField;
	import flash.utils.Timer;

	/**
	 * @author gerard
	 */
	public class Clock extends MovieClip 
	{
		public var txtTime:TextField;
		private var _timer:Timer;
		public function Clock()
		{
			_timer=new Timer(100,0);
			_timer.addEventListener(TimerEvent.TIMER, adjustTime);
			addEventListener(Event.ADDED_TO_STAGE,initUI);
		}

		private function adjustTime(event : TimerEvent) : void 
		{
			txtTime.text=DateTimeUtils.formattedTime();
		}

		private function initUI(event : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE,initUI);
			_timer.start();
		}
	}
}
