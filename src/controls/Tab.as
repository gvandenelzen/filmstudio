package controls 
{
	import flash.text.TextFormat;

	/**
	 * @author gerard
	 */
	public class Tab extends TextButton 
	{
		private var _tF:TextFormat;
		public function Tab()
		{
			_tF=txtCaption.getTextFormat();
			selected=false;
		}
		public function set selected(selected:Boolean):void
		{
			
			
			if(enabled)
			{	
				_tF.bold=selected;
				buttonMode=!selected;
				useHandCursor=!selected;
				mouseEnabled=!selected;
				txtCaption.setTextFormat(_tF);
			}
		}
	}
}
