package controls 
{
	import fl.controls.Button;
	import fl.controls.ComboBox;
	import utils.Logger;
	import view.View;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.media.Camera;
	import flash.media.Video;
	import flash.media.scanHardware;
	import flash.text.TextField;

	/**
	 * @author gerard
	 */
	public class CameraSelection extends View 
	{
		public var btnOK : Button;
		public var myVideo : Video;
		public var cbCameras : ComboBox;
		public var txtCamera : TextField;
		private var _cam : Camera;

		public function CameraSelection()
		{
			addEventListener(Event.ADDED_TO_STAGE, initUI);
		}

		private function initUI(event : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initUI);
		
			hide();
		}

		override public function show() : void
		{
			Logger.writeLog('CameraSelection show');
			scanHardware();
			cbCameras.addEventListener(Event.CHANGE, onCamera);
			cbCameras.addEventListener(Event.SELECT, onCamera);
			setCameras();
			cbCameras.selectedIndex = 0;
			onCamera();
			btnOK.addEventListener(MouseEvent.CLICK, onComplete);
			super.show();
			Logger.writeLog('CameraMikeSelection show END');
		}

		private function onComplete(event : MouseEvent) : void 
		{
			dispatchEvent(new Event(Event.SELECT));
		}

		private function setCameras() : void 
		{
			for (var i : uint;i < Camera.names.length;i++)
			{
				var camName : String = Camera.names[i];
				cbCameras.addItem({label:camName, data:i});
			}
		}

		override public function hide() : void
		{
			myVideo.clear();
			myVideo.attachCamera(null);
			cbCameras.removeEventListener(Event.CHANGE, onCamera);
			cbCameras.removeEventListener(Event.SELECT, onCamera);
			super.hide();
			Logger.writeLog('CameraMikeSelection hide');
		}

		private function onCamera(event : Event = null) : void
		{
			Logger.writeLog('CameraSelection onCamera');
			myVideo.clear();
			myVideo.attachCamera(null);
			var i : uint = cbCameras.selectedIndex;
	
			var ind : String = i.toString();
			_cam = Camera.getCamera(ind);
			if(cam)
			{
				txtCamera.text = _cam.name;
				myVideo.attachCamera(_cam);
			}
		}

		public function get cam() : Camera
		{
			return _cam;
		}

	}
}

