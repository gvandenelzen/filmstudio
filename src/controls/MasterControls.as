package controls 
{
	import flash.display.MovieClip;

	/**
	 * @author gerard
	 */
	public class MasterControls extends MovieClip 
	{
		public var mcMasterSoundControl:MasterSoundControl;
		public function MasterControls()
		{
			visible=false;
		}
		public function init():void
		{
			visible=true;
		}
	}
}
