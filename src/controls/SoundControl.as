package controls 
{
	import utils.Logger;

	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.media.SoundTransform;

	/**
	 * @author gerard
	 */
	public class SoundControl extends MovieClip 
	{
		public static var MUTE : SoundTransform = new SoundTransform(0);
		public var mcMute : Toggle;
		public var mcVolumeControl : VolumeControl;

		public function SoundControl()
		{
			Logger.writeLog("MasterSoundControl init");
			mcMute.addEventListener(MouseEvent.CLICK, onMute);
			mcVolumeControl.addEventListener(Event.CHANGE, onVolumeChanged);
		}

		private function onVolumeChanged(event : Event) : void 
		{
			dispatchEvent(new Event(Event.CHANGE));
		}

		private function onMute(event : MouseEvent) : void 
		{
			
			dispatchEvent(new Event(Event.CHANGE));
		}

		public function get value() : Number
		{
			if(!mcMute.selected)
				return 0;
			return mcVolumeControl.value;
		}

		public function set value(value : Number) : void
		{
			mcVolumeControl.value = value;
		}

		public function set mute(mute : Boolean) : void
		{
			mcMute.selected = mute;
		}
	}
}
