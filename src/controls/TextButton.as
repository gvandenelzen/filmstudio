package controls 
{
	import flash.events.Event;
	import flash.display.MovieClip;
	import flash.text.TextField;

	/**
	 * @author gerard
	 */
	public class TextButton extends MovieClip 
	{
		public var txtCaption : TextField;
		private var _caption : String;
	

		public function TextButton(caption : String = "")
		{
			if(txtCaption)
				txtCaption.text = caption;
			else
				_caption = caption;
			addEventListener(Event.ADDED_TO_STAGE, initUI);
		}

		private function initUI(event : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initUI);
			if(txtCaption)
			{
				txtCaption.selectable = false;
				txtCaption.mouseEnabled = false;
				;
			}
			if(_caption)
				caption = _caption;
			enabled=true;
		}

		public function set caption(caption : String) : void
		{
			if(!txtCaption)
				return;
			txtCaption.text = caption;
		}

	

		override public function set enabled(enabled : Boolean) : void
		{
			buttonMode = enabled;
			useHandCursor = enabled;
			mouseEnabled = enabled;
			super.enabled = enabled;	
		}

	}
}
