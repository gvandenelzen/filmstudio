package controls 
{
	import flash.display.Sprite;

	/**
	 * @author gerard
	 */
	public class RecordBar extends Sprite 
	{
		private static const STRIPE_WIDTH : Number = 3;

		private var _duration : uint = 60;
		private var _flexTime : uint = 0;
		private var _width : Number;
		private var _height : Number;

		public function RecordBar(width : Number,height : Number)
		{
			_width = width;
			_height = height;
			makeStripedMask(width, height);
		}

		private function makeStripedMask(mwidth : Number,mheight : Number) : void 
		{
			var sMask : Sprite = new Sprite();
			with(sMask.graphics)
			{
				for (var i : Number = 0;i < mwidth;i += STRIPE_WIDTH * 2)
				{
					beginFill(0xFF0000, 0.5);
					drawRect(i, 0, STRIPE_WIDTH, mheight);
					endFill();
				}
			}
			addChild(sMask);
			mask = sMask;
		}

		public function set time(time : uint) : void
		{
			var gw : uint = _width * (_duration / (_duration+_flexTime));
			var rw : uint = _width - gw;
			with(graphics)
			{
				clear();
				var w : uint = gw * (time /(_duration));
				if(w> gw)
					w=gw;
				beginFill(0x00FF00);
				drawRect(0, 0, w, _height);
				endFill();
				if(time > _duration)
				{
					time = time-_duration ;
					var nw : uint = rw * (time / _flexTime);
					beginFill(0xFF0000);
					drawRect(gw, 0, nw, _height)	;
					endFill();
				}
			}
		}

		public function set duration(duration : uint) : void
		{
			_duration = duration;
		}

		public function set flexTime(flexTime : uint) : void
		{
			_flexTime = flexTime;
		}
	}
}
