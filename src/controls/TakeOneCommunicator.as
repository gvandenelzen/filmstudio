package  controls
{
	import old.TakeOne;

	import utils.Logger;

	import flash.events.AsyncErrorEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.NetStatusEvent;
	import flash.net.NetConnection;
	import flash.net.NetStream;

	/**
	 * @author gerard
	 */
	public class TakeOneCommunicator extends EventDispatcher 
	{
		private static const COMSTREAM_CONTROLLER : String = 'takecomstream_controller';
		private static const COMSTREAM_RECORDER : String = 'takecomstream_recorder';
		private var _comStreamOUT : NetStream;
		private var _comStreamIN : NetStream;
		private var _method : String;
		private var _parameter : *;

		public function TakeOneCommunicator(netConnection : NetConnection,role : String)
		{
			Logger.writeLog('Communicator init',role);
				
			_comStreamOUT = new NetStream(netConnection);
			_comStreamOUT.addEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
			_comStreamOUT.addEventListener(AsyncErrorEvent.ASYNC_ERROR, asyncErrorHandler);
			_comStreamOUT.client = this;
			_comStreamIN = new NetStream(netConnection);
			_comStreamIN.addEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
			_comStreamIN.addEventListener(AsyncErrorEvent.ASYNC_ERROR, asyncErrorHandler);
			_comStreamIN.client = this;
			switch(role)
			{

				case TakeOne.RECORDER:
					_comStreamOUT.publish(COMSTREAM_RECORDER);
					_comStreamIN.play(COMSTREAM_CONTROLLER);
					break;
				case TakeOne.CONTROLLER:
					_comStreamOUT.publish(COMSTREAM_CONTROLLER);
					_comStreamIN.play(COMSTREAM_RECORDER);
					break;
			}
			
		}

		public function onNetRequest(...args : *) : void
		{
			var parameter : Array = args;
			_method = parameter[0];
			if(parameter.length > 1)
		 		_parameter = parameter[1];
			dispatchEvent(new Event(Event.CHANGE));
		}

		public function call(method : String,param : *=  '') : void
		{
			if(_comStreamOUT)
			{
				_comStreamOUT.send('onNetRequest', method, param);
			}
		}

		public function get method() : String
		{
			return _method;
		}

		public function get parameter() : *
		{
			return _parameter;
		}

		private function netStatusHandler(e : NetStatusEvent) : void 
		{
		}

		
		public function onPlayStatus(infoObj : Object) : void 
		{
		}

		public function onCuePoint(infoObj : Object) : void 
		{
		}

		public function asyncErrorHandler(e : AsyncErrorEvent) : void 
		{
			//
		}
	}
}
