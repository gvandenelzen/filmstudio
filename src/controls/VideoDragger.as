package controls 
{
	import flash.display.MovieClip;
	import flash.geom.Point;
	import flash.media.Video;
	import flash.net.NetStream;

	/**
	 * @author gerard
	 */
	public class VideoDragger extends MovieClip 
	{
		private var _netStream : NetStream;
		private var _video : Video;
		private var _origin : Point;

		public function VideoDragger()
		{
			alpha = 0;
			_origin = new Point(x, y);
			_video = new Video(150, 100);
			addChild(_video);
			buttonMode = true;
		}

		public function set netStream(netStream : NetStream) : void
		{
			alpha = 0.6;
			_video.attachNetStream(netStream);
			_netStream = netStream;
		}

		public function reset() : void
		{
			_video.clear();
			alpha = 0;
			x = _origin.x;
			y = _origin.y;
			_video.attachNetStream(null);
		}

		public function get netStream() : NetStream
		{
			return _netStream;
		}
	}
}
