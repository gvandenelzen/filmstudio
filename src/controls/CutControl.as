package controls 
{
	import utils.DateTimeUtils;
	import utils.Logger;
	import utils.LoopManager;

	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;

	/**
	 * @author gerard
	 */
	public class CutControl extends MovieClip 
	{
		public static const IN_POSITION : String = "IN_POSTION";
		public static const OUT_POSITION : String = "OUT_POSTION";
		public static const PLAY_POSITION : String = "PLAY_POSTION";
		public var mcIn : Marker;
		public var mcOut : Marker;
		public var mcPosition : Marker;
		public var mcTrack : MovieClip;
		public var mcCut : MovieClip;
		private var _markers : Array;
		private var _duration : Number;
		private var _dragging : Boolean;
		private var _action : String;
		private var _lastPosition:Number;
		private var _activeMarker:Marker;
		public function CutControl()
		{
			addEventListener(Event.ADDED_TO_STAGE, initUI);
		}

		private function initUI(event : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initUI);
			_lastPosition=0;
			initMarkers();
		}

		private function initMarkers() : void 
		{
			_markers = [];
			_markers.push(mcIn);
			_markers.push(mcOut);
			_markers.push(mcPosition);
			for each (var marker:Marker in _markers)
			{
				marker.addEventListener(MouseEvent.MOUSE_DOWN, onMarkerMouse);
			}
			resetMarkers();
		}

		private function resetMarkers() : void
		{
			deselectMarkers();
			mcIn.x = 0;
			mcOut.x =  mcTrack.width;
			mcPosition.x = 0;
		}

		private function deselectMarkers() : void
		{
			for each (var marker:Marker in _markers)
			{
				marker.selected = false;
			}
		}

		public function get playPosition() : Number
		{
			return mcPosition.x / mcTrack.width;
		}

		public function set duration(duration : Number) : void
		{
			_duration = duration;
		}

		public function set endPosition(position : Number) : void
		{
			mcOut.x = position * mcTrack.width;
			setMarkerActive(mcOut);
			setCutTrack();
			_action = OUT_POSITION;
		}
		public function get endPosition() : Number
		{
			return mcOut.x / mcTrack.width;

		}

		public function set inPosition(position : Number) : void
		{
			mcIn.x = position * mcTrack.width;
			setMarkerActive(mcIn);
			setCutTrack();
			_action = IN_POSITION;
		}
		public function get inPosition() : Number
		{
			return mcIn.x / mcTrack.width;

		}

		public function set currentTime(time : Number) : void
		{
			mcPosition.time = DateTimeUtils.secondsToVideoTime(time);
			if(mcPosition.selected && _dragging)
				return;
			mcPosition.x = (time / _duration) * mcTrack.width;
		}

		private function onMarkerMouse(event : MouseEvent) : void 
		{
			switch(event.type)
			{
				case MouseEvent.MOUSE_DOWN:
					
					LoopManager.register(checkMarkerPosition);
					startMarkerDrag(event.currentTarget as Marker);
					_dragging = true;
					stage.addEventListener(MouseEvent.MOUSE_UP, onMarkerMouse);
					break;
				case MouseEvent.MOUSE_UP:
					LoopManager.unregister(checkMarkerPosition);
					_dragging = false;
					stopDrag();
					setCutTrack();
					stage.removeEventListener(MouseEvent.MOUSE_UP, onMarkerMouse);
					dispatchEvent(new Event(Event.SELECT));
					
					break;
			}
		}

		private function startMarkerDrag(marker : Marker) : void 
		{
			setMarkerActive(marker);

			var min : Number = 0;
			var max : Number = mcTrack.width;
			switch(marker)
			{
				case mcIn:
					max = mcOut.x - mcTrack.x;
					_action = IN_POSITION;
					break;
				case mcOut:
					min = mcIn.x;
					max = mcTrack.width - min;
					_action = OUT_POSITION;
					break;
				case mcPosition:
					_action = PLAY_POSITION;
					break;			
			}
			var dragRect : Rectangle = new Rectangle(min, marker.y, max, 0);
			marker.startDrag(false, dragRect);
		}

		private function setMarkerActive(marker : Marker) : void 
		{
			_activeMarker=marker;
			deselectMarkers();
			marker.parent.swapChildren(marker, marker.parent.getChildAt(marker.parent.numChildren - 1));
			marker.selected = true;
			switch(marker)
			{
				case mcIn:
					_action = IN_POSITION;
					break;
				case mcOut:
					_action = OUT_POSITION;
					break;
				case mcPosition:
					_action = PLAY_POSITION;
					break;			
			}
		}

		private function checkMarkerPosition() : void 
		{
			if(_lastPosition==_activeMarker.x)
				return;
			_lastPosition=_activeMarker.x;
			setCutTrack();
			dispatchEvent(new Event(Event.CHANGE));
		}

		private function setCutTrack() : void 
		{
			if(mcOut.selected || mcIn.selected)
			{
				mcCut.x = mcIn.x;
				mcCut.width = mcOut.x - mcIn.x;
			}
		}

		public function get action() : String
		{
			return _action;
		}
		
		public function set action(action : String) : void
		{
			switch(action)
			{
				case IN_POSITION:
					setMarkerActive(mcIn);
					break;
				case OUT_POSITION:
					setMarkerActive(mcOut);
					break;
				case PLAY_POSITION:
					setMarkerActive(mcPosition);
					break;			
			}
		}
	}
}
