package controls 
{
	import events.VideoControlEvent;
	

	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.text.TextField;

	/**
	 * @author gerard
	 */
	public class TestInOutControl extends MovieClip 
	{
		public var txtDX:TextField;
		public var txtStart:TextField;
		public var txtPart:TextField;
		public var txtTime:TextField;
		public var  mcIn : Slider;
		public var  mcCurrent : Slider;
		public var  mcFine : Slider;
		private var _inValue : Number;
		private var _duration : Number;

		public function TestInOutControl()
		{
			addEventListener(Event.ADDED_TO_STAGE, initUI);
		}

		public function get from() : Number
		{
			return mcIn.value;
		}

		public function get to() : Number
		{
			return 1;
		}
		public function set duration(duration:Number):void
		{
			_duration=duration;
			showPart();
		}
		public function set currentTime(time:Number):void
		{
			mcCurrent.value=(time/_duration);
			txtTime.text=time.toFixed(3);
			mcCurrent.mouseChildren=false;
		}
		private function initUI(event : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initUI);
			mcIn.addEventListener(Event.CHANGE, onInChanged);
			duration=1;
		}

		private function doPlay() : void 
		{
			var vEvent : VideoControlEvent;
			vEvent = new VideoControlEvent(VideoControlEvent.PLAY);	
			dispatchEvent(vEvent);
		}

		private function onInChanged(event : Event) : void 
		{
			mcFine.value = 0.5;
			mcFine.addEventListener(Event.CHANGE, onFineChanged);
			_inValue = mcIn.value	;
			txtStart.text=(_inValue*_duration).toFixed(3);
			
			doPlay();
			showPart();
		}

		private function onFineChanged(event : Event) : void 
		{
			var dx : Number = (mcFine.value - 0.5) / 10;
			txtDX.text=(dx*_duration).toFixed(3);
			mcIn.value = _inValue + dx;
			showPart();
			doPlay();
		}
		private function showPart():void
		{
			txtPart.text=(mcIn.value*_duration).toFixed(3)+":"+_duration.toFixed(3);
			
		}
	}
}
