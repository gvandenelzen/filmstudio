package controls 
{
	import fl.controls.CheckBox;
	import fl.controls.listClasses.ICellRenderer;
	import fl.controls.listClasses.ListData;

	import flash.events.MouseEvent;

	/**
	 * @author gerard
	 */
	public class CheckBoxCell extends CheckBox  implements ICellRenderer
	{

		protected var _listData : ListData;
		private var _active : Boolean;
		private var _data : Object;

		public function CheckBoxCell()
		{
			super();
			addEventListener(MouseEvent.CLICK, onClick);
			label = "";
			_selected = true;
		}

		private function onClick(event : MouseEvent) : void 
		{
			//event.stopImmediatePropagation();
			_selected = !_selected;
			_data.active=_selected;
			_active=_selected;
			//dispatchEvent(new Event(Event.CHANGE));
		}

		public function get data() : Object
		{
			return _data;
		}

		public function set data(value : Object) : void
		{
			_data=value;
			for (var i:String in value)
			{
				if(i=='active')
					_selected=value[i];
			}
		}

		public function get listData() : ListData
		{
			return _listData;
		}

		public function set listData(value : ListData) : void
		{
			_listData = value;
		}

		override public function set selected(selected : Boolean) : void
		{
			//_selected=selected;
		}

		override public function get selected() : Boolean
		{
			return _selected;
		}

		public function get active() : Boolean
		{
			return _active;
		}

		public function set active(active : Boolean) : void
		{
			_active = active;
			_selected=active;
		}
	}
}
