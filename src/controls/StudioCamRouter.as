package controls 
{
	import flash.geom.Point;

	import fl.controls.Button;

	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;

	/**
	 * @author gerard
	 */
	public class StudioCamRouter extends MovieClip 
	{
		public var btnStudioCam : Button;

		public function StudioCamRouter()
		{
			visible = false;
		}

		public function init() : void
		{
			btnStudioCam.label = 'Studiobild';
			btnStudioCam.addEventListener(MouseEvent.MOUSE_DOWN, onMouse);
			visible = true;
		}

		private function onMouse(event : MouseEvent) : void 
		{
			switch(event.type)
			{
				case MouseEvent.MOUSE_DOWN:
					startDrag();
					if(stage)
						stage.addEventListener(MouseEvent.MOUSE_UP, onMouse);
					else
						addEventListener(MouseEvent.MOUSE_UP, onMouse);
					dispatchEvent(new Event(Event.SELECT));
					break;
				case MouseEvent.MOUSE_UP:
					stopDrag();
					if(stage)
						stage.removeEventListener(MouseEvent.MOUSE_UP, onMouse);
					else
						removeEventListener(MouseEvent.MOUSE_UP, onMouse);
						
					dispatchEvent(new Event(Event.COMPLETE));
					break;
			}
		}

		public function resetPosition(routerOrigin : Point) : void 
		{
			x = routerOrigin.x;
			y = routerOrigin.y;
		}
	}
}
