package controls 
{
	import fl.controls.ComboBox;
	import fl.controls.listClasses.CellRenderer;
	import fl.controls.listClasses.ICellRenderer;

	/**
	 * @author gerard
	 */
	public class ComboBoxCell extends CellRenderer  implements ICellRenderer
	{
		public var cBB : ComboBox  ;

		public function ComboBoxCell()
		{
			super();
		}

		
		
		override public function set data(arg0 : Object) : void
		{
			cBB.addItem(arg0);
		}

		override protected function drawLayout() : void 
		{
			this.addChild(cBB);
			super.drawLayout();
		}
	}
}
/*
public class CheckBoxCell extends CellRenderer implements ICellRenderer {
        var chk:CheckBox = new CheckBox();
    
        public function CheckBoxCell():void {

        }
        override protected function drawLayout():void {
            this.addChild(chk);
            chk.label = data.endhour;
            super.drawLayout();
        }
    }
*/