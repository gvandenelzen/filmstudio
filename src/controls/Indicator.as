package controls 
{
	import flash.display.MovieClip;
	import flash.geom.ColorTransform;

	/**
	 * @author gerard
	 */
	public class Indicator extends MovieClip 
	{
		public var mcColor:MovieClip;
		public function Indicator()
		{
		}
		public function set color(color:uint):void
		{
			var colorTransform:ColorTransform=new ColorTransform();
			colorTransform.color=color;
			mcColor.transform.colorTransform=colorTransform;
			visible=true;
		}
	}
}
