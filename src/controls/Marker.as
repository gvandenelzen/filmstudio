package controls 
{
	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;

	/**
	 * @author gerard
	 */
	public class Marker extends MovieClip 
	{
		public var mcSelected : MovieClip;
		public var mcNormal : MovieClip;
		public var txtTime:TextField;
		private var _selected : Boolean;

		public function Marker()
		{
			selected=false;
			if(txtTime)
				txtTime.autoSize=TextFieldAutoSize.CENTER;
		}

		public function get selected():Boolean
		{
			return _selected;
		}
		public function set selected(selected : Boolean) : void
		{
			_selected=selected;
			if(mcSelected)
				mcSelected.visible = selected;
			if(mcNormal)
				mcNormal.visible = !selected;
		}
		public function set time(time:String):void
		{
			if(txtTime)
				txtTime.text=time;
		}
	}
}
