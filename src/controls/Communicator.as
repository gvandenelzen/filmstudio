package  controls
{
	import model.Role;

	import utils.Logger;

	import flash.events.AsyncErrorEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.NetStatusEvent;
	import flash.net.NetConnection;
	import flash.net.NetStream;

	/**
	 * @author gerard
	 */
	public class Communicator extends EventDispatcher 
	{
		private static const COMSTREAM_STUDIO : String = 'comstream_studio';
		private static const COMSTREAM_CONTROL : String = 'comstream_control';
		private var _comStreamOUT : NetStream;
		private var _comStreamIN : NetStream;
		private var _method : String;
		private var _parameter : *;

		public function Communicator(netConnection : NetConnection,role : String)
		{
			Logger.writeLog('Communicator init',role);
			if( role==Role.AUTONOM)
				return;
				
			_comStreamOUT = new NetStream(netConnection);
			_comStreamOUT.addEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
			_comStreamOUT.addEventListener(AsyncErrorEvent.ASYNC_ERROR, asyncErrorHandler);
			_comStreamOUT.client = this;
			_comStreamIN = new NetStream(netConnection);
			_comStreamIN.addEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
			_comStreamIN.addEventListener(AsyncErrorEvent.ASYNC_ERROR, asyncErrorHandler);
			_comStreamIN.client = this;
			switch(role)
			{

				case Role.CONTROL:
					_comStreamOUT.publish(COMSTREAM_CONTROL);
					_comStreamIN.play(COMSTREAM_STUDIO);
					break;
				case Role.STUDIO:
					_comStreamOUT.publish(COMSTREAM_STUDIO);
					_comStreamIN.play(COMSTREAM_CONTROL);
					break;
			}
			
		}

		public function onNetRequest(...args : *) : void
		{
			var parameter : Array = args;
			_method = parameter[0];
			if(parameter.length > 1)
		 		_parameter = parameter[1];
			dispatchEvent(new Event(Event.CHANGE));
		}

		public function call(method : String,param : *=  '') : void
		{
			if(_comStreamOUT)
			{
				_comStreamOUT.send('onNetRequest', method, param);
			}
		}

		public function get method() : String
		{
			return _method;
		}

		public function get parameter() : *
		{
			return _parameter;
		}

		private function netStatusHandler(e : NetStatusEvent) : void 
		{
		}

		
		public function onPlayStatus(infoObj : Object) : void 
		{
		}

		public function onCuePoint(infoObj : Object) : void 
		{
		}

		public function asyncErrorHandler(e : AsyncErrorEvent) : void 
		{
			//
		}
	}
}
