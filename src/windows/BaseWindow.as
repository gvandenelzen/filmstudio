package  
windows
{
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.text.TextField;

	/**
	 * @author Gérard van den Elzen, © DMT 2010 
	 */
	public class BaseWindow extends MovieClip 
	{
		public var btnClose : SimpleButton;
		public var mcBackground : MovieClip;
		public var txtHeader : TextField;
		private var _initialized : Boolean;
		private var _header : String = 'Info';
		
		private var _hasBlocker : Boolean;

		public function BaseWindow()
		{
			_hasBlocker = true;
			if (stage)
				initUI();
			else
				addEventListener(Event.ADDED_TO_STAGE, initUI);
		}

		private function initUI(event : Event = null) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initUI);
			stage.addEventListener(Event.RESIZE, onResize);
			addEventListener(Event.REMOVED_FROM_STAGE, dispose);
			
			if(!_initialized)
			{
				mcBackground.filters = [new DropShadowFilter(5, 45, 0, 0.6, 5, 5, 0.7, 3)];
				if(btnClose)
					btnClose.addEventListener(MouseEvent.CLICK, onClose);
				hasBlocker = _hasBlocker;	
				initialize();
				if(txtHeader)	
					txtHeader.text = _header;
				_initialized = true;
			}
		}

		private function onResize(event : Event) : void 
		{
			setBlocker();
		}

		private function setBlocker() : void 
		{
			if(!stage)
				return;
			graphics.clear();

			x = (stage.width - width) / 2;
			y =(stage.height - height) / 2;
			with(graphics)
			{
				beginFill(0X000000, 0.3);
				drawRect(-(stage.width-width)/2, -(stage.height-height)/2, stage.width, stage.height);
			}
		}

		private function removeBlocker() : void
		{
			graphics.clear();
		}

		private function onClose(event : MouseEvent) : void 
		{
			dispatchEvent(new Event(Event.CANCEL));
		}

		protected function initialize() : void 
		{
			throw new Error('MUST be iniatized in extende class');
		}

		public function set header(header : String) : void
		{
			_header = header;
			if(txtHeader)
				txtHeader.text = header;
		}

		protected function set hasBlocker(hasBlocker : Boolean) : void
		{
			_hasBlocker = hasBlocker;
			if(!_hasBlocker)
				removeBlocker();
			else
				setBlocker();
		}

		public function dispose(event:Event=null) : void
		{
			if(stage)
			{
				stage.removeEventListener(Event.RESIZE, onResize);
			}
			removeEventListener(Event.REMOVED_FROM_STAGE, dispose);
		}
	}
}
