package  windows
{
	import fl.controls.Button;

	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;

	/**
	 * @author Gérard van den Elzen, © DMT 2010 
	 */
	public class MessageWindow extends BaseWindow 
	{
		public var btnOK : Button;
		public var txtMessage : TextField;

		private var _message : String='';

		public function MessageWindow()
		{
			_message = "";
		}

		override protected function initialize() : void 
		{
			btnOK.addEventListener(MouseEvent.CLICK, onCancel);
			txtMessage.text = _message;
		}
		private function onCancel(event : MouseEvent) : void 
		{
			dispatchEvent(new Event(Event.CANCEL));
		}
		
		public function set message(message : String) : void
		{
			_message = message;
			if(txtMessage)
				txtMessage.text = _message;
		}
	}
}
