package utils 
{

	/**
	 * @author gerard
	 */
	public class TextUtil 
	{
		public static const SPACE:String=' ';
		public static function containesOnlySpaces(txt:String):Boolean
		{
			var tmpArray:Array=txt.split('');
			for each (var char:String in tmpArray)
			{
				if(char!=SPACE)
					return false;
			}
			return true;
		}
	}
}
