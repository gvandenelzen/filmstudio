package utils 
{

	/**
	 * @author gerard
	 */
	public class HTMLUtils 
	{
		private static const HTML_CODES : Array = ["\\'s","\\/","\/","\\\\",'&Auml;','&auml;','&Ouml;','&ouml;','&Uuml;','&uuml;','&szlig;','&Eacute;','&eacute;','&agrave;','&euro;','&amp;'];
		private static const CHARS: Array = ["'s" ,"/","/","\\",'Ä','ä','Ö','ö','Ü','ü','ß','É','é','à','€','&'];
		public static function convertHTMLEntities(html : String) : String
		{
			var i:uint=HTML_CODES.length;
			while(i--)
			{
				html=html.split(HTML_CODES[i]).join(CHARS[i]);
			}
			return html;
		}
	}
}
