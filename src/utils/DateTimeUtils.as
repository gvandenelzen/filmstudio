package utils
{

	/**
	 * @author gerard
	 */
	public class DateTimeUtils 
	{
		public static const SECONDS_PER_MINUTE : uint = 60;
		public static const SECONDS_PER_HOUR : uint = SECONDS_PER_MINUTE * 60;
		public static const SECONDS_PER_DAY : uint = SECONDS_PER_HOUR * 24;

		public static function addLeadingZeros(number : uint,positions : uint = 2) : String
		{
			var nstring : String = String(number);
			while(nstring.length < positions)
				nstring = '0' + nstring;
				
			return nstring;
		}

		public static function dbTimeStampToTime(ts : String) : Number
		{
			//	2011-04-12 09:17:22
			var tmpArray : Array = ts.split(' ');
			var dateS : String = tmpArray[0];
			var dateArray : Array = dateS.split('-');
			var year : uint = dateArray[0];
			var month : uint = dateArray[1];
			month--;
			var day : uint = dateArray[2];
			var timeS : String = tmpArray[1];
			var timeArray : Array = timeS.split(':');
			var hours : uint = timeArray[0];
			var minutes : uint = timeArray[1];
			var seconds : uint = timeArray[2];
			var date : Date = new Date(year, month, day, hours, minutes, seconds);
			return date.time;
		}

		public static function fileStampToFormattedDate(ts : Number,noTime:Boolean=false,delim:String='.') : String
		{
			//20110412091722
			var timestamp : String = ts.toString();
			var year : String = timestamp.substr(0, 4);
			var month : String = timestamp.substr(4, 2);
			var day : String = timestamp.substr(6, 2);
			var hours : String = timestamp.substr(8, 2);
			var minutes : String = timestamp.substr(10, 2);
			var seconds : String = timestamp.substr(12, 2);
			if(noTime)
				return  day + delim + month + delim + year ;
			
			return  day + delim + month + delim + year + delim + hours + ':' + minutes + ':' + seconds;
				
		}

		public static function dateToFileStamp() : String
		{
			var now : Date = new Date();
			var year : uint = now.getFullYear();
			var month : uint = now.getMonth() + 1;
			var day : uint = now.getDate();
			var hours : uint = now.getHours();
			var minutes : uint = now.getMinutes();
			var seconds : uint = now.getSeconds();
			return  year + addLeadingZeros(month) + addLeadingZeros(day) + '_' + addLeadingZeros(hours) + addLeadingZeros(minutes) + addLeadingZeros(seconds);
		}

		public static function dateToTimeStamp(date : Date = null) : String
		{
			if(!date)
				 date = new Date();
			var year : uint = date.getFullYear();
			var month : uint = date.getMonth() + 1;
			var day : uint = date.getDate();
			var hours : uint = date.getHours();
			var minutes : uint = date.getMinutes();
			var seconds : uint = date.getSeconds();
			return  year + addLeadingZeros(month) + addLeadingZeros(day) + addLeadingZeros(hours) + addLeadingZeros(minutes) + addLeadingZeros(seconds);
		}
		public static function timeStampToDate(timestamp:String) : Date
		{
			var year : int = Number(timestamp.substr(0, 4));
			var month : int =Number( timestamp.substr(4, 2));
			var day : int = Number(timestamp.substr(6, 2));
			var hours : int = Number(timestamp.substr(8, 2));
			var minutes : int = Number(timestamp.substr(10, 2));
			var seconds : int = Number(timestamp.substr(12, 2));		
			return new Date(year,month-1,day,hours,minutes,seconds);
			}

		public static function formattedTime() : String
		{
			var now : Date = new Date();
			var hours : uint = now.getHours();
			var minutes : uint = now.getMinutes();
			var seconds : uint = now.getSeconds();
			return  addLeadingZeros(hours) + ":" + addLeadingZeros(minutes) + ":" + addLeadingZeros(seconds);
		}

		public static function formattedDate(date : Date = null,noTime : Boolean = false,delim : String = '.') : String
		{
			if(date)
				var now : Date = date;
			else
				now = new Date();
			if(noTime)
				return addLeadingZeros(now.date) + delim + addLeadingZeros(+now.month + 1) + delim + now.fullYear;
			return addLeadingZeros(now.date) + delim + addLeadingZeros(+now.month + 1) + delim + now.fullYear + delim + addLeadingZeros(now.hours) + delim + addLeadingZeros(now.minutes);
		}

		public static function secondsToDate(seconds : uint) : String
		{
			var date : Date = new Date(seconds * 1000);
			return addLeadingZeros(date.date) + "." + addLeadingZeros(+date.month + 1) + "." + date.fullYear + " " + addLeadingZeros(date.hours) + ":" + addLeadingZeros(date.minutes);
		}

		public static function secondsToTime(seconds : uint,format : Boolean = false) : String
		{
			var h : String = '';
			var m : String = '';
			var s : String = '';
			if(format)
			{
				
				h = 'h';
				m = 'm';
				s = 's';
			}
			var hours : uint = seconds / SECONDS_PER_HOUR;
			var minutes : uint = (seconds - hours * SECONDS_PER_HOUR) / SECONDS_PER_MINUTE;
			var secs : uint = seconds - (minutes * SECONDS_PER_MINUTE + hours * SECONDS_PER_HOUR);
			return addLeadingZeros(hours) + h + ":" + addLeadingZeros(minutes) + m + ":" + addLeadingZeros(secs) + s;
		}

		public static function secondsToMS(seconds : uint) : String
		{
			var hours : uint = seconds / SECONDS_PER_HOUR;
			var minutes : uint = (seconds - hours * SECONDS_PER_HOUR) / SECONDS_PER_MINUTE;
			var secs : uint = seconds - (minutes * SECONDS_PER_MINUTE + hours * SECONDS_PER_HOUR);
			return  addLeadingZeros(minutes) + ":" + addLeadingZeros(secs);
		}

		public static function secondsToVideoTime(totsecs : Number) : String
		{
			var seconds : uint = Math.floor(totsecs);
			var hours : uint = seconds / SECONDS_PER_HOUR;
			var minutes : uint = (seconds - hours * SECONDS_PER_HOUR) / SECONDS_PER_MINUTE;
			var secs : uint = seconds - (minutes * SECONDS_PER_MINUTE + hours * SECONDS_PER_HOUR);
			var ms : uint = Math.round((totsecs - seconds) * 1000);
			return  addLeadingZeros(minutes) + ":" + addLeadingZeros(secs) + ':' + addLeadingZeros(ms, 3);
		}

		public static function datePlusDays(date : Date,days : Number) : Date 
		{
			return new Date(date.getTime() + (days * SECONDS_PER_DAY * 1000));
		}
	}
}
