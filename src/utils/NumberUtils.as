package utils 
{

	/**
	 * @author gerard
	 */
	public class NumberUtils 
	{
		public static function intToHexString(integer:uint):String
		{
			var hex:String=integer.toString(16);
			
			while(hex.length<6)
				hex="0"+hex;
				
			hex=hex.toUpperCase();
			return "0x"+hex;
		}
	}
}
