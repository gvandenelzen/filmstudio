package  
utils
{
	import flash.display.Sprite;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.PixelSnapping;
	import flash.geom.Matrix;

	/**
	 * @author gerard
	 */
	public class ImageUtils 
	{
		public static function cloneImage(image : DisplayObject) : DisplayObject
		{
			var bitmapData : BitmapData = new BitmapData(image.width, image.height, true, 0);
			bitmapData.draw(image, null, null, null, null, true) ;
			var ps : String = PixelSnapping.AUTO;
			return 	new Bitmap(bitmapData, ps, true);
		}

		public static function resizeImage(image : DisplayObject,w : Number,h : Number) : DisplayObject
		{
		
			var scale : Number = 1 ;
			if (image.width > w || image.height > h )
			{
				Logger.writeLog('ImgeUtils resizeImage image is bigger', image.width, image.height, w, h);
				if(w / h < image.width / image.height)
					scale = w / image.width;
				else
					scale = h / image.height;
			}
			image.scaleX = image.scaleY = scale;
			Logger.writeLog('ImgeUtils resizeImage scale', image.width, image.height, scale);
			var sprite : Sprite = new Sprite();
			sprite.addChild(image);
			image.x = (w - image.width) / 2;
			image.y = (h - image.height) / 2;
			var bitmapData : BitmapData = new BitmapData(w, h, true, 0xFF0000);
			Logger.writeLog('ImgeUtils resizeImage', image.width, image.height, w, h);
			bitmapData.draw(sprite, null, null, null, null, true) ;
			var ps : String = PixelSnapping.AUTO;
			return 	new Bitmap(bitmapData, ps, true);
		}

		public static function cropImage(image : DisplayObject,w : Number,h : Number) : DisplayObject
		{
			var scale : Number = 1 ;
			if (image.width > w || image.height > h )
			{
				if(w / h < image.width / image.height)
					scale = w / image.width;
				else
					scale = h / image.height;
				w = image.width * scale;
				h = image.height * scale;
			}
			else
			{
				w = image.width;
				h = image.height;
			}
			var bitmapData : BitmapData = new BitmapData(w, h, true, 0);
			var matrix : Matrix = new Matrix();
			matrix.scale(scale, scale);
			bitmapData.draw(image, matrix, null, null, null, true) ;
			var ps : String = PixelSnapping.AUTO;
			return 	new Bitmap(bitmapData, ps, true);
		}
	}
}
