package utils 
{
	import flash.display.Stage;
	import flash.system.Capabilities;

	/**
	 * @author gerard.vde
	 */
	public class AirUtils 
	{
		public static function  checkIfAir(stage : Stage) : Boolean
		{
			if(stage.loaderInfo.url.indexOf( 'app' ) == 0)
				return true;
			return false;
		}

		public static function getFileSeperator() : String
		{
			var system : String = Capabilities.os.split( ' ' ).shift( );
			switch(system)
			{
				case 'Windows':
					return "\\";
			}
			return "/";
		}
	}
}
