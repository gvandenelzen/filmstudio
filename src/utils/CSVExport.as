package utils 
{

	/**
	 * @author gerard
	 */
	public class CSVExport 
	{
		public static const CR : String = "\r\n";
		public static const DEL : String = ";";
		public static const SEARCH : RegExp = /"/g;
		public static const REPLACE : String = "'";
		public static const WRAP : String = "\"";

		public static function convertToCSV(rows : Array,headers : Array = null) : String
		{
			var csvString : String = '';
			if(headers)
			{
				for each (var header:String in headers)
				{
					csvString += wrap(header) + DEL;
				}
				csvString += CR;
			}
			for each (var row:Array in rows)
			{
				for each (var field:String in row)
				{
					csvString += wrap(field) + DEL;
				}
				csvString += CR;
			}
			return csvString;
		}

		private static function wrap(txt : String) : String 
		{
			if(!txt)
				return WRAP +  WRAP;
			txt = txt.replace(SEARCH, REPLACE);
			return WRAP + txt + WRAP;
		}
	}
}
