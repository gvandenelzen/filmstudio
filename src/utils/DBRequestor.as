﻿package utils 
{
	import flash.utils.ByteArray;

	import settings.Constants;

	import events.DataEvent;

	import model.ErrorCodes;
	import com.sociodox.utils.Base64;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.utils.Timer;
	/**
	 * @author gerard
	 */
	public class DBRequestor extends EventDispatcher 
	{
		private var _xmlLoader : URLLoader;

		private var _xml : XML;
		private var _scriptURL : String;
		private var _data : String;
		private var _request : String;
		private var _queu : Array;
		private var _busy : Boolean;
		private var _timeOut : Timer;
		private var _error : String;
		private var _hasError : Boolean;
		private var _zlib : Boolean;

		function DBRequestor(scriptURL : String)
		{
			_queu = [];
			_timeOut = new Timer(25000, 1);
			_timeOut.addEventListener(TimerEvent.TIMER_COMPLETE, onTimeout);
			_scriptURL = scriptURL;
			_xmlLoader = new URLLoader();
			_xmlLoader.addEventListener(Event.COMPLETE, onLoadComplete);
			_xmlLoader.addEventListener(IOErrorEvent.IO_ERROR, onError);
			_xmlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onError);
		}

		private function onTimeout(event : TimerEvent) : void 
		{
			Logger.writeLog('DBRequest time out');
			dispatchEvent(new DataEvent(DataEvent.FREE, ""));
			onError();
		}

		/*
		 * We define request explicitly
		 */
		public function executeRequest(request : String,variables : URLVariables = null) : void
		{
			if(Constants.USE_COMPRESSION == 1)
			{
				if(variables == null)
					variables = new  URLVariables();
				variables.compress = Constants.USE_COMPRESSION;
			}
			if(_busy)
			{
				_queu.push({request:request, variables:variables});
				return;
			}
			dispatchEvent(new DataEvent(DataEvent.BUSY, ""));
			_request = request;
			_timeOut.start();
			_busy = true;
			_xml = null;
			var urlRequest : URLRequest = new URLRequest(_scriptURL);
			urlRequest.method = URLRequestMethod.POST;
			if(!variables)
				variables = new URLVariables();
			variables['request'] = request;
			var today : Date = new Date();
			variables['sessionId'] = today.getTime();
			urlRequest.data = variables;
			Logger.writeLog('DBRequest start ', request);
			try
			{
				_xmlLoader.load(urlRequest);
				Logger.writeLog("DBRequestor executeRequest", request);
			}
			catch (e : Error)
			{
				dispatchEvent(new Event(DataEvent.FREE));
				onError();
			}
		}

		
		public function dispose() : void
		{
			_xmlLoader.removeEventListener(Event.COMPLETE, onLoadComplete);
			_xmlLoader.removeEventListener(IOErrorEvent.IO_ERROR, onError);
			_xmlLoader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, onError);
			_xmlLoader = null;
		}

		private function parseResponse(data : String) : void
		{
			_error = "";
			_hasError = false;
			try
			{
				data = cleanData(data);
				_xml = new XML(data);
			}
			catch (e : Error)
			{
				Logger.writeLog("Bad xml ", data);
				_error = "Bad xml";
				_hasError = true;
				dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, ErrorCodes.BAD_XML));
				return;
			}

			if (!_xml)
			{
				Logger.writeLog("Bad xml ", data);
				_error = "Bad xml";
				_hasError = true;
				dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, ErrorCodes.BAD_XML));
				return;
			}
			if (_xml.hasSimpleContent())
			{
				_xml = new XML("<result>" + data + "</result>");
				_xml = _xml.children()[0];
			}
			if(_xml['status'])
			{
				if(_xml['status'] == 'ok')
				{
					_zlib = (_xml['zlib'] == 1) ? true : false;
					if(_zlib)
					{
						_data = unCompress(_xml['data']);
					}
					else
					{
						_data = _xml['data'];
					}
					_request = _xml['request'];
					dispatchEvent(new Event(Event.COMPLETE));
				}
				else
				{
					_request = _xml['request'];
					Logger.writeLog('DB Error ', _xml['data']);
					_error = _xml['data'];
					_hasError = true;
					dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, ErrorCodes.DB_ERROR));
				}
				Logger.writeLog('DBRequest end ', _request);
				return;
			}
			dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, ErrorCodes.BAD_XML));
		}

		private function cleanData(data : String) : String
		{
			return  XMLUtils.clean(data);
		}

		private function onLoadComplete(e : Event) : void
		{
			_timeOut.stop();
			_timeOut.reset();
			e.stopImmediatePropagation();
			dispatchEvent(new DataEvent(DataEvent.FREE, ""));
			if (!_xmlLoader.data)
			{
				dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, ErrorCodes.BAD_XML));
				checkQueu();
			}
			else
			{
				parseResponse(_xmlLoader.data);
				checkQueu();
			}
		}

		private function checkQueu() : void 
		{
			_busy = false;
			if(_queu.length == 0)
				return;
			var req : Object = _queu.shift();
			if(!req)
				return;
			executeRequest(req['request'], req['variables']);
		}

		private function onError(e : Event = null) : void
		{
			_timeOut.stop();
			_timeOut.reset();
			dispatchEvent(new DataEvent(DataEvent.FREE, ""));
			dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, ErrorCodes.IO_ERROR));
			checkQueu();
		}

		private function unCompress(cdata : String) : String
		{	
			try 
			{
				var byteArray : ByteArray = Base64.decode(cdata);
				byteArray.inflate()
			} catch (e : Error) 
			{
				Logger.writeLog("Error inflate " + e.message);
				dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, ErrorCodes.EXPAND_ERROR));
				return "";
			}
			var result : String = byteArray.toString();
			return result;
		}

		public function get data() : String
		{
			return _data;
		}

		public function get request() : String
		{
			return _request;
		}

		public function get error() : String
		{
			return _error;
		}

		public function get hasError() : Boolean
		{
			return _hasError;
		}
	}
}
