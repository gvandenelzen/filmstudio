package utils 
{

	/**
	 * @author gerard
	 */
	public class ParameterDecoder 
	{
		public static function decode(encoded : String) : String 
		{
			var hexNum : uint = Number('0x' + encoded);
			if(isNaN(hexNum))
				return encoded;
			var hexArray : Array = encoded.split('');
			hexArray = hexArray.reverse();
			var decoded : String = '';
			var charCode : uint;
			var normChar : String;
			for (var i : uint = 0;i < hexArray.length - 1;i += 2) 
			{
				charCode = Number("0x" + hexArray[i] + hexArray[i + 1]);
				normChar = String.fromCharCode(charCode);
				decoded += normChar;
			}
			return decoded;
		}

		public static function encode(toencode : String) : String 
		{
			var encoded : String = "";
			var charNr : Number;
			var hex:String;
			for(var i : uint = 0;i < toencode.length;i++)
			{
				charNr = toencode.charCodeAt(i);
				hex=charNr.toString(16);
				while(hex.length<2)
					hex="0"+hex;
				encoded+=hex;
			}
			var encodedArray:Array = encoded.split("");
			encodedArray=encodedArray.reverse();
			return encodedArray.join("") ;
		}
	}
}
