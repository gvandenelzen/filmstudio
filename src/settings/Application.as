package settings 
{

	/**
	 * @author gerard
	 */
	public class Application 
	{
		public static const MAIN : String = 'MainView';
		public static const NAVIGATION : String = 'Navigation';
		public static const VIDEO_EDITOR : String = 'VideoEditor';
		public static const VIDEO_ADMIN : String = 'VideoAdmin';
		public static const RECORDER : String = 'Recorder';
		public static const SEQUENCER : String = 'Sequencer';
		public static const INTERVIEWS_ADMIN : String = 'InterviewsAdmin';
		public static const PUBLISHING : String = 'Publishing';
		public static const USERS_ADMIN : String = 'UsersAdmin';
		public static const THEMES_ADMIN : String = 'ThemesAdmin';
		public static const BIG_VIDEO_PLAYER:String='BigVideoPlayer';
		public static const VIDEO_UPLOAD:String='VideoUploader';
	}
}
