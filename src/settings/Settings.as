package settings 
{
	import utils.Logger;

	import flash.events.Event;

	/**
	 * @author gerard
	 */
	public class Settings 
	{
		private var _cameraSettings : CameraSettings;
		private var _recordSettings : RecordSettings;
		private var _connectionSettings : ConnectionSettings;

		public function Settings(xml : XML = null) : void
		{
			if(xml)
				parseSettings(xml);
			else
			{
				_cameraSettings=new CameraSettings();
				_recordSettings=new RecordSettings();
				 _connectionSettings=new  ConnectionSettings();
			}
		}

		private function parseSettings(xml : XML) : void 
		{
			Logger.writeLog('parseSettings start');
			var settingsList : XMLList = xml.children();
			var nodeName : String;
			for each (var nodeXML:XML in settingsList)
			{
				nodeName = nodeXML.name();
				switch (nodeName)
				{
					case 'connection':
						_connectionSettings = new ConnectionSettings(nodeXML);
						break;
					case 'record':
						_recordSettings = new RecordSettings(nodeXML);
						break;
					case 'camera':
						_cameraSettings = new CameraSettings(nodeXML);
						break;
				}
			}
			if(!_cameraSettings)
				_cameraSettings = new CameraSettings();
		}
		
		public function get cameraSettings() : CameraSettings
		{
			return _cameraSettings;
		}
		
		public function get recordSettings() : RecordSettings
		{
			return _recordSettings;
		}
		
		public function get connectionSettings() : ConnectionSettings
		{
			return _connectionSettings;
		}
	}
}
