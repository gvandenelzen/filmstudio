package settings 
{
	import utils.Logger;

	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;

	/**
	 * @author gerard
	 */
	public class SettingsLoader extends EventDispatcher 
	{
		private var _settings : Settings;

		public function loadSettings(settings : String) : void 
		{
			var settingsDir : File = File.documentsDirectory;
			settingsDir = settingsDir.resolvePath('filmstudio');
			if(!settingsDir.isDirectory)
				settingsDir.createDirectory();
			var settingsFile : File = settingsDir.resolvePath(settings);
			var originalSettings : File = File.applicationDirectory;
			originalSettings = originalSettings.resolvePath(settings);
			Logger.writeLog("LoadSettings file : ", settingsFile.nativePath);
			if(!settingsFile.exists)
			{

				originalSettings.copyTo(settingsFile);
			}
			else
			{
				/*
				try
				{
					var oXML : XML = readSettingsFile(originalSettings);
					var nXML : XML = readSettingsFile(settingsFile);
					Logger.writeLog("LoadSettings version : ", oXML.@version, nXML.@version);
					if(oXML.@version != nXML.@version)
					originalSettings.copyTo(settingsFile, true);
				}
				catch(e:Error)
				{
					Logger.writeLog("LoadSettings fileERROR : ",e.message);
				}
				 
				 */
			}
			_settings = new Settings(readSettingsFile(settingsFile));
			dispatchEvent(new Event(Event.COMPLETE));
		}

		private function readSettingsFile(settingsFile : File) : XML
		{
			var fileStream : FileStream = new FileStream();
			fileStream.open(settingsFile, FileMode.READ);
			var data : String = fileStream.readUTFBytes(fileStream.bytesAvailable);
			if(data)
			{
				try
				{
					var xml : XML = new XML(data);
				}
				catch(e : Error)
				{
					var msg : String = 'Settings invalid XML:' + settingsFile;
					dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, msg));
					return xml;
				}
				return xml;
			}
			dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, msg));
			return xml;
		}

		
		
		public function get connectionSettings() : ConnectionSettings
		{
			return _settings.connectionSettings;
		}

		public function get recordSettings() : RecordSettings
		{
			return _settings.recordSettings;
		}

		public function get cameraSettings() : CameraSettings
		{
			return _settings.cameraSettings;
		}

		public function get settings() : Settings
		{
			return _settings;
		}
	}
}
