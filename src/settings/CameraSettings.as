package  

settings{

	/**
	 * @author gerard
	 */
	public class CameraSettings 
	{
		public var quality : uint = 60;
		public var bandwidth : uint = 0;
		public var fps : uint = 15;
		public var width : uint = 320;
		public var height : uint = 240;

		public function CameraSettings(xml : XML = null)
		{
			
			if(xml)
				parseSettings(xml);
		}

		private function parseSettings(xml : XML) : void 
		{
			var settingsList : XMLList = xml.children();
			var nodeName:String;
			for each (var settingsXML:XML in settingsList)
			{
				nodeName=settingsXML.name();
				switch(nodeName)
				{
					case 'quality':
						quality = int(settingsXML.text());
						break;
					case 'bandwidth':
						bandwidth = int(settingsXML.text());
						break;
					case 'width':
						width = int(settingsXML.text());
						break;
					case 'height':
						height = int(settingsXML.text());
						break;
					case 'fps':
						fps = int(settingsXML.text());
						break;
				}
			}
		}
	}
}
