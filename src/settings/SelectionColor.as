package settings 
{

	/**
	 * @author gerard
	 */
	public class SelectionColor 
	{
		public static const NO_SELECTION:uint=0x666666;
		public static const CURRENT_SELECTION:uint=0xF89B34;
		public static const ONE_SELECTION:uint=0x4480BA;
		public static const MULTIPLE_SELECTION:uint=0x0060A7;
	}
}
