package  
settings
{

	/**
	 * @author gerard
	 */
	public class RecordSettings 
	{

		private static const DURATION : uint = 90;
		private static const COUNTDOWN : uint = 3;
		private static const FLEX_TIME : uint = 30;
		public var duration : uint ;
		public var countDown : uint;
		public var flexTime : uint;
		public var forceHD : Boolean;

		public function RecordSettings(xml : XML = null)
		{
			duration = DURATION;
			countDown = COUNTDOWN;
			flexTime = FLEX_TIME;
			forceHD = false;
			if(xml)
				parseSettings(xml);
		}

		private function parseSettings(xml : XML) : void 
		{
			var settingsList : XMLList = xml.children();
			var nodeName : String;
			for each (var settingsXML:XML in settingsList)
			{
				nodeName = settingsXML.name();
				switch(nodeName)
				{
					case 'duration':
						duration = int(settingsXML.text());
						break;
					case 'countdown':
						countDown = int(settingsXML.text());
						break;
					case 'flextime':
						flexTime = int(settingsXML.text());
						break;
					case 'forceHD':
						forceHD = (int(settingsXML.text())==1)?true:false;
						break;
				}
			}
		}
	}
}
